package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ThreeBounce
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.initialization.InitializationStatus
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.api.API
import homeworkout.sixpackabs.workoutfitness.model.Model_Ads
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Boolean
import kotlin.Throwable
import kotlin.let

class SplashActivity : AppCompatActivity() {

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        MobileAds.initialize(this) { initializationStatus: InitializationStatus? -> }
        runOnUiThread { this.setUpAnimation() }
        getAdsData()
        Handler().postDelayed(
            { HomeWorkoutApplication.appOpenManager?.showAdIfAvailable(true) },
            5000
        )
    }

    private fun getAdsData() {
        val retrofit: Retrofit.Builder = Retrofit.Builder().baseUrl(Constant.adsAPI)
            .addConverterFactory(GsonConverterFactory.create())
        val service: API = retrofit.build().create(API::class.java)
        val call: Call<Model_Ads> = service.getAdsData(42)
        call.enqueue(object : Callback<Model_Ads> {
            override fun onResponse(call: Call<Model_Ads>, response: Response<Model_Ads>) {
                if (response.code() == 200) {
                    response.body()?.let { body ->
                        if (body.isStatus) {
                            if (body.data != null) {
                                HomeWorkoutApplication.set_AdsInt(body.data.publisher_id)
                                if (body.data.publishers != null) {
                                    publisher_yes = true
                                    if (HomeWorkoutApplication.get_AdsInt() == 1) {
                                        if (body.data.publishers.admob != null) {
                                            if (body.data.publishers.admob.admob_interstitial != null) {
                                                HomeWorkoutApplication.set_Admob_interstitial_Id(
                                                    body.data.publishers.admob.admob_interstitial.admob_interstitial_id
                                                )
                                            } else {
                                                HomeWorkoutApplication.set_Admob_interstitial_Id(
                                                    resources.getString(R.string.str_Interstitial_ad)
                                                )
                                            }
                                            if (body.data.publishers.admob.admob_native != null) {
                                                HomeWorkoutApplication.set_Admob_native_Id(body.data.publishers.admob.admob_native.admob_Native_id)
                                            } else {
                                                HomeWorkoutApplication.set_Admob_native_Id(
                                                    resources.getString(
                                                        R.string.str_native_ad_key
                                                    )
                                                )
                                            }
                                            if (response.body()!!.data.publishers.admob.admob_open != null) {
                                                HomeWorkoutApplication.set_Admob_openapp(response.body()!!.data.publishers.admob.admob_open.admob_Open_id)
                                            } else {
                                                HomeWorkoutApplication.set_Admob_openapp(
                                                    resources.getString(
                                                        R.string.open_app_ad_id
                                                    )
                                                )
                                            }
                                            if (body.data.publishers.admob.qureka_open != null) {
                                                HomeWorkoutApplication.set_qureka(
                                                    Boolean.parseBoolean(
                                                        body.data.publishers.admob.qureka_open.querka_id
                                                    )
                                                )
                                            } else {
                                                HomeWorkoutApplication.set_qureka(false)
                                            }
                                        } else {
                                            HomeWorkoutApplication.set_Admob_interstitial_Id(
                                                resources.getString(R.string.str_Interstitial_ad)
                                            )
                                            HomeWorkoutApplication.set_Admob_native_Id(
                                                resources.getString(
                                                    R.string.str_native_ad_key
                                                )
                                            )
                                            HomeWorkoutApplication.set_Admob_openapp(
                                                resources.getString(
                                                    R.string.open_app_ad_id
                                                )
                                            )
                                            HomeWorkoutApplication.set_qureka(false)
                                        }
                                    } else {
                                        HomeWorkoutApplication.set_Admob_interstitial_Id(
                                            resources.getString(
                                                R.string.str_Interstitial_ad
                                            )
                                        )
                                        HomeWorkoutApplication.set_Admob_native_Id(
                                            resources.getString(
                                                R.string.str_native_ad_key
                                            )
                                        )
                                        HomeWorkoutApplication.set_Admob_openapp(
                                            resources.getString(
                                                R.string.open_app_ad_id
                                            )
                                        )
                                        HomeWorkoutApplication.set_qureka(false)
                                    }
                                } else {
                                    HomeWorkoutApplication.set_Admob_interstitial_Id("1234")
                                    HomeWorkoutApplication.set_Admob_native_Id("1234")
                                    HomeWorkoutApplication.set_Admob_openapp("1234")
                                    HomeWorkoutApplication.set_qureka(false)
                                }
                            } else {
                                HomeWorkoutApplication.set_Admob_interstitial_Id(
                                    resources.getString(
                                        R.string.str_Interstitial_ad
                                    )
                                )
                                HomeWorkoutApplication.set_Admob_native_Id(resources.getString(R.string.str_native_ad_key))
                                HomeWorkoutApplication.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                                HomeWorkoutApplication.set_qureka(false)
                            }
                        } else {
                            HomeWorkoutApplication.set_Admob_interstitial_Id(resources.getString(R.string.str_Interstitial_ad))
                            HomeWorkoutApplication.set_Admob_native_Id(resources.getString(R.string.str_native_ad_key))
                            HomeWorkoutApplication.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                            HomeWorkoutApplication.set_qureka(false)
                        }
                    } ?: kotlin.run {
                        HomeWorkoutApplication.set_Admob_interstitial_Id(resources.getString(R.string.str_Interstitial_ad))
                        HomeWorkoutApplication.set_Admob_native_Id(resources.getString(R.string.str_native_ad_key))
                        HomeWorkoutApplication.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                        HomeWorkoutApplication.set_qureka(false)
                    }
                } else {
                    HomeWorkoutApplication.set_Admob_interstitial_Id(resources.getString(R.string.str_Interstitial_ad))
                    HomeWorkoutApplication.set_Admob_native_Id(resources.getString(R.string.str_native_ad_key))
                    HomeWorkoutApplication.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                    HomeWorkoutApplication.set_qureka(false)
                }
            }

            override fun onFailure(call: Call<Model_Ads>, t: Throwable) {
                HomeWorkoutApplication.set_Admob_interstitial_Id(resources.getString(R.string.str_Interstitial_ad))
                HomeWorkoutApplication.set_Admob_native_Id(resources.getString(R.string.str_native_ad_key))
                HomeWorkoutApplication.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                HomeWorkoutApplication.set_qureka(false)
            }
        })
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setUpAnimation() {
        val relativeLayout = RelativeLayout(this)
        val progressBar = ProgressBar(this, null, 16842874)
        val doubleBounce: Sprite = ThreeBounce()
        doubleBounce.color = resources.getColor(R.color.white)
        progressBar.indeterminateDrawable = doubleBounce
        val layoutParams = RelativeLayout.LayoutParams(110, 110)
        layoutParams.addRule(14)
        layoutParams.addRule(12)
        layoutParams.setMargins(0, 0, 0, 50)
        relativeLayout.addView(progressBar, layoutParams)
        setContentView(relativeLayout)
    }

    override fun onBackPressed() {

    }

    companion object {
        var publisher_yes = false
    }
}