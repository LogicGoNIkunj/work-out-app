package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.charts.CombinedChart.DrawOrder
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.CombinedData
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityReportBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.startActivity
import homeworkout.sixpackabs.workoutfitness.interfaces.DialogCallBack
import homeworkout.sixpackabs.workoutfitness.ui.adapter.WeekDayReportAdapter
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.DialogUtils
import homeworkout.sixpackabs.workoutfitness.utils.PreferenceDataBase
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.extension.MATCH_PARENT
import homeworkout.sixpackabs.workoutfitness.extension.getCompactColor
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.jvm.internal.Intrinsics

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var dialogUtils: DialogUtils

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    var dayList: ArrayList<String> = arrayListOf()
    private var dayYearList: ArrayList<String> = arrayListOf()

    private lateinit var binding: ActivityReportBinding

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityReportBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()

    }

    private fun initView() {
        binding.toolbar.setNavigationOnClickListener { finish() }
        setupGraph()
        setWeight()
        setBmiCalculation()

        binding.tvTotalWorkouts.text = databaseHelper.historyTotalWorkoutCount.toString()
        binding.tvTotalKcal.text = databaseHelper.historyTotalCalories.toString()
        binding.tvTotalMinutes.text = (databaseHelper.historyTotalMinutes.div(60)).toString()
        binding.rvHistoryWeek.adapter = WeekDayReportAdapter(common)
    }

    private fun setHeightWeightDialog() {
        dialogUtils.showHeightWeightDialog(this, databaseHelper, object : DialogCallBack {
            override fun positiveClick() {
                setupGraph()
                setWeight()
                setBmiCalculation()
            }

            override fun negativeClick() = Unit

        })
    }

    private fun setWeight() {

        if (preferenceDataBase.getWeight() != 0.0f) {
            if (preferenceDataBase.getWeightUnit().equals(Constant.CONS_KG, ignoreCase = true))
                binding.txtCurrentKg.text = String.format(getString(R.string.current)+"\n%s KG", common.getStringFormat(preferenceDataBase.getWeight().toDouble()))
            else
                binding.txtCurrentKg.text = String.format(getString(R.string.current)+"\n%s LB", common.getStringFormat(common.convertKgToLb(preferenceDataBase.getWeight().toDouble())))
        }else {
            if (preferenceDataBase.getWeightUnit().equals(Constant.CONS_KG, ignoreCase = true)) {
                binding.txtSelWeightUnit.text = Constant.CONS_KG
                binding.txtCurrentKg.text = String.format(getString(R.string.current) + "\n%s KG",preferenceDataBase.getWeight())
            } else {
                binding.txtSelWeightUnit.text = Constant.CONS_LB
                binding.txtCurrentKg.text = String.format(getString(R.string.current) + "\n%s LB",common.getStringFormat(common.convertKgToLb(preferenceDataBase.getWeight().toDouble())))
            }
        }
        val parseFloat = databaseHelper.maximumWeight.toFloat()
        val parseFloat2 = databaseHelper.minimumWeight.toFloat()

        if (parseFloat > 0) {
            binding.txtHeaviestKg.text =
                if (preferenceDataBase.getWeightUnit() == Constant.CONS_KG)
                    String.format(getString(R.string.heaviest)+"\n%s KG", parseFloat)
                else
                    String.format(getString(R.string.heaviest)+"\n%s LB", common.getStringFormat(common.convertKgToLb(parseFloat.toDouble())))

        } else {
            binding.txtHeaviestKg.text =
                if (Intrinsics.areEqual(preferenceDataBase.getWeightUnit(), Constant.CONS_KG))
                    String.format(getString(R.string.heaviest)+"\n%s KG", preferenceDataBase.getWeight())
                else
                    String.format(getString(R.string.heaviest)+"\n%s LB", common.getStringFormat(common.convertKgToLb(preferenceDataBase.getWeight().toDouble())))

        }
        if (parseFloat2 > 0) {
            binding.txtLightestKg.text =
                if (preferenceDataBase.getWeightUnit().equals(Constant.CONS_KG, true))
                    String.format(getString(R.string.lightest)+"\n%s KG", parseFloat2)
                else
                    String.format(getString(R.string.lightest)+"\n%s LB",common.getStringFormat(common.convertKgToLb(parseFloat2.toDouble())))

        } else {
            binding.txtLightestKg.text =
                if (preferenceDataBase.getWeightUnit().equals(Constant.CONS_KG, true))
                    String.format(getString(R.string.lightest)+"\n%s KG", preferenceDataBase.getWeight())
                else
                    String.format(getString(R.string.lightest)+"\n%s KG", common.getStringFormat(common.convertKgToLb(preferenceDataBase.getWeight().toDouble())))
        }

        var strHeaviestKg = binding.txtHeaviestKg.text.toString().removePrefix(String.format(getString(R.string.heaviest)))
        strHeaviestKg = if (strHeaviestKg.contains(" KG")) strHeaviestKg.replace(" KG", "")
        else strHeaviestKg.replace(" LB", "")

        if (strHeaviestKg.toFloat() > 0.0f)
            binding.chartWeight.axisLeft.axisMaximum = strHeaviestKg.toFloat() + 10.0f
         else
            binding.chartWeight.axisLeft.axisMaximum = 240.0f

        if (strHeaviestKg.toFloat() > 0.0f)
            binding.chartWeight.axisLeft.axisMinimum = strHeaviestKg.toFloat() - 10.0f
        else
            binding.chartWeight.axisLeft.axisMinimum = 30.0f

        if (preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, "") == Constant.CONS_CM) {
            binding.tvHeight.text = String.format(getString(R.string.height)+"\n %s CM",preferenceDataBase.getString(Constant.CONS_CENTI_METER, ""))
            return
        }
        binding.tvHeight.text = String.format(Locale.getDefault(),getString(R.string.height) + "\n%d.%d FT ",preferenceDataBase.getFoot(),preferenceDataBase.getInch().toInt())
    }

    private fun setupGraph() {
        val simpleDateFormat = SimpleDateFormat("dd/MM", Locale.getDefault())
        val simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        try {
            calendar.time = simpleDateFormat2.parse("$year-01-01") ?: Date()
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val count = common.isLeapYear(year) + 1
        dayList = arrayListOf()
        dayYearList = arrayListOf()
        for (i3 in 0 until count) {
            dayList.add(simpleDateFormat.format(calendar.time))
            dayYearList.add(simpleDateFormat2.format(calendar.time))
            calendar.add(5, 1)
        }
        binding.chartWeight.setBackgroundColor(-1)

        binding.chartWeight.apply {
            drawOrder = arrayOf(DrawOrder.LINE)
            description.isEnabled = false
            description.text = "Date"
            setNoDataText(resources.getString(R.string.app_name))
            setDrawGridBackground(false)
            setDrawBarShadow(false)
            isHighlightFullBarEnabled = false
            legend.isWordWrapEnabled = false
            legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
            legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
            legend.orientation = Legend.LegendOrientation.HORIZONTAL
            legend.setDrawInside(false)
            axisLeft.setDrawGridLines(true)
            axisRight.isEnabled = false
        }

        binding.chartWeight.xAxis.apply {
            position = XAxis.XAxisPosition.BOTTOM
            axisMinimum = 0.0f
            axisMaximum = count.toFloat()
            granularity = 1.0f
            labelCount = 30
            valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    if (value >= dayList.size.toFloat() || value <= 0.toFloat()) {
                        return ""
                    }
                    return dayList[value.toInt()]
                }
            }
        }

        val combinedData = CombinedData()
        combinedData.setData(lineData)
        combinedData.setValueTypeface(Typeface.DEFAULT)
        binding.chartWeight.apply {
            data = combinedData
            setVisibleXRange(5.0f, 8.0f)
            centerViewTo(
                dayYearList.indexOf(common.convertFullDateToDate(common.currentDate)).toFloat(),50.0f,YAxis.AxisDependency.LEFT)
            invalidate()
        }
    }

    private val lineData: LineData
        get() {
            val userWeightData = databaseHelper.userWeightData
            val lineData = LineData()
            val arrayList = ArrayList<Entry>()
            if (userWeightData.size > 0) {

                for (i in userWeightData.indices) {
                    val dayYear = dayYearList.indexOf( userWeightData[i]["DT"])
                    userWeightData[i][Constant.CONS_KG]?.let {
                        if (preferenceDataBase.getWeightUnit().equals(Constant.CONS_KG,true))
                            arrayList.add(Entry(dayYear.toFloat(), it.toFloat()))
                        else
                            arrayList.add(Entry(dayYear.toFloat(), common.convertKgToLb(it.toDouble()).toFloat()))
                    }
                }
            } else {
                if (preferenceDataBase.getWeight() > 0.toFloat()) {
                    val convertFullDateToDate = common.convertFullDateToDate(common.currentDate)

                    val indexOf2 = dayYearList.indexOf(convertFullDateToDate)
                    if (preferenceDataBase.getWeightUnit().equals(Constant.CONS_KG,true))
                        arrayList.add(Entry(indexOf2.toFloat(), preferenceDataBase.getWeight()))
                    else
                        arrayList.add(Entry(indexOf2.toFloat(),common.convertKgToLb(preferenceDataBase.getWeight().toDouble()).toFloat()))
                }
            }
            val lineDataSet = LineDataSet(arrayList, "Date")
            lineDataSet.color = Color.rgb(130, 87, 242)
            lineDataSet.lineWidth = 1.5f
            lineDataSet.setCircleColor(Color.rgb(130, 130, 130))
            lineDataSet.circleRadius = 5.0f
            lineDataSet.fillColor = Color.rgb(130, 130, 130)
            lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
            lineDataSet.setDrawValues(true)
            lineDataSet.valueTextSize = 10.0f
            lineDataSet.valueTextColor = Color.rgb(130, 87, 242)
            lineDataSet.axisDependency = YAxis.AxisDependency.LEFT
            lineData.addDataSet(lineDataSet)
            return lineData
        }

    private fun setBmiCalculation() {
        val bmiCalculation = common.getBmiValue(
            preferenceDataBase.getWeight(),preferenceDataBase.getFoot(),preferenceDataBase.getInch().toInt())
        val format = String.format(Locale.getDefault(), "%.1f", *arrayOf<Any>(bmiCalculation).copyOf(1))
        val format2 = String.format(Locale.getDefault(),"%.1f", *arrayOf<Any>(common.calculationForBmiGraph(format.toFloat())).copyOf(1))
        var layoutParams = LinearLayout.LayoutParams(0,MATCH_PARENT, format2.toFloat())
        val append = StringBuilder().append(getString(R.string.infinity) + "\n")
        var title = String.format(Locale.getDefault(), "%.2f", *arrayOf<Any>(bmiCalculation).copyOf(1))
        if (title == "0.00") title = "0"

        binding.tvInfinity.text = append.append(title).toString()
        val weight = String.format(Locale.getDefault(), "%.1f", *arrayOf<Any>(bmiCalculation).copyOf(1))
        binding.txtWeightString.apply {
            if (common.bmiWeightString(weight.toFloat()).equals("Unknown",true)) {
                this.text = "Overweight"
                setTextColor(getCompactColor(R.color.colorFour))
                layoutParams = LinearLayout.LayoutParams(0, MATCH_PARENT, 4.3f)
                return@apply
            }
            text = common.bmiWeightString(weight.toFloat())
            setTextColor(ColorStateList.valueOf(common.getBmiWeightTextColor(binding.txtWeightString.text.toString())))
        }
        binding.blankView1.layoutParams = layoutParams

    }

    private fun addDataIntoGraph() {
        databaseHelper.let { databaseHelper ->
            dialogUtils.showWeightDialog(this, databaseHelper, object : DialogCallBack {
                override fun positiveClick() {
                    setupGraph()
                    setWeight()
                    setBmiCalculation()
                }
                override fun negativeClick() {}
            })
        }
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.ivAddGraph -> addDataIntoGraph()
            R.id.btnEdit -> setHeightWeightDialog()
            R.id.btnEditHeight -> setHeightWeightDialog()
            R.id.tvSeeAllWeekStatus -> startActivity<HistoryActivity> { }
        }
    }
}