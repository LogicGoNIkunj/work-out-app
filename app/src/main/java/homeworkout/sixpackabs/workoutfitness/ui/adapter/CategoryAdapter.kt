package homeworkout.sixpackabs.workoutfitness.ui.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import homeworkout.sixpackabs.workoutfitness.databinding.ItemCategoryBinding
import homeworkout.sixpackabs.workoutfitness.extension.getCompactDrawable
import homeworkout.sixpackabs.workoutfitness.extension.startActivity
import homeworkout.sixpackabs.workoutfitness.model.Category
import homeworkout.sixpackabs.workoutfitness.ui.activity.*
import homeworkout.sixpackabs.workoutfitness.utils.Constant


class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.Holder>() {

    var list = arrayListOf<Category>()
        set(value) {
            field = value
            notifyItemRangeChanged(0, field.size)
        }

    inner class Holder(val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context

        init {
            binding.root.setOnClickListener {
                Constant.categoryName = list[adapterPosition].category
                when (list[adapterPosition].id) {
                    1 -> context.startActivity<ChestExerciseActivity> {
                        putExtra(Constant.CONS_CATEGORY_NAME, list[adapterPosition].category)
                    }
                    2 -> context.startActivity<AbsExerciseActivity> {
                        putExtra(Constant.CONS_CATEGORY_NAME, list[adapterPosition].category)
                    }
                    3 -> context.startActivity<ArmExerciseActivity> {
                        putExtra(Constant.CONS_CATEGORY_NAME, list[adapterPosition].category)
                    }
                    4 -> context.startActivity<ShoulderExerciseActivity> {
                        putExtra(Constant.CONS_CATEGORY_NAME, list[adapterPosition].category)
                    }
                    5 -> context.startActivity<LegExerciseActivity> {
                        putExtra(Constant.CONS_CATEGORY_NAME, list[adapterPosition].category)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.binding.ivCategory.setImageDrawable(holder.context.getCompactDrawable(list[position].image))
    }

    override fun getItemCount() = list.size
}