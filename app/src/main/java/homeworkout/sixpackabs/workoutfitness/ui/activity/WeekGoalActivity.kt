package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityWeeklyGoalSetBinding
import homeworkout.sixpackabs.workoutfitness.extension.getCompactColor
import homeworkout.sixpackabs.workoutfitness.extension.getCompactDrawable
import homeworkout.sixpackabs.workoutfitness.interfaces.DialogDismissCallBack
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.DialogUtils
import homeworkout.sixpackabs.workoutfitness.utils.PreferenceDataBase
import javax.inject.Inject

@AndroidEntryPoint
class WeekGoalActivity : AppCompatActivity() {

    @Inject
    lateinit var dialogUtils: DialogUtils

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    private lateinit var binding: ActivityWeeklyGoalSetBinding

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.none, R.anim.slide_down)
    }

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityWeeklyGoalSetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.edWeekDays.background = getCompactDrawable(R.drawable.back_exercise_item)?.apply {
            setTint(getCompactColor(R.color.colorPurple_950))
        }
        binding.edFirstDayOfWeek.background =
            getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                setTint(getCompactColor(R.color.colorPurple_950))
            }
        binding.edWeekDays.text = preferenceDataBase.getWeekGoalDay().toString()
        binding.edFirstDayOfWeek.text =
            common.getFirstWeekOfDayByDayNo(preferenceDataBase.getFirstDayOfWeek())
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBack -> onBackPressed()
            R.id.edWeekDays -> dialogUtils.showWeekDayGoalDialog(this,
                object : DialogDismissCallBack {
                    override fun onDismiss(value: Any?) {
                        value?.let {
                            binding.edWeekDays.text = it.toString()
                            binding.edWeekDays.background =
                                getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                                    setTint(getCompactColor(R.color.colorPurple_950))
                                }
                            binding.edFirstDayOfWeek.background =
                                getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                                    setTint(getCompactColor(R.color.colorPurple_950))
                                }
                        }
                    }
                })
            R.id.edFirstDayOfWeek -> dialogUtils.showFirstWeekDayDialog(
                this,
                object : DialogDismissCallBack {
                    override fun onDismiss(value: Any?) {
                        value?.let {
                            binding.edFirstDayOfWeek.text = value.toString()
                            binding.edWeekDays.background =
                                getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                                    setTint(getCompactColor(R.color.colorPurple_950))
                                }
                            binding.edFirstDayOfWeek.background =
                                getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                                    setTint(getCompactColor(R.color.colorPurple_950))
                                }
                        }
                    }
                })
            R.id.btnSave -> {
                preferenceDataBase.setWeekGoalDay(binding.edWeekDays.text.toString().toInt())
                preferenceDataBase.setFirstDayOfWeek(common.getFirstWeekDayNoByDayName(binding.edFirstDayOfWeek.text.toString()))
                onBackPressed()
            }
        }
    }

}