package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.EventDecorator
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HistoryActivity : AppCompatActivity() {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var common: Common

    override fun onBackPressed() = finish()

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        CoroutineScope(Dispatchers.Main).launch {
            setProgress()
            delay(500)
            setContentView(R.layout.activity_history)
            admobBigNative(findViewById(R.id.flNativeAd))
            initView()
        }

    }

    private fun setProgress() {

        val progressBar = ProgressBar(this).apply {
            indeterminateDrawable.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(getCompactColor(R.color.colorSoftBlue), BlendModeCompat.SRC_ATOP)
        }
        val relativeLayout = RelativeLayout(this).apply {
            gravity = Gravity.CENTER
            setBackgroundColor(getCompactColor(R.color.white))
            addView(progressBar, RelativeLayout.LayoutParams(100, 100))
        }

        setContentView(relativeLayout)
    }

    private fun initView() {
        findViewById<Toolbar>(R.id.toolbar).setNavigationOnClickListener { finish() }
        setupCalendarView()
    }

    private fun setupCalendarView() {
        val eventList: ArrayList<CalendarDay> = ArrayList()

        databaseHelper.completeExerciseDate.forEachIndexed { _, date ->
            eventList.add(CalendarDay.from(date.getYear(common), date.getMonth(common), date.getDay(common)))
        }
        findViewById<MaterialCalendarView>(R.id.calendarView)?.addDecorators(EventDecorator(getCompactDrawable(R.drawable.background_event), eventList))

    }

    fun onClick(view: View) {
        if (view.id == R.id.imgBack) onBackPressed()
    }

    private fun admobBigNative(flNativeAd: FrameLayout) {

        val builder = AdLoader.Builder(this, HomeWorkoutApplication.get_Admob_native_Id())
            .forNativeAd { nativeAd: NativeAd ->
                @SuppressLint("InflateParams")
                val adView = layoutInflater.inflate(R.layout.layout_big_native_ad, null) as NativeAdView
                populateUnifiedNativeAdView(nativeAd, adView)
                findViewById<AppCompatTextView>(R.id.tvSpaceForAd).gone()
                flNativeAd.removeAllViews()
                flNativeAd.addView(adView)
            }

        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                findViewById<AppCompatTextView>(R.id.tvSpaceForAd).visible()
            }

        }).build()

        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_CROP)
        adView.mediaView = mediaView
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.invisible()
        } else {
            adView.bodyView?.visible()
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.invisible()
        } else {
            adView.callToActionView?.visible()
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.gone()
        } else {
            (adView.iconView as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.visible()
        }
        adView.setNativeAd(nativeAd)

    }
}