package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.AppCompatTextView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.google.android.gms.ads.MobileAds
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.onesignal.OneSignal
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.model.Category
import homeworkout.sixpackabs.workoutfitness.ui.activity.SplashActivity.Companion.publisher_yes
import homeworkout.sixpackabs.workoutfitness.ui.adapter.BodyChallengeAdapter
import homeworkout.sixpackabs.workoutfitness.ui.adapter.CategoryAdapter
import homeworkout.sixpackabs.workoutfitness.ui.adapter.WeekDayReportAdapter
import homeworkout.sixpackabs.workoutfitness.ui.custom.CustomDrawerLayout
import homeworkout.sixpackabs.workoutfitness.utils.*
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication.Companion.checkqureka
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication.Companion.qurekaimage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(R.layout.activity_home) {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var dialogUtils: DialogUtils

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    private val workoutDayChangeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == Constant.CONS_ACTION_CHANGE_WORKOUT_DAYS) {
                setupViewPager()
            }
        }
    }

    private var drawerLayout: CustomDrawerLayout? = null
    private val bodyChallengeAdapter: BodyChallengeAdapter by lazy { BodyChallengeAdapter() }
    private val categoryAdapter: CategoryAdapter by lazy { CategoryAdapter() }

    override fun onBackPressed() {
        val intent = Intent(this@HomeActivity, ExitActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        setupWeekTopData()
    }

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        changeStatusBarColor(R.color.white)
        val intentFilter = IntentFilter(Constant.CONS_ACTION_CHANGE_WORKOUT_DAYS)
        registerReceiver(workoutDayChangeReceiver, intentFilter)
        analytics()
        databaseHelper.readWriteDatabase
        setUpDrawerLayout()
        setupViewPager()
        setUpCategoryData()

        if (isNetworkAvailable()) {
            if (publisher_yes) {
                if (checkqureka) {
                    if (HomeWorkoutApplication.get_qureka()) {
                        if (qurekaimage != "") {
                            Glide.with(this).load(qurekaimage)
                                .into(findViewById<View>(R.id.img) as ImageView)
                            findViewById<View>(R.id.qurekaads).visible()
                            findViewById<View>(R.id.qureka).setOnClickListener { view: View? ->
                                try {
                                    val builder = CustomTabsIntent.Builder()
                                    val customTabsIntent = builder.build()
                                    customTabsIntent.intent.setPackage("com.android.chrome")
                                    customTabsIntent.launchUrl(
                                        this,
                                        Uri.parse(HomeWorkoutApplication.url)
                                    )
                                } catch (e: Exception) {
                                    showToast("Something went wrong")
                                }
                            }
                        } else {
                            findViewById<View>(R.id.img).gone()
                            findViewById<View>(R.id.qurekaads).gone()
                        }
                    }
                }
            }
        }

    }

    private fun analytics() {
        MobileAds.initialize(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        FirebaseAnalytics.getInstance(this)
        FirebaseCrashlytics.getInstance()
        OneSignal.initWithContext(this)
        OneSignal.setAppId(resources.getString(R.string.one_signal_id))
    }


    private fun setUpCategoryData() {
        val categoryList = ArrayList<Category>()
        var category = Category()
        category.category = Constant.CONS_BODY_CHEST
        category.image = R.drawable.ic_chest
        category.id = 1
        categoryList.add(category)

        category = Category()
        category.category = Constant.CONS_ABS
        category.image = R.drawable.ic_abs
        category.id = 2
        categoryList.add(category)

        category = Category()
        category.category = Constant.CONS_ARM
        category.image = R.drawable.ic_arm
        category.id = 3
        categoryList.add(category)

        category = Category()
        category.category = Constant.CONS_SHOULDER_AND_BACK
        category.image = R.drawable.ic_shoulder_back
        category.id = 4
        categoryList.add(category)

        category = Category()
        category.category = Constant.CONS_LEG
        category.image = R.drawable.ic_leg
        category.id = 5
        categoryList.add(category)

        categoryAdapter.list = categoryList
        findViewById<RecyclerView>(R.id.rvCategory).adapter = categoryAdapter

    }

    private fun setupViewPager() {
        bodyChallengeAdapter.fullBodyCount =
            databaseHelper.getWorkoutWeekCompletedDayCount(Constant.CONS_FULL_BODY)
        bodyChallengeAdapter.lowerBodyCount =
            databaseHelper.getWorkoutWeekCompletedDayCount(Constant.CONS_LOWER_BODY)
        findViewById<ViewPager2>(R.id.vpBodyChallenge).adapter = bodyChallengeAdapter
        TabLayoutMediator(
            findViewById(R.id.tabLayout),
            findViewById(R.id.vpBodyChallenge)
        ) { _, _ -> }.attach()
    }

    private fun setUpDrawerLayout() {
        drawerLayout = findViewById(R.id.drawerLayout)
        findViewById<AppCompatTextView>(R.id.tvTitle).text = getString(R.string.app_name)
        drawerLayout?.apply {
            setViewScale(Gravity.START, 0.8f)
            setRadius(Gravity.START, 35f)
            setViewElevation(Gravity.START, 80f)
            useCustomBehavior(GravityCompat.START)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupWeekTopData() {
        findViewById<TextView>(R.id.tvTotalWorkouts).text =
            "${getString(R.string.workouts)}\n${databaseHelper.historyTotalWorkoutCount}"
        findViewById<TextView>(R.id.tvTotalKcal).text =
            "${getString(R.string.kcal)}\n${databaseHelper.historyTotalCalories}"
        findViewById<TextView>(R.id.tvTotalMinutes).text =
            "${getString(R.string.minutes)}\n${databaseHelper.historyTotalMinutes.div(60)}"
        findViewById<RecyclerView>(R.id.rvHistoryWeek).adapter = WeekDayReportAdapter(common)
    }

    fun onClick(view: View) {
        if (view.id != R.id.ivDrawer || view.id != R.id.navHome || view.id != R.id.ivClose) {
            CoroutineScope(Dispatchers.Main).launch {
                if ((drawerLayout?.isDrawerOpen(GravityCompat.START) == true)) drawerLayout?.closeDrawer(
                    GravityCompat.START
                )

                delay(350)

                when (view.id) {
                    R.id.tvTotalWorkouts, R.id.tvTotalKcal, R.id.tvTotalMinutes -> startActivity<HistoryActivity> {
                        putExtra(Constant.CONS_HISTORY_FROM, false)
                    }
                    R.id.tvSeeAllWeekStatus -> {
                        startActivity<WeekGoalActivity> { }
                        overridePendingTransition(R.anim.slide_up, R.anim.none)
                    }
                    R.id.navLibrary -> startActivity<WorkoutLibraryActivity> { }
                    R.id.navReport -> startActivity<DetailActivity> { }
                    R.id.navReminder -> startActivity<AlarmActivity> { }
                    R.id.navSetting -> startActivity<SettingActivity> { }
                    R.id.navReportProgress -> dialogUtils.restartProgressDialog(
                        this@HomeActivity,
                        getString(R.string.restart_progress_title)
                    )
                }
            }
        }

        when (view.id) {
            R.id.ivDrawer -> {
                if ((drawerLayout?.isDrawerOpen(GravityCompat.START) == true)) drawerLayout?.closeDrawer(
                    GravityCompat.START
                )
                else drawerLayout?.openDrawer(GravityCompat.START)
                return
            }
            R.id.navHome -> drawerLayout?.closeDrawer(GravityCompat.START)
            R.id.ivClose -> drawerLayout?.closeDrawer(GravityCompat.START)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(workoutDayChangeReceiver)
    }

}