package homeworkout.sixpackabs.workoutfitness.ui.adapter

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.RowReminderBinding
import homeworkout.sixpackabs.workoutfitness.extension.changeTimeFormat
import homeworkout.sixpackabs.workoutfitness.interfaces.OnReminderListener
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderDatabase
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderModel
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderReceiver

class ReminderAdapter : RecyclerView.Adapter<ReminderAdapter.ViewHolder>() {

    var reminderListener: OnReminderListener? = null
    var reminderDataBaseHelper: ReminderDatabase? = null
    var listReminder: ArrayList<ReminderModel> = arrayListOf()

    inner class ViewHolder(val binding: RowReminderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context

        init {
            binding.tvRepeat.setOnClickListener {
                try {
                    if ((listReminder[adapterPosition].day as CharSequence).contains(",")) {
                        val id: String = listReminder[adapterPosition].reminderId.toString()
                        val day =
                            (listReminder[adapterPosition].day as CharSequence).split(",") as ArrayList<String>
                        reminderListener?.updateDays(id, day)
                        return@setOnClickListener
                    }
                    val id: String = listReminder[adapterPosition].reminderId.toString()
                    reminderListener?.updateDays(id, arrayListOf(listReminder[adapterPosition].day))

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            binding.tvTime.setOnClickListener {
                if ((listReminder[adapterPosition].day as CharSequence).contains(",")) {

                    val id = listReminder[adapterPosition].reminderId.toString()
                    val arrayList =
                        (listReminder[adapterPosition].day as CharSequence).split(",") as ArrayList<String>
                    val time = listReminder[adapterPosition].time
                    val parseInt: Int = (time as CharSequence).split(":")[0].toInt()
                    val time2 = listReminder[adapterPosition].time
                    val slitData = (time2 as CharSequence).split(":")[1].toInt()
                    reminderListener?.updateTime(id, arrayList, parseInt, slitData)
                    return@setOnClickListener
                }

                val id = listReminder[adapterPosition].reminderId.toString()
                val day = listReminder[adapterPosition].day
                val time = listReminder[adapterPosition].time
                val pos: Int = (time as CharSequence).split(":")[0].toInt()
                val slitData = (time as CharSequence).split(":")[1].toInt()

                reminderListener?.updateTime(id, arrayListOf(day), pos, slitData)

            }
            binding.switchAlarmOnOff.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) reminderDataBaseHelper?.updateReminderIsActive(
                    listReminder[adapterPosition].reminderId.toString(), "true"
                )
                else reminderDataBaseHelper?.updateReminderIsActive(
                    listReminder[adapterPosition].reminderId.toString(), "false"
                )
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder =
        ViewHolder(RowReminderBinding.inflate(LayoutInflater.from(viewGroup.context),viewGroup,false))

    override fun getItemCount() = listReminder.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

         listReminder[position].time.changeTimeFormat {
             SpannableString(this).apply {
                setSpan(RelativeSizeSpan(0.60f), this.length-2, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                holder.binding.tvTime.text = this
            }
        }
        holder.binding.switchAlarmOnOff.isChecked = listReminder[position].isActive
        val sorted = (listReminder[position].day as CharSequence).split(",")
        if (sorted.size == 7) {
            holder.binding.tvRepeat.text = holder.context.getString(R.string.every_day)
        } else {
            holder.binding.tvRepeat.text = ""
            for (index in sorted.indices) {
                if (holder.binding.tvRepeat.text.toString().isEmpty())
                    holder.binding.tvRepeat.text = getDayName(sorted[index])
                else holder.binding.tvRepeat.append(", " + getDayName(sorted[index]))
            }
        }

        holder.binding.ivDelete.setOnClickListener { confirmDeleteReminder(it.context, position) }
    }

    private fun getDayName(str: String): String {
        return when (str) {
            "1" -> "Mon"
            "2" -> "Tue"
            "3" -> "Wed"
            "4" -> "Thu"
            "5" -> "Fri"
            "6" -> "Sat"
            "7" -> "Sun"
            else -> ""
        }
    }

    private fun confirmDeleteReminder(context2: Context, position: Int): Boolean {
        val builder = AlertDialog.Builder(context2)
        builder.setTitle("Tip")
        builder.setMessage("Confirm Delete?")
        builder.setCancelable(true)
        builder.setPositiveButton("Yes") { dialog, _ ->
            reminderDataBaseHelper?.deleteReminder(listReminder[position].reminderId.toString())
            ReminderReceiver().cancelAlarm(context2, listReminder[position].reminderId)
            notifyItemRangeRemoved(position, listReminder.size)
            listReminder.removeAt(position)
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog, _ ->
            dialog.dismiss()
        }
        builder.create().show()

        return false
    }

}