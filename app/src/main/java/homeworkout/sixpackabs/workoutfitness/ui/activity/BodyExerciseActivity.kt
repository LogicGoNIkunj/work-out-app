package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityBodyExerciseBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.interfaces.OnExerciseItemClick
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.ui.adapter.WorkoutAdapter
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication
import homeworkout.sixpackabs.workoutfitness.utils.NpaGridLayoutManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.math.roundToInt

@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class BodyExerciseActivity : AppCompatActivity() {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var common: Common

    private lateinit var binding: ActivityBodyExerciseBinding

    private val workoutAdapter: WorkoutAdapter by lazy { WorkoutAdapter() }

    private var mInterstitialAd: InterstitialAd? = null

    private var position: Int = -1

    private var tableName = ""
    private var day = ""
    private var weekName = ""

    private var isactivityleft = false

    private val workoutDayChangeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == Constant.CONS_ACTION_CHANGE_WORKOUT_DAYS) {
                setUpUI()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        changeStatusBarColor(R.color.colorYellow_200)
        binding = ActivityBodyExerciseBinding.inflate(layoutInflater)
        setContentView(binding.root)
        isactivityleft = false
        loadAds()
        setUpUI()
        setUpExerciseList()
        val intentFilter = IntentFilter(Constant.CONS_ACTION_CHANGE_WORKOUT_DAYS)
        registerReceiver(workoutDayChangeReceiver, intentFilter)
    }

    private fun setUpExerciseList() {
        binding.toolbar.setNavigationOnClickListener { finish() }
        day = intent.getStringExtra(Constant.CONS_DAY_NAME) ?: ""
        weekName = intent.getStringExtra(Constant.CONS_WEEK_NAME) ?: ""

        workoutAdapter.common = common
        workoutAdapter.backTintColor = R.color.colorYellow_900
        CoroutineScope(Dispatchers.IO).launch {
            val list: ArrayList<WorkOutDetail> =
                if (intent.getStringExtra(Constant.CONS_WORKOUT_CATEGORY_ITEM) == Constant.CONS_FULL_BODY) {
                    tableName = Constant.CONS_TABLE_FULL_BODY_WORKOUT_LIST
                    databaseHelper.getDayExerciseFromWeek(
                        day, weekName, Constant.CONS_TABLE_FULL_BODY_WORKOUT_LIST
                    )
                } else {
                    tableName = Constant.CONS_TABLE_LOWER_BODY_LIST
                    databaseHelper.getDayExerciseFromWeek(
                        day, weekName, Constant.CONS_TABLE_LOWER_BODY_LIST
                    )
                }

            withContext(Dispatchers.Main) {

                workoutAdapter.workoutDetailList = list
                binding.rvExercise.apply {
                    layoutManager = NpaGridLayoutManager(this@BodyExerciseActivity, 1)
                    adapter = workoutAdapter
                }
            }
        }

        // scrollable Description textview
        binding.layoutExercise.tvDescription.movementMethod = ScrollingMovementMethod()

        workoutAdapter.listener = object : OnExerciseItemClick {
            override fun onExerciseClick(index: Int) {
                binding.layoutExercise.txtTitle.changeTint(R.color.colorYellow_200)
                binding.layoutExercise.btnClose.changeTint(R.color.colorYellow_200)
                binding.layoutExercise.ivNext.changeTint(R.color.colorYellow_200)
                binding.layoutExercise.ivPrev.changeTint(R.color.colorYellow_200)
                binding.layoutExercise.ivYoutube.changeTint(R.color.colorYellow_200)
                binding.rvExercise.gone()
                binding.btnStartWorkout.gone()
                binding.layoutExercise.root.visible()
                binding.layoutExercise.tvDescription.scrollTo(0, 0)
                binding.layoutExercise.btnClose.isEnabled = true
                Handler(Looper.getMainLooper()).postDelayed(
                    { binding.layoutExercise.root.slideUp() },
                    10
                )
                position = index

                loadExerciseItem()
            }
        }
    }

    private fun closeExerciseBottom() {
        binding.layoutExercise.root.slideDown {
            Handler(Looper.getMainLooper()).postDelayed({
                binding.layoutExercise.btnClose.isEnabled = false
                binding.layoutExercise.root.gone()
                binding.rvExercise.visible()
                binding.btnStartWorkout.visible()
            }, 550)
        }
        position = -1
    }

    private fun loadExerciseItem() {
        when {
            position != 0 && position < workoutAdapter.workoutDetailList.size - 1 -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorYellow_200)
                binding.layoutExercise.ivNext.changeTint(R.color.colorYellow_200)
            }
            position == workoutAdapter.workoutDetailList.size - 1 -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorYellow_200)
                binding.layoutExercise.ivNext.changeTint(R.color.colorYellow_900)
            }
            position != 0 -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorYellow_200)
            }
            else -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorYellow_900)
                binding.layoutExercise.ivNext.changeTint(R.color.colorYellow_200)
            }
        }

        val (description, _, _, _, title) = workoutAdapter.workoutDetailList[position]
        binding.layoutExercise.tvTitle.text = title

        binding.layoutExercise.tvDescription.text =
            description.replace("\\n", "\n", false).replace("\\r", "", false)
        val assetItems = common.replaceSpacialCharacters(title).let { common.getAssetItems(it) }
        binding.layoutExercise.tvCount.text =
            "${(position + 1)}/${workoutAdapter.workoutDetailList.size}"

        CoroutineScope(Dispatchers.Main).launch {
            binding.layoutExercise.imageFlipper.removeAllViews()
            for (index in assetItems.indices) {
                val imageView = ImageView(this@BodyExerciseActivity)
                Glide.with(this@BodyExerciseActivity).load(assetItems[index]).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                binding.layoutExercise.imageFlipper.addView(imageView)
            }
            if (assetItems.size == 1) {
                val imageView = ImageView(this@BodyExerciseActivity)
                Glide.with(this@BodyExerciseActivity).load(assetItems[0]).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                binding.layoutExercise.imageFlipper.addView(imageView)
            }

            binding.layoutExercise.imageFlipper.apply {
                isAutoStart = true
                flipInterval = resources.getInteger(R.integer.view_flipper_animation)
                startFlipping()
            }

        }

    }

    private fun setUpUI() {

        val categoryType = intent.getStringExtra(Constant.CONS_WORKOUT_CATEGORY_ITEM)
        val bodyCount: Int
        if (categoryType == Constant.CONS_FULL_BODY) {
            binding.tvTitle.text = getString(R.string.full_body)
            binding.ivImage.setImageResource(R.drawable.img_full_body)
            bodyCount = databaseHelper.getWorkoutWeekCompletedDayCount(Constant.CONS_FULL_BODY)
        } else {
            binding.tvTitle.text = getString(R.string.lower_body)
            binding.ivImage.setImageResource(R.drawable.img_lower_body)
            bodyCount = databaseHelper.getWorkoutWeekCompletedDayCount(Constant.CONS_LOWER_BODY)
        }

        val format = String.format("%.1f", bodyCount.toFloat() * 100.toFloat() / 28.toFloat())
        binding.tvRemainDays.text = "${(28 - bodyCount)} Days Left"
        binding.tvDayInPercentage.text = "${format.toDouble().roundToInt()}%"
        binding.progressBar.progress = format.toFloat().toInt()
    }

    override fun onBackPressed() {
        finish()
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btnStartWorkout -> {
                navigateToWorkout()
                try {
                    if (mInterstitialAd != null && !isactivityleft) {
                        mInterstitialAd?.show(this@BodyExerciseActivity)
                        HomeWorkoutApplication.appOpenManager?.isAdShow = true
                    }
                } catch (e: Exception) {
                }
            }
            R.id.ivYoutube -> {
                try {
                    if (workoutAdapter.workoutDetailList[position].workoutVideoLink.isNotEmpty()) {
                        common.openYoutube(workoutAdapter.workoutDetailList[position].workoutVideoLink)
                        return
                    }
                    showToast(getString(R.string.error_video_not_exist))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                    showToast(getString(R.string.error_video_not_exist))
                }
            }
            R.id.ivPrev -> {
                if (position != 0 && position != -1) {
                    position--
                    loadExerciseItem()
                }
            }
            R.id.ivNext -> {
                if (position != workoutAdapter.workoutDetailList.size - 1) {
                    position++
                    loadExerciseItem()
                }

            }
            R.id.btnClose -> closeExerciseBottom()

        }
    }


    override fun onResume() {
        super.onResume()
        isactivityleft = false
    }

    override fun onPause() {
        super.onPause()
        this.isactivityleft = true
    }

    override fun onStop() {
        super.onStop()
        this.isactivityleft = true
    }


    private fun navigateToWorkout() {
        startActivity<WorkoutActivity> {
            putExtra(Constant.CONS_WORKOUT_LIST, workoutAdapter.workoutDetailList)
            putExtra(Constant.CONS_WORKOUT_WORK_TABLE_NAME, tableName)
            putExtra(Constant.CONS_DAY_NAME, day)
            putExtra(Constant.CONS_WEEK_NAME, weekName)
        }
    }

    fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            HomeWorkoutApplication.get_Admob_interstitial_Id(),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd!!.fullScreenContentCallback = object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            HomeWorkoutApplication.appOpenManager?.isAdShow = false
                            mInterstitialAd = null
                            loadAds()
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            HomeWorkoutApplication.appOpenManager?.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            HomeWorkoutApplication.appOpenManager?.isAdShow = true
                        }
                    }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    HomeWorkoutApplication.appOpenManager?.isAdShow = false
                }
            })
    }


    override fun onDestroy() {
        super.onDestroy()
        isactivityleft = true
        unregisterReceiver(workoutDayChangeReceiver)
    }
}