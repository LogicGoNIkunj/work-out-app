package homeworkout.sixpackabs.workoutfitness.ui.custom

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.cardview.widget.CardView
import androidx.core.graphics.ColorUtils
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.google.android.material.shape.MaterialShapeDrawable
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.extension.getCompactColor
import kotlin.math.roundToInt

@Suppress("DEPRECATION")
open class CustomDrawerLayout : DrawerLayout {
    private var settings = HashMap<Int, Setting>()
    private var defaultScrimColor = -0x67000000
    private var defaultDrawerElevation = 0f
    private var frameLayout: FrameLayout? = null
    private var drawerView: View? = null
    private var defaultFitsSystemWindows = false
    private var contrastThreshold = 3f
    private var cardBackgroundColor = 0
    private var _isOpen = true
    private lateinit var colorAnimation: ValueAnimator
    private var colorFrom = 0
    private var colorTo = 0

    private val activity: Activity
        get() = getActivity(context)

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs)
    }

    @SuppressLint("CustomViewStyleable")
    private fun init(context: Context, attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.AdvanceDrawerLayout)
        cardBackgroundColor =
            typedArray.getColor(R.styleable.AdvanceDrawerLayout_cardBackgroundColor, 0)
        typedArray.recycle()
        defaultDrawerElevation = drawerElevation
        defaultFitsSystemWindows = fitsSystemWindows

        colorFrom = activity.getCompactColor(R.color.white)
        colorTo = activity.getCompactColor(R.color.colorSoftBlue)

        addDrawerListener(object : DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                this@CustomDrawerLayout.drawerView = drawerView
                updateSlideOffset(drawerView, slideOffset)
            }

            override fun onDrawerOpened(drawerView: View) = Unit

            override fun onDrawerClosed(drawerView: View) = Unit

            override fun onDrawerStateChanged(newState: Int) = Unit
        })
        frameLayout = FrameLayout(context)
        frameLayout?.setPadding(0, 0, 0, 0)
        super.addView(frameLayout)
    }

    private fun changeStatusBarColor(color: Int) {
        activity.window?.statusBarColor = color
    }

    override fun addView(child: View, params: ViewGroup.LayoutParams) {
        child.layoutParams = params
        addView(child)
    }

    override fun addView(child: View) {
        if (child is NavigationView)
            super.addView(child)
        else {
            CardView(context).apply {
                radius = 0f
                addView(child)
                cardElevation = 0f
                setCardBackgroundColor(cardBackgroundColor)
                frameLayout?.addView(this)
            }
        }
    }

    fun setViewScale(gravity: Int, percentage: Float) {
        val absGravity = getDrawerViewAbsoluteGravity(gravity)
        val setting: Setting?
        if (!settings.containsKey(absGravity)) {
            setting = createSetting()
            settings[absGravity] = setting
        } else setting = settings[absGravity]
        setting?.percentage = percentage
        if (percentage < 1) {
            setStatusBarBackground(null)
            systemUiVisibility = 0
        }
        setting?.scrimColor = Color.TRANSPARENT
        setting?.drawerElevation = 0f
    }

    fun setViewElevation(gravity: Int, elevation: Float) {
        val absGravity = getDrawerViewAbsoluteGravity(gravity)
        val setting: Setting?
        if (!settings.containsKey(absGravity)) {
            setting = createSetting()
            settings[absGravity] = setting
        } else
            setting = settings[absGravity]
        setting?.scrimColor = Color.TRANSPARENT
        setting?.drawerElevation = 0f
        setting?.elevation = elevation
    }

    fun setRadius(gravity: Int, radius: Float) {
        val absGravity = getDrawerViewAbsoluteGravity(gravity)
        val setting: Setting?
        if (!settings.containsKey(absGravity)) {
            setting = createSetting()
            settings[absGravity] = setting
        } else
            setting = settings[absGravity]
        setting?.radius = radius
    }

    override fun setDrawerElevation(elevation: Float) {
        defaultDrawerElevation = elevation
        super.setDrawerElevation(elevation)
    }

    override fun setScrimColor(@ColorInt color: Int) {
        defaultScrimColor = color
        super.setScrimColor(color)
    }

    fun useCustomBehavior(gravity: Int) {
        val absGravity = getDrawerViewAbsoluteGravity(gravity)
        if (!settings.containsKey(absGravity))
            settings[absGravity] = createSetting()

    }

    override fun openDrawer(drawerView: View, animate: Boolean) {
        super.openDrawer(drawerView, animate)
        post { updateSlideOffset(drawerView, if (isDrawerOpen(drawerView)) 1f else 0f) }
    }

    private fun updateSlideOffset(drawerView: View, slideOffset: Float) {

        val horizontalGravity = getDrawerViewAbsoluteGravity(GravityCompat.START)
        val childAbsGravity = getDrawerViewAbsoluteGravity(drawerView)

        val isRtl: Boolean = layoutDirection == View.LAYOUT_DIRECTION_RTL || activity.window.decorView.layoutDirection == View.LAYOUT_DIRECTION_RTL || resources.configuration.layoutDirection == View.LAYOUT_DIRECTION_RTL
        for (i in 0 until frameLayout!!.childCount) {
            val child = frameLayout!!.getChildAt(i) as CardView
            val setting = settings[childAbsGravity]
            var adjust: Float
            if (setting != null) {
                if (drawerView.background is ColorDrawable) {
                    val bgColor = (drawerView.background as ColorDrawable).color
                    activity.window.decorView.setBackgroundColor(bgColor)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        systemUiVisibility = if (ColorUtils.calculateContrast(Color.WHITE,bgColor) < contrastThreshold && slideOffset > 0.4)
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        else
                            0
                    }
                } else if (drawerView.background is MaterialShapeDrawable && (drawerView.background as MaterialShapeDrawable).fillColor != null) {

                    val bgColor =
                        (drawerView.background as MaterialShapeDrawable).fillColor!!.defaultColor
                    activity.window.decorView.setBackgroundColor(bgColor)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        systemUiVisibility = if (ColorUtils.calculateContrast(Color.WHITE, bgColor) < contrastThreshold && slideOffset > 0.4) View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR else 0
                    }
                }
                child.radius = (((setting.radius * slideOffset) * 2.0).roundToInt() / 2).toFloat()
                super.setScrimColor(setting.scrimColor)
                super.setDrawerElevation(setting.drawerElevation)
                val percentage = 1f - setting.percentage
                ViewCompat.setScaleY(child, 1f - percentage * slideOffset)
                child.cardElevation = setting.elevation * slideOffset
                adjust = setting.elevation
                val isLeftDrawer =
                    if (isRtl)
                        childAbsGravity != horizontalGravity
                    else
                        childAbsGravity == horizontalGravity

                val width = if (isLeftDrawer)
                    drawerView.width + adjust
                else
                    -drawerView.width - adjust

                updateSlideOffset(child, setting, width, slideOffset, isLeftDrawer)
            } else {
                super.setScrimColor(defaultScrimColor)
                super.setDrawerElevation(defaultDrawerElevation)
            }
        }
    }


    private fun getActivity(context: Context)= (context as Activity)

    open fun updateSlideOffset(
        child: CardView,
        setting: Setting?,
        width: Float,
        slideOffset: Float,
        isLeftDrawer: Boolean
    ) {
        child.x = width * slideOffset
        if (slideOffset>0.5){
            if (_isOpen) {
                _isOpen = false
                colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
                colorAnimation.duration = 250
                colorAnimation.addUpdateListener { changeStatusBarColor(it.animatedValue as Int) }
                colorAnimation.start()
            }
            if (slideOffset>0.9){
                colorFrom = activity.getCompactColor(R.color.colorSoftBlue)
                colorTo = activity.getCompactColor(R.color.white)
            }
        }else if (slideOffset<0.5){
            if (!_isOpen) {
                _isOpen = true
                colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
                colorAnimation.duration = 250
                colorAnimation.addUpdateListener { changeStatusBarColor(it.animatedValue as Int) }
                colorAnimation.start()
            }
            if ((slideOffset<0.1)) {
                colorFrom = activity.getCompactColor(R.color.white)
                colorTo = activity.getCompactColor(R.color.colorSoftBlue)
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (drawerView != null) updateSlideOffset(
            drawerView!!,
            if (isDrawerOpen(drawerView!!)) 1f else 0f
        )
    }

    private fun getDrawerViewAbsoluteGravity(gravity: Int) =
        GravityCompat.getAbsoluteGravity(gravity,ViewCompat.getLayoutDirection(this)) and GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK

    private fun getDrawerViewAbsoluteGravity(drawerView: View): Int {
        val gravity = (drawerView.layoutParams as LayoutParams).gravity
        return getDrawerViewAbsoluteGravity(gravity)
    }

    open fun createSetting()=Setting()

    open inner class Setting {
        var percentage = 1f
        var scrimColor = defaultScrimColor
        var elevation = 0f
        var drawerElevation = defaultDrawerElevation
        var radius = 0f

    }

}