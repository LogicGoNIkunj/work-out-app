package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityWorkoutDetailBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.WRAP_CONTENT
import homeworkout.sixpackabs.workoutfitness.extension.gone
import homeworkout.sixpackabs.workoutfitness.extension.showToast
import homeworkout.sixpackabs.workoutfitness.extension.visible
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class WorkoutListDetailsActivity : AppCompatActivity(R.layout.activity_workout_detail) {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var common: Common

    private var currentIndex = 0
    private var workoutDetailType = ""
    private var workoutCategoryData: ArrayList<WorkOutDetail> = arrayListOf()

    private lateinit var binding: ActivityWorkoutDetailBinding

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityWorkoutDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        admobNative(findViewById(R.id.admob_native_ll))
        val extra = intent.getSerializableExtra(Constant.CONS_WORKOUT_LIST_ARRAY)
        extra?.let {
            workoutCategoryData = it as ArrayList<WorkOutDetail>
            currentIndex = intent.getIntExtra(Constant.CONS_WORKOUT_LIST_POSITION, 0)

            workoutDetailType = intent.getStringExtra(Constant.CONS_WORKOUT_DETAIL_TYPE) ?: ""
            initView()
        }
    }

    private fun initView() {
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        val (descriptions, _, _, _, title) = workoutCategoryData[currentIndex]

        binding.txtWorkoutTitle.text = title
        val property = System.getProperty("line.separator")

        binding.txtWorkoutDetails.text =
            descriptions.replace("\\n", property!!, false).replace("\\r", "", false)
        binding.imgWorkoutDemo.removeAllViews()
        val assetItems = common.getAssetItems(common.replaceSpacialCharacters(title))

        for (index in assetItems.indices) {
            val imageView = ImageView(this@WorkoutListDetailsActivity)
            Glide.with(this@WorkoutListDetailsActivity).load(assetItems[index]).into(imageView)
            val layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            layoutParams.gravity = GravityCompat.START
            imageView.layoutParams = layoutParams
            binding.imgWorkoutDemo.apply {
                addView(imageView)
                isAutoStart = true
                flipInterval =
                    this@WorkoutListDetailsActivity.resources.getInteger(R.integer.view_flipper_animation)
                startFlipping()
            }
        }

    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.ivVideo -> {
                try {
                    if (workoutCategoryData[currentIndex].workoutVideoLink.isNotEmpty()) {
                        common.openYoutube(workoutCategoryData[currentIndex].workoutVideoLink)
                        return
                    }
                    showToast(getString(R.string.error_video_not_exist))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                    showToast("Youtube player not available on this device")
                }
            }
        }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.none, R.anim.slide_down)
    }

    private fun admobNative(admob_native_ll: FrameLayout) {
        val builder = AdLoader.Builder(this, HomeWorkoutApplication.get_Admob_native_Id())
            .forNativeAd { nativeAd: NativeAd ->
                @SuppressLint("InflateParams") val adView =
                    this.layoutInflater.inflate(R.layout.native_ads_small, null) as NativeAdView
                if (isDestroyed || isFinishing || isChangingConfigurations) {
                    nativeAd.destroy()
                    return@forNativeAd
                }
                binding.tvSpaceForAd.gone()
                populateUnifiedNativeAdView(nativeAd, adView)
                admob_native_ll.removeAllViews()
                admob_native_ll.addView(adView)
            }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                binding.tvSpaceForAd.visible()
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (Objects.requireNonNull(adView.headlineView) as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            Objects.requireNonNull(adView.bodyView).visibility = View.INVISIBLE
        } else {
            Objects.requireNonNull(adView.bodyView).visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            Objects.requireNonNull(adView.callToActionView).visibility = View.INVISIBLE
        } else {
            Objects.requireNonNull(adView.callToActionView).visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            Objects.requireNonNull(adView.iconView).visibility = View.GONE
        } else {
            (Objects.requireNonNull(adView.iconView) as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }
}