package homeworkout.sixpackabs.workoutfitness.ui.adapter


import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ItemBodyChallengeBinding
import homeworkout.sixpackabs.workoutfitness.extension.startActivity
import homeworkout.sixpackabs.workoutfitness.ui.activity.DayStatusActivity
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import kotlin.math.roundToInt

class BodyChallengeAdapter : RecyclerView.Adapter<BodyChallengeAdapter.Holder>() {

    var fullBodyCount = 0
    var lowerBodyCount = 0

    inner class Holder(val binding: ItemBodyChallengeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context


        init {
            binding.root.setOnClickListener {
                context.startActivity<DayStatusActivity> {
                    Constant.categoryLevel = Constant.CONS_FULL_BODY_DATA
                    if (adapterPosition == 0) {
                        Constant.categoryName = Constant.CONS_FULL_BODY
                        putExtra(Constant.CONS_CATEGORY_NAME, Constant.CONS_FULL_BODY)
                    } else {
                        Constant.categoryName = Constant.CONS_LOWER_BODY
                        putExtra(Constant.CONS_CATEGORY_NAME, Constant.CONS_LOWER_BODY)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(ItemBodyChallengeBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (position == 0) {
            val format =
                String.format("%.1f", fullBodyCount.toFloat() * 100.toFloat() / 28.toFloat())
            holder.binding.tvRemainDays.text = "${(28 - fullBodyCount)} Days Left"
            holder.binding.tvDayInPercentage.text = "${format.toDouble().roundToInt()}%"
            holder.binding.progressBar.progress = format.toFloat().toInt()
            holder.binding.tvTitle.text = "${holder.context.getString(R.string.full_body)} "
            holder.binding.ivImage.setImageResource(R.drawable.img_full_body)
        } else if (position == 1) {
            val format =
                String.format("%.1f", lowerBodyCount.toFloat() * 100.toFloat() / 28.toFloat())
            holder.binding.tvRemainDays.text = "${(28 - lowerBodyCount)} Days Left"
            holder.binding.tvDayInPercentage.text = "${format.toDouble().roundToInt()}%"
            holder.binding.progressBar.progress = format.toFloat().toInt()
            holder.binding.tvTitle.text = holder.context.getString(R.string.lower_body)
            holder.binding.ivImage.setImageResource(R.drawable.img_lower_body)
        }

    }

    override fun getItemCount() = 2

}