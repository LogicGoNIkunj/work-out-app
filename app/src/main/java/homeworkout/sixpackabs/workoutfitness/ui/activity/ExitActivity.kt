package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.utils.FeedbackDialogFragment
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication
import java.util.*

@AndroidEntryPoint
class ExitActivity : AppCompatActivity(R.layout.exit_activity) {

    private var frmlay: FrameLayout? = null
    private var nativeAds: NativeAd? = null
    private var resultrate: TextView? = null
    private var rateStat: RatingBar? = null
    private var answerValue = 0
    private var sharedPreferences: SharedPreferences? = null
    private var rating_lin: LinearLayout? = null
    var ads_space: ImageView? = null
    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        window.setBackgroundDrawable(null)
        sharedPreferences = getSharedPreferences("rating", MODE_PRIVATE)
        init()
        RefreshAd()
        click()
    }


    private fun init() {
        frmlay = findViewById(R.id.frmlay)
        rateStat = findViewById(R.id.ratestar)
        resultrate = findViewById(R.id.resultrate)
        rating_lin = findViewById(R.id.rate_linear)
        ads_space = findViewById(R.id.card_exit_banner)
        findViewById<View>(R.id.exit_card).setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    @SuppressLint("SetTextI18n")
    fun click() {
        findViewById<View>(R.id.no).setOnClickListener { finish() }
        findViewById<View>(R.id.exit).setOnClickListener {
            HomeWorkoutApplication.appOpenManager?.isAdShow = true
            finishAffinity()
        }
        rateStat!!.onRatingBarChangeListener =
            OnRatingBarChangeListener { ratingBar: RatingBar?, rating: Float, fromUser: Boolean ->
                answerValue = rateStat!!.rating.toInt()
                when (answerValue) {
                    1 -> {
                        resultrate!!.text = "Bad"
                        feedback()
                    }
                    2 -> {
                        resultrate!!.text = "GOOD"
                        feedback()
                    }
                    3 -> {
                        resultrate!!.text = "Superb"
                        feedback()
                    }
                    4 -> {
                        resultrate!!.text = "Good Job"
                        val str = "android.intent.action.VIEW"
                        try {
                            startActivity(
                                Intent(
                                    str,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                )
                            )
                        } catch (unused: ActivityNotFoundException) {
                            startActivity(
                                Intent(
                                    str,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                )
                            )
                        }
                        val editor1: SharedPreferences.Editor = sharedPreferences!!.edit()
                        editor1.putBoolean("ratePref", false)
                        editor1.apply()
                        rating_lin!!.visibility = View.GONE
                    }
                    5 -> {
                        resultrate!!.text = "Awesome"
                        val str = "android.intent.action.VIEW"
                        try {
                            startActivity(
                                Intent(
                                    str,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                )
                            )
                        } catch (unused: ActivityNotFoundException) {
                            startActivity(
                                Intent(
                                    str,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                )
                            )
                        }
                        val editor1: SharedPreferences.Editor = sharedPreferences!!.edit()
                        editor1.putBoolean("ratePref", false)
                        editor1.apply()
                        rating_lin!!.visibility = View.GONE
                    }
                    else -> {
                        Toast.makeText(applicationContext, "No Point", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }

    override fun onResume() {
        super.onResume()
        sharedPreferences = getSharedPreferences("rating", MODE_PRIVATE)
        if (sharedPreferences?.getBoolean("ratePref", true) == true) {
            rating_lin!!.visibility = View.VISIBLE
        }
    }

    private fun feedback() {
        FeedbackDialogFragment().show(supportFragmentManager, "FeedbackDialogFragment")
    }

    private fun RefreshAd() {
        val builder = AdLoader.Builder(this, HomeWorkoutApplication.get_Admob_native_Id())
            .forNativeAd { nativeAd: NativeAd ->
                nativeAds = nativeAd
                @SuppressLint("InflateParams") val adView = layoutInflater
                    .inflate(R.layout.unifiednativead, null) as NativeAdView
                populateUnifiedNativeAdView(nativeAd, adView)
                frmlay!!.removeAllViews()
                frmlay!!.addView(adView)
                ads_space!!.visibility = View.GONE
            }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                ads_space!!.visibility = View.VISIBLE
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.mediaView = mediaView
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (Objects.requireNonNull(adView.headlineView) as TextView).text =
            nativeAd.headline
        if (nativeAd.body == null) {
            Objects.requireNonNull(adView.bodyView).visibility = View.INVISIBLE
        } else {
            Objects.requireNonNull(adView.bodyView).visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            Objects.requireNonNull(adView.callToActionView).visibility =
                View.INVISIBLE
        } else {
            Objects.requireNonNull(adView.callToActionView).visibility =
                View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            Objects.requireNonNull(adView.iconView).visibility = View.GONE
        } else {
            (Objects.requireNonNull(adView.iconView) as ImageView).setImageDrawable(
                nativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (nativeAds != null) {
            nativeAds!!.destroy()
        }
    }
}