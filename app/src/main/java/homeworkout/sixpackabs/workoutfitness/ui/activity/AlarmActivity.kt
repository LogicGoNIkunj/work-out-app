package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.interfaces.OnReminderListener
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderDatabase
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderModel
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderReceiver
import homeworkout.sixpackabs.workoutfitness.ui.adapter.ReminderAdapter
import homeworkout.sixpackabs.workoutfitness.utils.HomeWorkoutApplication
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class AlarmActivity : AppCompatActivity(R.layout.activity_reminder), OnReminderListener {

    private var date = ""
    private var day = 0
    private var hour = 0
    private var minute = 0
    private var month = 0
    private var time = ""
    private var year = 0
    private var currentCalendar = Calendar.getInstance()
    private var reminderId = ""
    private var alertDialog: AlertDialog? = null

    private val reminderAdapter: ReminderAdapter by lazy { ReminderAdapter() }

    @Inject
    lateinit var reminderDatabase: ReminderDatabase

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        admobNative(findViewById(R.id.admob_native_ll))
        initView()
        setupReminderDatabase()
    }

    private fun setupReminderDatabase() {

        hour = currentCalendar.get(Calendar.HOUR_OF_DAY)
        minute = currentCalendar.get(Calendar.MINUTE)
        year = currentCalendar.get(Calendar.YEAR)
        month = currentCalendar.get(Calendar.MONTH) + 1
        day = currentCalendar.get(Calendar.DATE)
        date = "$day/$month/$year"
        time = "$hour:$minute"
        val dateSplit = Regex("/").split(date, 0).toTypedArray()
        val timeSplit = Regex(":").split(time, 0).toTypedArray()
        day = dateSplit[0].toInt()
        month = dateSplit[1].toInt()
        year = dateSplit[2].toInt()
        hour = timeSplit[0].toInt()
        minute = timeSplit[1].toInt()
    }

    private fun initView() {
        findViewById<Toolbar>(R.id.toolbar).setNavigationOnClickListener { finish() }
        reminderAdapter.reminderDataBaseHelper = reminderDatabase
        reminderAdapter.reminderListener = this
        val list = reminderDatabase.listOfAllReminders
        if (list.size > 0) {
            findViewById<AppCompatTextView>(R.id.tvNoDatFound).gone()
            findViewById<AppCompatTextView>(R.id.rcyReminderList).visible()
            reminderAdapter.listReminder = list
            findViewById<RecyclerView>(R.id.rcyReminderList).apply {
                layoutManager = LinearLayoutManager(this@AlarmActivity)
                adapter = reminderAdapter
            }
        } else {
            findViewById<AppCompatTextView>(R.id.tvNoDatFound).visible()
            findViewById<AppCompatTextView>(R.id.rcyReminderList).invisible()
        }
    }

    private fun openTimePicker(isEdit: Boolean, mHour: Int, mMinute: Int) {
        val picker = MaterialTimePicker.Builder().setHour(mHour).setMinute(mMinute)
            .setTimeFormat(TimeFormat.CLOCK_12H).setTitleText("Select Time").build()

        picker.addOnPositiveButtonClickListener {
            this.hour = picker.hour
            this.minute = picker.minute

            time = if (minute < 10) "0$hour:0$minute"
            else "$hour:$minute"


            if (isEdit) {
                val calendar = Calendar.getInstance()
                month -= 1
                calendar[Calendar.MONTH] = month
                calendar[Calendar.YEAR] = year

                if (this.hour <= currentCalendar.get(Calendar.HOUR)) if (this.minute < currentCalendar.get(
                        Calendar.MINUTE
                    )
                ) day += 1

                calendar[Calendar.DAY_OF_MONTH] = day
                calendar[Calendar.HOUR_OF_DAY] = hour
                calendar[Calendar.MINUTE] = minute
                calendar[Calendar.SECOND] = 0
                reminderDatabase.updateReminderTime(reminderId, time)

                ReminderReceiver().setAlarm(this, calendar, reminderId.toInt())
                initView()
            } else openDayPickerDialog(false, arrayListOf())

        }
        picker.show(supportFragmentManager, "MaterialTimePicker")

    }

    override fun updateDays(reminderId: String, dayList: ArrayList<String>) {
        this.reminderId = reminderId
        openDayPickerDialog(true, dayList)
        alertDialog = null
    }

    override fun updateTime(
        reminderId: String, timeList: ArrayList<String>, hour: Int, minute: Int
    ) {
        this.reminderId = reminderId
        setupReminderDatabase()
        openTimePicker(true, hour, minute)
    }

    private fun openDayPickerDialog(isEdit: Boolean, arrayList: ArrayList<String>) {

        val inflate = layoutInflater.inflate(R.layout.dl_alarm_repeat_type, null) as LinearLayout

        val chkMon: AppCompatCheckBox = inflate.findViewById(R.id.chkMon)
        val chkTue: AppCompatCheckBox = inflate.findViewById(R.id.chkTue)
        val chkWed: AppCompatCheckBox = inflate.findViewById(R.id.chkWed)
        val chkThu: AppCompatCheckBox = inflate.findViewById(R.id.chkThu)
        val chkFri: AppCompatCheckBox = inflate.findViewById(R.id.chkFri)
        val chkSat: AppCompatCheckBox = inflate.findViewById(R.id.chkSat)
        val chkSun: AppCompatCheckBox = inflate.findViewById(R.id.chkSun)

        if (isEdit) {
            chkMon.isChecked = arrayList.contains("1")
            chkTue.isChecked = arrayList.contains("2")
            chkWed.isChecked = arrayList.contains("3")
            chkThu.isChecked = arrayList.contains("4")
            chkFri.isChecked = arrayList.contains("5")
            chkSat.isChecked = arrayList.contains("6")
            chkSun.isChecked = arrayList.contains("7")
        }
        val builder = AlertDialog.Builder(this@AlarmActivity)
        builder.setMessage("Select Days")
        builder.setCancelable(true)
        val linearLayout2 = LinearLayout(this)

        chkMon.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("1") else arrayList.remove("1")
        }
        chkTue.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("2") else arrayList.remove("2")
        }
        chkWed.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("3") else arrayList.remove("3")
        }
        chkThu.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("4") else arrayList.remove("4")
        }
        chkFri.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("5") else arrayList.remove("5")
        }
        chkSat.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("6") else arrayList.remove("6")
        }
        chkSun.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) arrayList.add("7") else arrayList.remove("7")
        }

        linearLayout2.layoutParams = LinearLayout.LayoutParams(
            (resources.displayMetrics.widthPixels * 0.85).toInt(), MATCH_PARENT
        )
        linearLayout2.addView(inflate)
        builder.setView(linearLayout2)
        builder.setPositiveButton("Done") { _, _ ->
            selectDay(arrayList, isEdit, chkMon, chkTue, chkWed, chkThu, chkFri, chkSat, chkSun)
        }

        alertDialog = builder.create()
        getCompactDrawable(R.drawable.back_exercise_item)?.apply {
            this.setTint(getCompactColor(R.color.white))
            alertDialog?.window?.setBackgroundDrawable(this)
        }
        if (alertDialog?.isShowing == false) {
            alertDialog?.show()
        }
    }

    private fun selectDay(
        arrayList: ArrayList<String>,
        isEdit: Boolean,
        chkMon: AppCompatCheckBox,
        chkTue: AppCompatCheckBox,
        chkWed: AppCompatCheckBox,
        chkThu: AppCompatCheckBox,
        chkFri: AppCompatCheckBox,
        chkSat: AppCompatCheckBox,
        chkSun: AppCompatCheckBox) {
        try {

            if (isEdit) {
                reminderDatabase.updateReminderDays(this.reminderId, getDay(arrayList))
                initView()
                return
            }
            arrayList.clear()

            if (chkMon.isChecked) arrayList.add("1") else arrayList.remove("1")
            if (chkTue.isChecked) arrayList.add("2") else arrayList.remove("2")
            if (chkWed.isChecked) arrayList.add("3") else arrayList.remove("3")
            if (chkThu.isChecked) arrayList.add("4") else arrayList.remove("4")
            if (chkFri.isChecked) arrayList.add("5") else arrayList.remove("5")
            if (chkSat.isChecked) arrayList.add("6") else arrayList.remove("6")
            if (chkSun.isChecked) arrayList.add("7") else arrayList.remove("7")

            if (arrayList.size == 0) {
                showToast("Please Select at least one day")
            } else {
                val calendar = Calendar.getInstance()

                val addReminder = reminderDatabase.addReminder(ReminderModel("", date, time, "false", "1", "Day", true, getDay(arrayList)))

                month -= 1
                calendar[Calendar.MONTH] = month
                calendar[Calendar.YEAR] = year

                if (hour <= calendar[11]) if (minute < calendar[Calendar.MINUTE]) day += 1

                calendar[Calendar.DAY_OF_MONTH] = day
                calendar[Calendar.HOUR_OF_DAY] = hour
                calendar[Calendar.MINUTE] = minute
                calendar[Calendar.SECOND] = 0

                addReminder.let { ReminderReceiver().setAlarm(applicationContext, calendar, it) }
                initView()

            }

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun getDay(arrayList: ArrayList<String>): String {
        val stringBuilderDay = StringBuilder()
        for (i in arrayList.indices) {
            if (stringBuilderDay.isNotEmpty()) stringBuilderDay.append(',')
            stringBuilderDay.append(arrayList[i])
        }
        return stringBuilderDay.toString()
    }

    fun onClick(view: View) {
        if (view.id == R.id.fabAddReminder) {
            setupReminderDatabase()
            // set pm for early morning workout
            hour = 7
            minute = 0
            openTimePicker(false, hour, minute)
        }
    }

    private fun admobNative(admob_native_ll: FrameLayout) {
        val builder = AdLoader.Builder(this, HomeWorkoutApplication.get_Admob_native_Id())
            .forNativeAd { nativeAd: NativeAd ->
                @SuppressLint("InflateParams") val adView =
                    this.layoutInflater.inflate(R.layout.native_ads_small, null) as NativeAdView
                if (isDestroyed || isFinishing || isChangingConfigurations) {
                    nativeAd.destroy()
                    return@forNativeAd
                }
                populateUnifiedNativeAdView(nativeAd, adView)
                admob_native_ll.removeAllViews()
                admob_native_ll.addView(adView)
            }
        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                admob_native_ll.visibility = View.GONE
            }
        }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (Objects.requireNonNull(adView.headlineView) as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            Objects.requireNonNull(adView.bodyView).visibility = View.INVISIBLE
        } else {
            Objects.requireNonNull(adView.bodyView).visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            Objects.requireNonNull(adView.callToActionView).visibility = View.INVISIBLE
        } else {
            Objects.requireNonNull(adView.callToActionView).visibility = View.VISIBLE
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            Objects.requireNonNull(adView.iconView).visibility = View.GONE
        } else {
            (Objects.requireNonNull(adView.iconView) as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }

}