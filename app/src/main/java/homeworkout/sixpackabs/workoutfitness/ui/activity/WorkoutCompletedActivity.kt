package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityCompletedBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.extension.MATCH_PARENT
import homeworkout.sixpackabs.workoutfitness.extension.getCompactColor
import homeworkout.sixpackabs.workoutfitness.extension.getCompactDrawable
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.DialogUtils
import homeworkout.sixpackabs.workoutfitness.utils.PreferenceDataBase
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class WorkoutCompletedActivity : AppCompatActivity() {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var dialogUtils: DialogUtils

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    private var workoutList: ArrayList<WorkOutDetail> = arrayListOf()
    private var duration = 0.0

    private var strDayName = ""
    private var strWeekName = ""
    private var isWeightKG = true
    private var isDataInserted = false

    private lateinit var binding: ActivityCompletedBinding

    override fun onBackPressed() = saveData()

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityCompletedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        editorListener()
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        val tableName = intent.getStringExtra(Constant.CONS_TABLE_NAME_FROM_WORK_ACTIVITY)
        if (tableName.equals("tbl_full_body_workouts_list") || tableName.equals("tbl_lower_body_list")) {
            binding.btnDoItAgain.gone()
        }
        val extra = intent.getSerializableExtra(Constant.CONS_WORKOUT_LIST)
        if (extra != null) {
            extra.let { workoutList = it as ArrayList<WorkOutDetail> }
            strDayName = intent.getStringExtra(Constant.CONS_DAY_NAME) ?: ""
            strWeekName = intent.getStringExtra(Constant.CONS_WEEK_NAME) ?: ""
            duration = intent?.getStringExtra("Duration")?.toDouble() ?: 0.0
            binding.txtDurationTime.text =
                common.convertSecondToTime(intent?.getStringExtra("Duration")?.toInt() ?: 0)
            binding.txtTotalNoOfLevel.text = workoutList.size.toString()
            binding.txtBurnCaloriesValues.text = common.getStringFormat(duration)

            setWeight()
            healthCalculation()
        }
    }

    private fun editorListener() {
        binding.edWeight.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                try {
                    if (preferenceDataBase.getWeightUnit() == Constant.CONS_KG) preferenceDataBase.setWeight(
                        common.getStringFormat(binding.edWeight.text.toString().toDouble())
                            .toFloat()
                    )
                    else preferenceDataBase.setWeight(
                        common.getStringFormat(
                            binding.edWeight.text.toString().toDouble()
                        ).toFloat()
                    )

                    healthCalculation()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return@setOnEditorActionListener false
        }
    }

    private fun healthCalculation() {
        if (preferenceDataBase.getWeight() != 0.0f && preferenceDataBase.getFoot() != 0 && preferenceDataBase.getInch()
                .toInt() != 0
        ) {
            val bmiCalculation = common.getBmiValue(
                preferenceDataBase.getWeight(),
                preferenceDataBase.getFoot(),
                preferenceDataBase.getInch().toInt()
            )
            val format =
                String.format(Locale.getDefault(), "%.1f", *arrayOf<Any>(bmiCalculation).copyOf(1))
            val format2 = String.format(
                Locale.getDefault(),
                "%.1f",
                *arrayOf<Any>(common.calculationForBmiGraph(format.toFloat())).copyOf(1)
            )
            val format3 =
                String.format(Locale.getDefault(), "%.1f", *arrayOf<Any>(bmiCalculation).copyOf(1))
            val layoutParams = LinearLayout.LayoutParams(0, MATCH_PARENT, format2.toFloat())

            binding.txtWeightString.text = common.bmiWeightString(format3.toFloat())
            binding.txtWeightString.setTextColor(
                ColorStateList.valueOf(
                    common.getBmiWeightTextColor(
                        binding.txtWeightString.text.toString()
                    )
                )
            )
            binding.blankView1.layoutParams = layoutParams
        }
    }

    private fun setWeight() {
        if (preferenceDataBase.getWeightUnit().equals(
                Constant.CONS_KG,
                ignoreCase = true
            ) && preferenceDataBase.getWeight() != 0.0f
        ) {
            binding.edWeight.setText(
                common.getStringFormat(
                    preferenceDataBase.getWeight().toDouble()
                )
            )
            binding.txtKG.setTextColor(getCompactColor(R.color.colorWhite))
            binding.txtLB.setTextColor(getCompactColor(R.color.colorBlack))
            binding.txtKG.background = getCompactDrawable(R.drawable.ic_selected)
            binding.txtLB.background = getCompactDrawable(R.drawable.back_purple)
        } else if (preferenceDataBase.getWeightUnit() == Constant.CONS_LB && preferenceDataBase.getWeight() != 0.0f) {
            binding.edWeight.setText(
                common.getStringFormat(
                    common.convertKgToLb(
                        preferenceDataBase.getWeight().toDouble()
                    )
                )
            )
            binding.txtKG.setTextColor(getCompactColor(R.color.colorBlack))
            binding.txtLB.setTextColor(getCompactColor(R.color.colorWhite))
            binding.txtKG.background = getCompactDrawable(R.drawable.back_purple)
            binding.txtLB.background = getCompactDrawable(R.drawable.ic_selected)
        }
    }

    private fun saveData() {

        if (binding.edWeight.text.toString().isNotEmpty()) {
            val weight = common.getDecimalValue(binding.edWeight.text.toString()).toFloat()
            if (preferenceDataBase.getString(
                    Constant.CONS_KG_LB_UNIT,
                    Constant.CONS_KG
                ) == Constant.CONS_KG
            ) {
                preferenceDataBase.setWeightUnit(Constant.CONS_KG)

                if (weight.toString().isEmpty()) preferenceDataBase.setWeight(0f)
                else preferenceDataBase.setWeight(weight)

            } else {
                preferenceDataBase.setWeightUnit(Constant.CONS_LB)

                if (weight.toString().isEmpty()) preferenceDataBase.setWeight(0f)
                else preferenceDataBase.setWeight(
                    common.convertLbToKg(weight.toDouble()).roundToInt().toFloat()
                )
            }
        }
        loadData()
    }

    private fun loadData() {
        databaseHelper.insertHistory(
            Constant.categoryLevel,
            Constant.categoryName,
            common.currentDate,
            duration.roundToInt().toString(),
            common.getStringFormat(duration),
            binding.txtTotalNoOfLevel.text.toString(),
            preferenceDataBase.getWeight().toString(),
            preferenceDataBase.getFoot().toString(),
            preferenceDataBase.getInch().toString(),
            "0",
            strDayName,
            strWeekName
        ).apply {
            if (this != -1) {
                isDataInserted = true
            }
        }

        val strTableName = intent.getStringExtra(Constant.CONS_TABLE_NAME_FROM_WORK_ACTIVITY) ?: ""
        if (strTableName.isNotEmpty() && strDayName.isNotEmpty() && strWeekName.isNotEmpty()) {
            databaseHelper.updateFullWorkoutDay(strDayName, strWeekName, strTableName)
            sendBroadcast(Intent(Constant.CONS_ACTION_CHANGE_WORKOUT_DAYS))
        }
        preferenceDataBase.setLastUnCompletedExercisePosition(
            strTableName,
            workoutList[0].workoutId.toString(),
            0
        )
        finish()
    }

    override fun onStop() {
        super.onStop()
        if (isDataInserted) return

        saveData()
    }

    fun onClick(view: View) {

        when (view.id) {
            R.id.ivIcon -> onBackPressed()
            R.id.btnNext -> saveData()
            R.id.btnSaveBottom -> saveData()
            R.id.btnShare -> common.shareLink(
                this,
                "Share " + getString(R.string.app_name) + " with you",
                """ I have finish ${binding.txtTotalNoOfLevel.text} of ${getString(R.string.app_name)} exercise.
                            you should start working out at home too. You'll get results in no time! 
                            Please download the app: https://play.google.com/store/apps/details?id=$packageName"""
            )
            R.id.btnDoItAgain -> {
                startActivity<WorkoutActivity> {
                    putExtra(
                        Constant.CONS_WORKOUT_LIST,
                        intent.getSerializableExtra(Constant.CONS_WORKOUT_LIST)
                    )
                }
                saveData()
            }
            R.id.txtKG -> {
                if (isWeightKG) return

                if (binding.edWeight.text.toString().isNotEmpty()) binding.edWeight.setText(
                    common.getStringFormat(
                        common.convertLbToKg(
                            binding.edWeight.text.toString().toDouble()
                        )
                    )
                )

                preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG)
                binding.txtKG.setTextColor(getCompactColor(R.color.colorWhite))
                binding.txtLB.setTextColor(getCompactColor(R.color.colorBlack))
                binding.txtKG.background = getCompactDrawable(R.drawable.ic_selected)
                binding.txtLB.background = getCompactDrawable(R.drawable.back_purple)
                binding.edWeight.hint = Constant.CONS_KG
                isWeightKG = true
            }
            R.id.txtLB -> {
                if (!isWeightKG) return

                binding.edWeight.hint = Constant.CONS_LB
                if (binding.edWeight.text.toString().isNotEmpty()) binding.edWeight.setText(
                    common.getStringFormat(
                        common.convertKgToLb(
                            binding.edWeight.text.toString().toDouble()
                        )
                    )
                )

                preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_LB)
                binding.txtKG.setTextColor(getCompactColor(R.color.colorBlack))
                binding.txtLB.setTextColor(getCompactColor(R.color.colorWhite))
                binding.edWeight.hint = Constant.CONS_LB

                binding.txtKG.background = getCompactDrawable(R.drawable.back_purple)
                binding.txtLB.background = getCompactDrawable(R.drawable.ic_selected)
                isWeightKG = false
            }
        }
    }
}