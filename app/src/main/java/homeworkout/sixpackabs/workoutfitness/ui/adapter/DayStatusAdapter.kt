package homeworkout.sixpackabs.workoutfitness.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ItemExerciseBodyBinding
import homeworkout.sixpackabs.workoutfitness.extension.getCompactDrawable
import homeworkout.sixpackabs.workoutfitness.extension.invisible
import homeworkout.sixpackabs.workoutfitness.extension.startActivity
import homeworkout.sixpackabs.workoutfitness.extension.visible
import homeworkout.sixpackabs.workoutfitness.interfaces.OnExerciseItemClick
import homeworkout.sixpackabs.workoutfitness.model.DayModel
import homeworkout.sixpackabs.workoutfitness.ui.activity.BodyExerciseActivity
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import java.util.*

class DayStatusAdapter : RecyclerView.Adapter<DayStatusAdapter.ViewHolder>() {

    var bodyType = ""
    var listDayStatuses: ArrayList<DayModel> = arrayListOf()
        set(value) {
            field = value
            notifyItemRangeChanged(0, field.size)
        }
    var listener: OnExerciseItemClick? = null

    override fun getItemCount() = listDayStatuses.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int) =
        ViewHolder(ItemExerciseBodyBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvDay.text = "Day\n${position + 1}"
        val type =
            if (bodyType == Constant.CONS_FULL_BODY) holder.context.getString(R.string.full_body)
            else holder.context.getString(
                R.string.lower_body
            )

        holder.binding.tvExerciseName.text = "$type - EXERCISES ${position + 1}"

        if (position == 0) {
            holder.binding.tvDay.background =
                holder.context.getCompactDrawable(R.drawable.active_exercise)
            holder.binding.tvExerciseName.background =
                holder.context.getCompactDrawable(R.drawable.active_exercise)
            holder.binding.btnStart.visible()
            return
        } else if (position != 0 && listDayStatuses[position - 1].isCompleted == "1") {
            holder.binding.tvDay.background =
                holder.context.getCompactDrawable(R.drawable.active_exercise)
            holder.binding.tvExerciseName.background =
                holder.context.getCompactDrawable(R.drawable.active_exercise)
            holder.binding.btnStart.visible()
            return
        }

        if (listDayStatuses[position].isCompleted == "0") {
            holder.binding.tvDay.background =
                holder.context.getCompactDrawable(R.drawable.de_active_exercise)
            holder.binding.tvExerciseName.background =
                holder.context.getCompactDrawable(R.drawable.de_active_exercise)
            holder.binding.btnStart.invisible()
        } else {
            holder.binding.tvDay.background =
                holder.context.getCompactDrawable(R.drawable.active_exercise)
            holder.binding.tvExerciseName.background =
                holder.context.getCompactDrawable(R.drawable.active_exercise)
            holder.binding.btnStart.visible()
        }

    }

    inner class ViewHolder(val binding: ItemExerciseBodyBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context

        init {
            binding.btnStart.setOnClickListener {
                context.startActivity<BodyExerciseActivity> {
                    if (bodyType == Constant.CONS_FULL_BODY) {
                        putExtra(Constant.CONS_WORKOUT_CATEGORY_ITEM, Constant.CONS_FULL_BODY)
                        putExtra(Constant.CONS_DAY_NAME, listDayStatuses[adapterPosition].day)
                        putExtra(Constant.CONS_WEEK_NAME, listDayStatuses[adapterPosition].weekName)
                    } else {
                        putExtra(Constant.CONS_WORKOUT_CATEGORY_ITEM, Constant.CONS_LOWER_BODY)
                        putExtra(Constant.CONS_DAY_NAME, listDayStatuses[adapterPosition].day)
                        putExtra(Constant.CONS_WEEK_NAME, listDayStatuses[adapterPosition].weekName)
                    }
                }
            }
        }
    }
}

