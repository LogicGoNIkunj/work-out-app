package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityLibraryBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.interfaces.OnExerciseItemClick
import homeworkout.sixpackabs.workoutfitness.ui.adapter.WorkoutAdapter
import homeworkout.sixpackabs.workoutfitness.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class WorkoutLibraryActivity : AppCompatActivity() {

    private var mInterstitialAd: InterstitialAd? = null
    private var tableName = ""
    private var position: Int = -1
    private var isactivityleft = false

    @Inject
    lateinit var common: Common

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    private val workoutAdapter: WorkoutAdapter by lazy { WorkoutAdapter() }

    private lateinit var binding: ActivityLibraryBinding

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityLibraryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        changeStatusBarColor(R.color.colorCoffee_850)
        isactivityleft = false
        loadAds()
        workoutAdapter.common = common
        workoutAdapter.backTintColor = R.color.colorCreme_900

        setUpExerciseList()
        setUpExerciseLevel()
    }

    private fun setUpExerciseList() {
        binding.rvExercise.apply {
            layoutManager = NpaGridLayoutManager(this@WorkoutLibraryActivity, 1)
            adapter = workoutAdapter
        }

        // scrollable Description textview
        binding.layoutExercise.tvDescription.movementMethod = ScrollingMovementMethod()

        workoutAdapter.listener = object : OnExerciseItemClick {
            override fun onExerciseClick(index: Int) {
                binding.layoutExercise.txtTitle.changeTint(R.color.colorCoffee_500)
                binding.layoutExercise.btnClose.changeTint(R.color.colorCoffee_500)
                binding.layoutExercise.ivNext.changeTint(R.color.colorCoffee_500)
                binding.layoutExercise.ivPrev.changeTint(R.color.colorCoffee_500)
                binding.layoutExercise.ivYoutube.changeTint(R.color.colorCoffee_500)
                binding.rvExercise.gone()
                binding.btnStartWorkout.gone()
                binding.layoutExercise.root.visible()
                binding.layoutExercise.tvDescription.scrollTo(0, 0)
                binding.layoutExercise.btnClose.isEnabled = true
                Handler(Looper.getMainLooper()).postDelayed({
                    binding.layoutExercise.root.slideUp()
                }, 10)
                position = index

                loadExerciseItem()
            }
        }
    }

    private fun loadExerciseItem() {
        when {
            position != 0 && position < workoutAdapter.workoutDetailList.size - 1 -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorCoffee_500)
                binding.layoutExercise.ivNext.changeTint(R.color.colorCoffee_500)
            }
            position == workoutAdapter.workoutDetailList.size - 1 -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorCoffee_500)
                binding.layoutExercise.ivNext.changeTint(R.color.colorCoffee_850)
            }
            position != 0 -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorCoffee_500)
            }
            else -> {
                binding.layoutExercise.ivPrev.changeTint(R.color.colorCoffee_850)
                binding.layoutExercise.ivNext.changeTint(R.color.colorCoffee_500)
            }
        }

        val (description, _, _, _, title) = workoutAdapter.workoutDetailList[position]
        binding.layoutExercise.tvTitle.text = title

        binding.layoutExercise.tvDescription.text =
            description.replace("\\n", "\n", false).replace("\\r", "", false)
        val assetItems = common.replaceSpacialCharacters(title).let { common.getAssetItems(it) }
        binding.layoutExercise.tvCount.text =
            "${(position + 1)}/${workoutAdapter.workoutDetailList.size}"

        CoroutineScope(Dispatchers.Main).launch {
            binding.layoutExercise.imageFlipper.removeAllViews()
            for (index in assetItems.indices) {
                val imageView = ImageView(this@WorkoutLibraryActivity)
                Glide.with(this@WorkoutLibraryActivity).load(assetItems[index]).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                binding.layoutExercise.imageFlipper.addView(imageView)
            }
            if (assetItems.size == 1) {
                val imageView = ImageView(this@WorkoutLibraryActivity)
                Glide.with(this@WorkoutLibraryActivity).load(assetItems[0]).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                binding.layoutExercise.imageFlipper.addView(imageView)
            }

            binding.layoutExercise.imageFlipper.isAutoStart = true
            binding.layoutExercise.imageFlipper.flipInterval =
                resources.getInteger(R.integer.view_flipper_animation)
            binding.layoutExercise.imageFlipper.startFlipping()
        }
    }

    private fun setUpExerciseLevel(level: ExerciseLevel = ExerciseLevel.BEGINNER_LEVEL) {
        changeBackLevelTextViewTint(level)
        when (level) {
            ExerciseLevel.BEGINNER_LEVEL -> {
                CoroutineScope(Dispatchers.IO).launch {
                    tableName = Constant.CONS_TABLE_BM_EXERCISE
                    val list = databaseHelper.getWiderWorkoutDetailList(
                        Constant.CONS_TABLE_BM_EXERCISE, Constant.CONS_WIDER_LEVEL_BEGINNER
                    )
                    withContext(Dispatchers.Main) {
                        workoutAdapter.workoutDetailList = list
                        binding.tvCategoryLevel.apply {
                            text = getString(R.string.beginner)
                            startDrawable(R.drawable.ic_beginner_level)
                        }
                        binding.tvTotalWorkout.text =
                            "${workoutAdapter.workoutDetailList.size} ${getString(R.string.workouts)}"
                    }
                }
            }
            ExerciseLevel.INTERMEDIATE_LEVEL -> {
                CoroutineScope(Dispatchers.IO).launch {
                    tableName = Constant.CONS_TABLE_BM_EXERCISE
                    val list = databaseHelper.getWiderWorkoutDetailList(
                        Constant.CONS_TABLE_BM_EXERCISE, Constant.CONS_WIDER_LEVEL_MEDIUM
                    )
                    withContext(Dispatchers.Main) {
                        workoutAdapter.workoutDetailList = list
                        binding.tvCategoryLevel.apply {
                            text = getString(R.string.intermediate)
                            startDrawable(R.drawable.ic_intermediate_level)
                        }
                        binding.tvTotalWorkout.text =
                            "${workoutAdapter.workoutDetailList.size} ${getString(R.string.workouts)}"
                    }
                }
            }
            ExerciseLevel.ADVANCED_LEVEL -> {
                CoroutineScope(Dispatchers.IO).launch {
                    tableName = Constant.CONS_TABLE_BM_EXERCISE
                    val list = databaseHelper.getWiderWorkoutDetailList(
                        Constant.CONS_TABLE_BM_EXERCISE, Constant.CONS_WIDER_LEVEL_ADVANCED
                    )
                    withContext(Dispatchers.Main) {
                        workoutAdapter.workoutDetailList = list
                        binding.tvCategoryLevel.apply {
                            text = getString(R.string.advanced)
                            startDrawable(R.drawable.ic_advance_level)
                        }
                        binding.tvTotalWorkout.text =
                            "${workoutAdapter.workoutDetailList.size} ${getString(R.string.workouts)}"
                    }
                }
            }
        }
    }

    private fun changeBackLevelTextViewTint(level: ExerciseLevel = ExerciseLevel.BEGINNER_LEVEL) {
        Constant.categoryName = Constant.BUILD_WIDER
        when (level) {
            ExerciseLevel.BEGINNER_LEVEL -> {
                Constant.categoryLevel = Constant.CONS_BEGINNER
                binding.ivLevel1.changeTint(R.color.colorCoffee_500)
                binding.ivLevel2.changeTint(android.R.color.transparent)
                binding.ivLevel3.changeTint(android.R.color.transparent)
            }
            ExerciseLevel.INTERMEDIATE_LEVEL -> {
                Constant.categoryLevel = Constant.CONS_MEDIUM
                binding.ivLevel1.changeTint(android.R.color.transparent)
                binding.ivLevel2.changeTint(R.color.colorCoffee_500)
                binding.ivLevel3.changeTint(android.R.color.transparent)
            }
            ExerciseLevel.ADVANCED_LEVEL -> {
                Constant.categoryLevel = Constant.CONS_ADVANCE
                binding.ivLevel1.changeTint(android.R.color.transparent)
                binding.ivLevel2.changeTint(android.R.color.transparent)
                binding.ivLevel3.changeTint(R.color.colorCoffee_500)
            }
        }

    }


    private fun navigateUpToWorkout() {
        startActivity<WorkoutActivity> {
            putExtra(Constant.CONS_WORKOUT_LIST, workoutAdapter.workoutDetailList)
            putExtra(Constant.CONS_WORKOUT_WORK_TABLE_NAME, tableName)
        }
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btnStartWorkout -> {
                navigateUpToWorkout()

                try {
                    if (mInterstitialAd != null && !isactivityleft) {
                        mInterstitialAd?.show(this@WorkoutLibraryActivity)
                        HomeWorkoutApplication.appOpenManager?.isAdShow = true
                    }
                } catch (e: Exception) {
                }

            }
            R.id.ivLevel1 -> {
                if (position != -1) closeExerciseBottom()
                setUpExerciseLevel(ExerciseLevel.BEGINNER_LEVEL)
            }
            R.id.ivLevel2 -> {
                if (position != -1) closeExerciseBottom()
                setUpExerciseLevel(ExerciseLevel.INTERMEDIATE_LEVEL)
            }
            R.id.ivLevel3 -> {
                if (position != -1) closeExerciseBottom()
                setUpExerciseLevel(ExerciseLevel.ADVANCED_LEVEL)
            }
            R.id.ivBack -> finish()
            R.id.ivYoutube -> {
                try {
                    if (workoutAdapter.workoutDetailList[position].workoutVideoLink.isNotEmpty()) {
                        common.openYoutube(workoutAdapter.workoutDetailList[position].workoutVideoLink)
                        return
                    }
                    showToast(getString(R.string.error_video_not_exist))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                    showToast(getString(R.string.error_video_not_exist))
                }
            }
            R.id.ivPrev -> {
                if (position != 0 && position != -1) {
                    position--
                    loadExerciseItem()
                }
            }
            R.id.ivNext -> {
                if (position != workoutAdapter.workoutDetailList.size - 1) {
                    position++
                    loadExerciseItem()
                }

            }
            R.id.btnClose -> closeExerciseBottom()
        }
    }

    override fun onResume() {
        super.onResume()
        isactivityleft = false
    }

    override fun onPause() {
        super.onPause()
        this.isactivityleft = true
    }

    override fun onStop() {
        super.onStop()
        this.isactivityleft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isactivityleft = true
    }

    private fun closeExerciseBottom() {
        binding.layoutExercise.root.slideDown {
            Handler(Looper.getMainLooper()).postDelayed({
                binding.layoutExercise.btnClose.isEnabled = false
                binding.layoutExercise.root.gone()
                binding.rvExercise.visible()
                binding.btnStartWorkout.visible()
            }, 550)
        }
        position = -1
    }

    override fun onBackPressed() = finish()


    fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            HomeWorkoutApplication.get_Admob_interstitial_Id(),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd!!.fullScreenContentCallback = object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            HomeWorkoutApplication.appOpenManager?.isAdShow = false
                            mInterstitialAd = null
                            loadAds()
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            HomeWorkoutApplication.appOpenManager?.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            HomeWorkoutApplication.appOpenManager?.isAdShow = true
                        }
                    }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    HomeWorkoutApplication.appOpenManager?.isAdShow = false
                }
            })
    }

}