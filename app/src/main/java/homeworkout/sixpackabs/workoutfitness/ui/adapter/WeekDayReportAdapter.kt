package homeworkout.sixpackabs.workoutfitness.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.RowWeekDayBinding
import homeworkout.sixpackabs.workoutfitness.extension.dateToDay
import homeworkout.sixpackabs.workoutfitness.extension.getCompactColor
import homeworkout.sixpackabs.workoutfitness.utils.Common
import java.util.*

class WeekDayReportAdapter(
    private val common: Common
) :
    RecyclerView.Adapter<WeekDayReportAdapter.ViewHolder>() {

    private var listOfCurrentWeek: ArrayList<String> = common.getCurrentWeekByFirstDay()
    private val calendar = Calendar.getInstance()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(RowWeekDayBinding.inflate(LayoutInflater.from(viewGroup.context),viewGroup,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {

        holder.binding.tvDayName.text = listOfCurrentWeek[i].dateToDay().substring(0, 1)

        if (calendar.time.after(common.getStringToDate(listOfCurrentWeek[i])))
            holder.binding.tvDayName.apply {
                backgroundTintList = AppCompatResources.getColorStateList(context, R.color.colorSoftBlue)
                setTextColor(context.getCompactColor(R.color.white))
            }
        else
            holder.binding.tvDayName.apply {
                backgroundTintList = AppCompatResources.getColorStateList(context, R.color.colorGray_200)
                setTextColor(context.getCompactColor(R.color.black))
            }

    }

    override fun getItemCount() = listOfCurrentWeek.size

    inner class ViewHolder(val binding: RowWeekDayBinding) : RecyclerView.ViewHolder(binding.root)


}