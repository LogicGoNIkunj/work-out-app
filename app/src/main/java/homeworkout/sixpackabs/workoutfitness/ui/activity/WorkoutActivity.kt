package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityWorkoutBinding
import homeworkout.sixpackabs.workoutfitness.databinding.StartWorkoutRowBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.interfaces.DialogDismissCallBack
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class WorkoutActivity : AppCompatActivity() {

    private var mInterstitialAd: InterstitialAd? = null

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var dialogUtils: DialogUtils

    @Inject
    lateinit var speechToText: SpeechToText

    @Inject
    lateinit var soundUtil: SoundUtil

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    private val states =
        arrayOf(intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_enabled))

    private var workoutList: ArrayList<WorkOutDetail> = arrayListOf()
    private var dialog: Dialog? = null
    private var isReadyToGo = true

    private var isGotoVideo = false
    private var isTimerPause = false
    private val doWorkoutPagerAdapter: DoWorkoutPagerAdapter by lazy { DoWorkoutPagerAdapter() }
    private var isActivityLeft = false
    private var dayName: String = ""
    private var weekName: String = ""
    private var tableName: String = ""
    private val textToSpeech: TextToSpeech? = null
    private var timeCountDown = 0
    private var timer: Timer? = null
    private var timerTotalExercise: Timer? = null

    private var timerTask: TimerTask? = null
    private var timerTaskTotalExercise: TimerTask? = null

    private var totalSec = 0

    private lateinit var binding: ActivityWorkoutBinding

    public override fun onPause() {
        if (!isReadyToGo || !isTimerPause) {
            isTimerPause = false
        }
        isActivityLeft = true
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (!isReadyToGo) {
            if (!isGotoVideo) {
                val append =
                    StringBuilder().append("To the exercise " + workoutList[binding.viewPagerWorkout.currentItem].workoutTime + " ")
                val lowerCase =
                    workoutList[binding.viewPagerWorkout.currentItem].workoutTitle.lowercase(Locale.getDefault())
                speechToText.speechText(append.append(lowerCase).toString())
                soundUtil.playSound(0)
            }
            isGotoVideo = false
        }
        isTimerPause = false
    }

    override fun onBackPressed() = confirmToExitDialog()

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        CoroutineScope(Dispatchers.Main).launch {
            setProgress()
            delay(1000)
            binding = ActivityWorkoutBinding.inflate(layoutInflater)
            setContentView(binding.root)
            isActivityLeft = false
            loadAds()
            val extra = intent.getSerializableExtra(Constant.CONS_WORKOUT_LIST)
            extra?.let { workoutList = it as ArrayList<WorkOutDetail> }

            dayName = intent.getStringExtra(Constant.CONS_DAY_NAME) ?: ""
            weekName = intent.getStringExtra(Constant.CONS_WEEK_NAME) ?: ""
            tableName = intent.getStringExtra(Constant.CONS_WORKOUT_WORK_TABLE_NAME) ?: ""
            tableName.getExerciseColor()
            initView()
            readyToGo()
        }
    }

    private fun setProgress() {
        val progressBar = ProgressBar(this).apply {
            indeterminateDrawable.colorFilter =
                BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                    getCompactColor(R.color.colorSoftBlue),
                    BlendModeCompat.SRC_ATOP
                )
        }
        RelativeLayout(this).apply {
            gravity = Gravity.CENTER
            setBackgroundColor(getCompactColor(R.color.white))
            addView(progressBar, RelativeLayout.LayoutParams(100, 100))
            setContentView(this)
        }
    }

    private fun initView() {

        binding.imgBtnDone.changeTint(backgroundColor)
        binding.progressBarBottomWorkoutTimeStatus.apply {
            val colors =
                intArrayOf(getCompactColor(backgroundColor), getCompactColor(secondBackgroundColor))
            progressTintList = ColorStateList(states, colors)
        }

        binding.viewPagerWorkout.apply {
            adapter = doWorkoutPagerAdapter
            currentItem = preferenceDataBase.getLastUnCompletedExercisePosition(
                tableName,
                workoutList[0].workoutId.toString()
            )
        }

        if (preferenceDataBase.isSoundMute()) binding.imgSound.setImageResource(R.drawable.ic_sound_off)
        else binding.imgSound.setImageResource(R.drawable.ic_sound_on)

        binding.viewPagerWorkout.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int, positionOffset: Float, positionOffsetPixels: Int
            ) = Unit

            override fun onPageSelected(position: Int) {
                workoutSetup(position)
                if (textToSpeech != null) {

                    textToSpeech.setSpeechRate(0.5f)

                    val title = workoutList[binding.viewPagerWorkout.currentItem].workoutTitle
                    val lowerCase = title.lowercase(Locale.getDefault())
                    textToSpeech.speak(lowerCase, 0, null, null)
                }
            }

            override fun onPageScrollStateChanged(state: Int) = Unit
        })

    }

    private fun saveData() {
        if (workoutList.size > 0) {
            val id = workoutList[0].workoutId.toString()
            preferenceDataBase.setLastUnCompletedExercisePosition(
                tableName,
                id,
                binding.viewPagerWorkout.currentItem
            )
        }
    }

    private fun startWorkoutTimer(totalTime: Int) {

        binding.txtTimeCountDown.text = "$timeCountDown / $totalTime"
        timer = Timer(false)
        timerTask = object : TimerTask() {
            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    if (!isTimerPause) {
                        timeCountDown += 1

                        binding.txtTimeCountDown.text = "$timeCountDown / $totalTime"

                        binding.progressBarBottomWorkoutTimeStatus.apply {
                            max = totalTime
                            progress = timeCountDown
                            secondaryProgress = totalTime
                        }

                        if (timeCountDown == totalTime) {
                            binding.progressBarBottomWorkoutTimeStatus.progress = 0
                            timeCountDown = 0
                            timer?.cancel()
                            workoutCompleted(binding.viewPagerWorkout.currentItem + 1)
                        }
                    }
                }
            }
        }
        if (timer != null) {
            timer?.schedule(timerTask, 1000, 1000)
        }
    }

    private fun showWorkoutDetails() {
        isTimerPause = true

        startActivity<WorkoutListDetailsActivity> {
            putExtra(Constant.CONS_WORKOUT_DETAIL_TYPE, Constant.CONS_IS_WORKOUT)
            putExtra(Constant.CONS_WORKOUT_LIST_ARRAY, workoutList)
            putExtra(Constant.CONS_WORKOUT_LIST_POSITION, binding.viewPagerWorkout.currentItem)
        }
        overridePendingTransition(R.anim.slide_up, R.anim.none)
    }

    private fun workoutSetup(i: Int) {
        isTimerPause = false
        val (_, _, time, time_type) = workoutList[i]
        val timer2 = timer
        timer2?.cancel()

        if (time_type == Constant.CONS_WORKOUT_TYPE_TIME) {
            binding.groupButton.gone()
            binding.groupBottomProgress.visible()
            val timeList = time.split(":")
            val minute = timeList[0].toInt() * 60
            val second = timeList[1].toInt()

            startWorkoutTimer(minute + second)
        } else {
            binding.groupButton.visible()
            binding.groupBottomProgress.gone()
        }
    }

    fun workoutCompleted(i: Int) {

        if (binding.viewPagerWorkout.currentItem == workoutList.size - 1) {
            dialog?.show()
            nextActivityStart()
        } else {
            soundUtil.playSound(1)
            isTimerPause = true

            startActivity<DemoWorkoutActivity> {
                putExtra(Constant.CONS_WORKOUT_LIST_ARRAY, workoutList)
                putExtra(Constant.CONS_WORKOUT_LIST_INDEX, i)
            }
            overridePendingTransition(R.anim.slide_up, R.anim.none)
            binding.viewPagerWorkout.currentItem = i
        }
    }

    private fun nextActivityStart() {

        startActivity<WorkoutCompletedActivity> {
            putExtra(Constant.CONS_WORKOUT_LIST, workoutList)
            putExtra("Duration", totalSec.toString())
            putExtra(Constant.CONS_DAY_NAME, dayName)
            putExtra(Constant.CONS_WEEK_NAME, weekName)
            putExtra(
                Constant.CONS_WORKOUT_ID_FROM_WORK_ACTIVITY, workoutList[0].workoutId.toString()
            )
            putExtra(Constant.CONS_TABLE_NAME_FROM_WORK_ACTIVITY, tableName)
        }

        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd?.show(this@WorkoutActivity)
                HomeWorkoutApplication.appOpenManager?.isAdShow = true
            }
        } catch (e: Exception) {
        }

        finish()

    }

    override fun onStop() {
        saveData()
        isTimerPause = true
        isActivityLeft = true
        super.onStop()
    }

    private fun readyToGo() {
        binding.groupSkip.visible()
        binding.txtTimer.gone()
        binding.groupBottomProgress.gone()

        countDownReadyToGo()
        val append = StringBuilder().append("Ready to go start with ")
        val title = workoutList[binding.viewPagerWorkout.currentItem].workoutTitle
        val lowerCase = title.lowercase(Locale.getDefault())
        speechToText.speechText(append.append(lowerCase).toString())

    }

    private fun countDownReadyToGo() {
        var countDownTime = preferenceDataBase.getCountDownTime()

        binding.txtCountDown.text = countDownTime.toString()
        binding.dayProgressBar.apply {
            val colors =
                intArrayOf(getCompactColor(backgroundColor), getCompactColor(secondBackgroundColor))
            progressTintList = ColorStateList(states, colors)
            max = countDownTime
            progress = countDownTime
            secondaryProgress = countDownTime
        }

        timer = Timer(false)
        timer?.schedule(object : TimerTask() {
            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    if (!isTimerPause) {
                        countDownTime--
                        binding.txtCountDown.text = countDownTime.toString()
                        binding.dayProgressBar.progress = countDownTime
                        if (countDownTime == 0) startExercise()
                        else if (countDownTime < 4) speechToText.speechText(countDownTime.toString())
                    }
                }
            }
        }, 1000, 1000)
    }

    fun startExercise() {
        isReadyToGo = false
        binding.groupSkip.gone()
        binding.txtTimer.visible()
        binding.groupBottomProgress.visible()

        timer?.cancel()
        initView()
        countTotalTime()
        workoutSetup(binding.viewPagerWorkout.currentItem)
        onResume()
    }

    private fun countTotalTime() {
        timerTotalExercise = Timer(false)
        timerTaskTotalExercise = object : TimerTask() {
            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    if (!isTimerPause) {
                        totalSec++
                        binding.txtTimer.text = common.convertSecondToTime(totalSec)
                    }
                }
            }
        }
        if (timerTotalExercise != null) timerTotalExercise?.schedule(
            timerTaskTotalExercise, 1000L, 1000L
        )
    }

    private fun confirmToExitDialog() {
        isTimerPause = true
        val dialog = Dialog(this)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_exercise_exit)

        dialog.window?.setLayout((resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(getCompactDrawable(R.drawable.back_exercise_item))
        dialog.findViewById<Button>(R.id.btnQuite).changeTint(backgroundColor)
        dialog.findViewById<Button>(R.id.btnContinue).changeTint(backgroundColor)
        dialog.findViewById<ImageButton>(R.id.ivClose).setOnClickListener {
            isTimerPause = false
            dialog.dismiss()
        }
        dialog.findViewById<Button>(R.id.btnQuite).setOnClickListener {
            quiteData(dialog)
            finish()
        }
        dialog.findViewById<Button>(R.id.btnContinue).setOnClickListener {
            isTimerPause = false
            dialog.dismiss()
        }
        dialog.setOnCancelListener { isTimerPause = false }
        dialog.show()
    }

    private fun quiteData(dialog: Dialog) {
        saveData()
        dialog.dismiss()
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> onBackPressed()
            R.id.btnSkip -> startExercise()
            R.id.imgBtnPause -> showWorkoutDetails()
            R.id.imgInfo -> showWorkoutDetails()
            R.id.imgSound -> {
                isTimerPause = true
                dialogUtils.soundOptionDialog(
                    this,
                    view as AppCompatImageView,
                    object : DialogDismissCallBack {
                        override fun onDismiss(value: Any?) {
                            isTimerPause = false
                        }
                    })
            }
            R.id.imgBtnDone -> workoutCompleted(binding.viewPagerWorkout.currentItem + 1)
            R.id.imgBtnNext -> workoutCompleted(binding.viewPagerWorkout.currentItem + 1)
            R.id.imgBtnPrev -> {
                if (binding.viewPagerWorkout.currentItem != 0)
                    workoutCompleted(binding.viewPagerWorkout.currentItem - 1)
            }
            R.id.imgVideo -> {
                isGotoVideo = true
                isTimerPause = true
                common.openYoutube(workoutList[binding.viewPagerWorkout.currentItem].workoutVideoLink)
            }
        }
    }

    fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            HomeWorkoutApplication.get_Admob_interstitial_Id(),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd!!.fullScreenContentCallback = object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            HomeWorkoutApplication.appOpenManager?.isAdShow = false
                            mInterstitialAd = null
                            loadAds()
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            HomeWorkoutApplication.appOpenManager?.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            HomeWorkoutApplication.appOpenManager?.isAdShow = true
                        }
                    }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    HomeWorkoutApplication.appOpenManager?.isAdShow = false
                }
            })
    }


    inner class DoWorkoutPagerAdapter : PagerAdapter() {
        override fun isViewFromObject(view: View, obj: Any) = view === obj
        override fun getCount(): Int = workoutList.size

        override fun instantiateItem(viewGroup: ViewGroup, i: Int): Any {
            val (_, _, time, time_type, title) = workoutList[i]
            val binding = StartWorkoutRowBinding.inflate(
                LayoutInflater.from(this@WorkoutActivity),
                viewGroup,
                false
            )

            if (isReadyToGo) {
                binding.txtWorkoutTitle.text = ""
                binding.txtWorkoutDetails.text = ""
            } else {
                if (time_type == Constant.CONS_WORKOUT_TYPE_STEP) binding.txtWorkoutTitle.text =
                    "x $time"
                else binding.txtWorkoutTitle.text = time

                binding.txtWorkoutDetails.text = title
            }
            binding.flipperWorkout.removeAllViews()
            val assetItems = common.getAssetItems(common.replaceSpacialCharacters(title))
            for (i2 in assetItems.indices) {
                val imageView = ImageView(this@WorkoutActivity)
                Glide.with(this@WorkoutActivity).load(assetItems[i2]).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                binding.flipperWorkout.addView(imageView)
            }
            binding.flipperWorkout.apply {
                isAutoStart = true
                flipInterval =
                    this@WorkoutActivity.resources.getInteger(R.integer.view_flipper_animation)
                startFlipping()
            }
            viewGroup.addView(binding.root)
            return binding.root
        }

        override fun destroyItem(viewGroup: ViewGroup, i: Int, obj: Any) =
            viewGroup.removeView(obj as RelativeLayout)
    }

    companion object {
        var backgroundColor = 0
        var secondBackgroundColor = 0
    }


    override fun onDestroy() {
        super.onDestroy()
        isActivityLeft = true
    }


}
