package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ActivityDaysStatusBinding
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.changeStatusBarColor
import homeworkout.sixpackabs.workoutfitness.ui.adapter.DayStatusAdapter
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.DialogUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class DayStatusActivity : AppCompatActivity() {

    @Inject
    lateinit var databaseHelper: DataBaseHelper

    @Inject
    lateinit var common: Common

    @Inject
    lateinit var dialogUtils: DialogUtils

    private lateinit var binding: ActivityDaysStatusBinding

    private val dayStatusAdapter: DayStatusAdapter by lazy { DayStatusAdapter() }

    private var categoryName: String = ""

    public override fun onResume() {
        super.onResume()
        initView()
    }

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityDaysStatusBinding.inflate(layoutInflater)
        setContentView(binding.root)

        changeStatusBarColor(R.color.colorYellow_200)
        categoryName = intent.getStringExtra(Constant.CONS_CATEGORY_NAME) ?: ""
        (findViewById<TextView>(R.id.tvTitle)).text = categoryName
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        binding.toolbar.setNavigationOnClickListener { finish() }

        if (categoryName == Constant.CONS_FULL_BODY) {
            dayStatusAdapter.bodyType = Constant.CONS_FULL_BODY
            val fullBodyCount = databaseHelper.getWorkoutWeekCompletedDayCount(Constant.CONS_FULL_BODY)
            val format = String.format("%.1f", fullBodyCount.toFloat() * 100.toFloat() / 28.toFloat())
            binding.tvTitle.text = getString(R.string.full_body)
            binding.tvRemainDays.text = "${(28 - fullBodyCount)} Days Left"
            binding.tvDayInPercentage.text = "${format.toDouble().roundToInt()}%"
            binding.progressBar.progress = format.toFloat().toInt()
            binding.ivImage.setImageResource(R.drawable.img_full_body)
        } else {
            dayStatusAdapter.bodyType =  Constant.CONS_LOWER_BODY
            val lowerBodyCount = databaseHelper.getWorkoutWeekCompletedDayCount(Constant.CONS_LOWER_BODY)
            val format = String.format("%.1f", lowerBodyCount.toFloat() * 100.toFloat() / 28.toFloat())
            binding.tvTitle.text = getString(R.string.lower_body)
            binding.tvRemainDays.text = "${(28 - lowerBodyCount)} Days Left"
            binding.tvDayInPercentage.text = "${format.toDouble().roundToInt()}%"
            binding.progressBar.progress = format.toFloat().toInt()
            binding.ivImage.setImageResource(R.drawable.img_lower_body)
        }

        CoroutineScope(Dispatchers.IO).launch {
           val list = databaseHelper.getWorkoutWeekFromCategory(categoryName)
            withContext(Dispatchers.Main) {
                dayStatusAdapter.listDayStatuses = list
                binding.rvExercise.adapter = dayStatusAdapter
            }
        }
    }


    override fun onBackPressed() {
        finish()
    }
}