package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.BuildConfig
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.extension.showToast
import homeworkout.sixpackabs.workoutfitness.extension.startAction
import homeworkout.sixpackabs.workoutfitness.extension.startActivity
import homeworkout.sixpackabs.workoutfitness.interfaces.OnMetricUnitDialogListener
import homeworkout.sixpackabs.workoutfitness.utils.*
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class SettingActivity : AppCompatActivity(R.layout.activity_setting) {

    @Inject
    lateinit var dialogUtils: DialogUtils

    @Inject
    lateinit var speechToText: SpeechToText

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    var sharedPreferences: SharedPreferences? = null
    var answerValue = 0


    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        sharedPreferences = getSharedPreferences("rating", MODE_PRIVATE)
        initView()
    }

    private fun initView() {
        (findViewById<TextView>(R.id.tvWeight)).text = preferenceDataBase.getWeightUnit()
        (findViewById<TextView>(R.id.tvHeight)).text = preferenceDataBase.getHeightUnit()
        (findViewById<TextView>(R.id.tvCountDown)).text =
            String.format(Locale.getDefault(), "%d secs", preferenceDataBase.getCountDownTime())
        (findViewById<TextView>(R.id.tvRestTime)).text =
            String.format(Locale.getDefault(), "%d secs", preferenceDataBase.getRestTime())
        (findViewById<Switch>(R.id.swtKeepScreenOn)).isChecked = preferenceDataBase.isOnKeepScreen()
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBack -> finish()
            R.id.rlRateUs -> if (sharedPreferences?.getBoolean("ratePref", true) == true) {
                reatingdialog()
            } else {
                Toast.makeText(
                    this@SettingActivity,
                    "You have already rated this application! ",
                    Toast.LENGTH_SHORT
                ).show()
            }
            R.id.rlWeight -> dlUnitDialog(true)
            R.id.rlHeight -> dlUnitDialog(false)
            R.id.rlFeedBack -> FeedbackDialogFragment().show(
                supportFragmentManager,
                "FeedbackDialogFragment"
            )
            R.id.rlPrivacyPolicy -> common.openUrl(this, getString(R.string.privacy_policy_link))
            R.id.rlSoundOption -> dialogUtils.soundOptionDialog(this, null, null)
            R.id.rlKeepScreenOn -> preferenceDataBase.setKeepScreen((view as Switch).isChecked)
            R.id.rlCountDown -> dialogUtils.showDurationDialog(
                this,
                Constant.CONS_COUNT_DOWN_TIME,
                findViewById(R.id.tvCountDown)
            )
            R.id.rlRestSet -> dialogUtils.showDurationDialog(
                this,
                Constant.CONS_REST_SET,
                findViewById(R.id.tvRestTime)
            )
            R.id.rlReminder -> startActivity<AlarmActivity> { }
            R.id.rlDeviceTTSSetting -> startAction("com.android.settings.TTS_SETTINGS") {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            }
            R.id.rlTestVoice -> {
                if (common.checkVolume()) {
                    speechToText.speechTextToProfile(getString(R.string.did_you_hear_test_voice))
                    Thread.sleep(1000)
                } else
                    showToast(getString(R.string.str_increase_phone_volume))

            }
            R.id.rlShareWithFriend -> {
                common.shareLink(
                    this,
                    "",
                    "I'm training with " + getString(R.string.app_name) +
                            " and am getting great results." +
                            "\n\n" + "Here are workout for all your main muscle groups to help you build and tone muscles - no equipment needed. Challenge yourself!" +
                            "\n\n" + "Download the app:" + ("https://play.google.com/store/apps/details?id=$packageName")
                )
            }

        }
    }

    private fun dlUnitDialog(isWeight: Boolean) {
        dialogUtils.showUnitDialog(this, isWeight, object : OnMetricUnitDialogListener {
            override fun setHeightWeight(checkedId: Int) {
                if (checkedId == R.id.rbDlWeight1) {
                    if (isWeight) {
                        preferenceDataBase.setWeightUnit(Constant.CONS_LB)
                        (findViewById<TextView>(R.id.tvWeight)).text = Constant.CONS_LB
                    } else {
                        preferenceDataBase.setHeightUnit(Constant.CONS_CM)
                        (findViewById<TextView>(R.id.tvHeight)).text = Constant.CONS_CM
                    }
                } else if (checkedId == R.id.rbDlWeight2) {
                    if (isWeight) {
                        preferenceDataBase.setWeightUnit(Constant.CONS_KG)
                        (findViewById<TextView>(R.id.tvWeight)).text = Constant.CONS_KG
                    } else {
                        preferenceDataBase.setHeightUnit(Constant.CONS_IN)
                        (findViewById<TextView>(R.id.tvHeight)).text = Constant.CONS_IN
                    }
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    fun reatingdialog() {
        val dialog = Dialog(this@SettingActivity)
        dialog.setContentView(R.layout.dialog_rateapp)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = dialog.window
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val rateone = dialog.findViewById<ImageView>(R.id.rateone)
        val ratetwo = dialog.findViewById<ImageView>(R.id.ratetwo)
        val ratethree = dialog.findViewById<ImageView>(R.id.ratethree)
        val ratefour = dialog.findViewById<ImageView>(R.id.ratefour)
        val ratefive = dialog.findViewById<ImageView>(R.id.ratefive)
        val tv_submit = dialog.findViewById<TextView>(R.id.tv_submit)
        val tv_notnow = dialog.findViewById<TextView>(R.id.tv_notnow)
        rateone.setOnClickListener {
            answerValue = 1
            tv_submit.text = "Feed"
            rateone.setImageResource(R.drawable.img_rate_starprss)
            ratetwo.setImageResource(R.drawable.img_ratestar_unpress)
            ratethree.setImageResource(R.drawable.img_ratestar_unpress)
            ratefour.setImageResource(R.drawable.img_ratestar_unpress)
            ratefive.setImageResource(R.drawable.img_ratestar_unpress)
        }
        ratetwo.setOnClickListener {
            answerValue = 2
            tv_submit.text = "Feed"
            rateone.setImageResource(R.drawable.img_rate_starprss)
            ratetwo.setImageResource(R.drawable.img_rate_starprss)
            ratethree.setImageResource(R.drawable.img_ratestar_unpress)
            ratefour.setImageResource(R.drawable.img_ratestar_unpress)
            ratefive.setImageResource(R.drawable.img_ratestar_unpress)
        }
        ratethree.setOnClickListener {
            answerValue = 3
            tv_submit.text = "Feed"
            rateone.setImageResource(R.drawable.img_rate_starprss)
            ratetwo.setImageResource(R.drawable.img_rate_starprss)
            ratethree.setImageResource(R.drawable.img_rate_starprss)
            ratefour.setImageResource(R.drawable.img_ratestar_unpress)
            ratefive.setImageResource(R.drawable.img_ratestar_unpress)
        }
        ratefour.setOnClickListener {
            answerValue = 4
            tv_submit.text = "Submit"
            rateone.setImageResource(R.drawable.img_rate_starprss)
            ratetwo.setImageResource(R.drawable.img_rate_starprss)
            ratethree.setImageResource(R.drawable.img_rate_starprss)
            ratefour.setImageResource(R.drawable.img_rate_starprss)
            ratefive.setImageResource(R.drawable.img_ratestar_unpress)
        }
        ratefive.setOnClickListener {
            answerValue = 5
            tv_submit.text = "Submit"
            rateone.setImageResource(R.drawable.img_rate_starprss)
            ratetwo.setImageResource(R.drawable.img_rate_starprss)
            ratethree.setImageResource(R.drawable.img_rate_starprss)
            ratefour.setImageResource(R.drawable.img_rate_starprss)
            ratefive.setImageResource(R.drawable.img_rate_starprss)
        }
        tv_submit.setOnClickListener {
            if (answerValue != 0) {
                if (answerValue < 4) {
                    FeedbackDialogFragment().show(supportFragmentManager, "FeedbackDialogFragment")
                } else {
                    startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)))
                    val editor1: SharedPreferences.Editor = sharedPreferences!!.edit()
                    editor1.putBoolean("ratePref", false)
                    editor1.apply()
                }
                dialog.dismiss()
            } else {
                Toast.makeText(applicationContext, "Select Rating Star", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        tv_notnow.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

}
