package homeworkout.sixpackabs.workoutfitness.ui.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.DemoWorkoutActivityBinding
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.utils.*
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class DemoWorkoutActivity : AppCompatActivity() {

    private var workoutList: ArrayList<*>? = null
    private var timer: Timer? = null

    private var isTimerPause = false
    private var currentItem = 0
    private var timeCountDown = 0

    @Inject
    lateinit var speechToText: SpeechToText

    @Inject
    lateinit var preferenceDataBase: PreferenceDataBase

    @Inject
    lateinit var common: Common

    private val states = arrayOf(
        intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_enabled)
    )

    private lateinit var binding: DemoWorkoutActivityBinding

    override fun onBackPressed() {
        isTimerPause = true
        confirmToExitDialog()
    }

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = DemoWorkoutActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        admobBigNative(binding.flNativeAd)

        val extra = intent.getSerializableExtra(Constant.CONS_WORKOUT_LIST_ARRAY)?.let {
            intent.getSerializableExtra(Constant.CONS_WORKOUT_LIST_ARRAY) as ArrayList<*>
        }
        workoutList = extra as ArrayList<*>
        currentItem = intent.getIntExtra(Constant.CONS_WORKOUT_LIST_INDEX, 0)
        initView()
        startWorkoutTimer()
    }

    private fun initView() {

        val workout = workoutList?.get(currentItem) as WorkOutDetail

        binding.dayProgressBar.apply {
            val colors = intArrayOf(
                getCompactColor(WorkoutActivity.backgroundColor),
                getCompactColor(WorkoutActivity.secondBackgroundColor)
            )
            progressTintList = ColorStateList(states, colors)
            progress = preferenceDataBase.getRestTime()
            max = preferenceDataBase.getRestTime()
            secondaryProgress = preferenceDataBase.getRestTime()
        }

        val append =
            StringBuilder().append("Take a rest Next ").append(workout.workoutTime).append(' ')
        val lowerCase = workout.workoutTitle.lowercase(Locale.getDefault())
        speechToText.speechText(append.append(lowerCase).toString())

        binding.txtWorkoutTime.text =
            if (workout.workoutTimeType == Constant.CONS_WORKOUT_TYPE_STEP)
                String.format("x %s", workout.workoutTime)
            else
                workout.workoutTime

        binding.tvWorkoutName.text = workout.workoutTitle
        binding.txtSteps.text = String.format(Locale.getDefault(), "%d ", (currentItem + 1))
        binding.flipperWorkout.removeAllViews()

        val assetItems = common.getAssetItems(common.replaceSpacialCharacters(workout.workoutTitle))

        for (i in assetItems.indices) {
            val imageView = ImageView(this)
            Glide.with(this).load(assetItems[i]).into(imageView)
            imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            binding.flipperWorkout.addView(imageView)
        }
        binding.flipperWorkout.apply {
            isAutoStart = true
            flipInterval = resources.getInteger(R.integer.view_flipper_animation)
            startFlipping()
        }

    }

    private fun startWorkoutTimer() {
        timeCountDown = preferenceDataBase.getRestTime()
        binding.txtCountDown.text = timeCountDown.toString()
        timer = Timer(false)
        timer?.schedule(object : TimerTask() {
            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    try {
                        if (!isTimerPause) {
                            timeCountDown--
                            binding.txtCountDown.text = timeCountDown.toString()
                            findViewById<ProgressBar>(R.id.dayProgressBar).progress = timeCountDown
                            if (timeCountDown == 0) {
                                finish()
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }, 1000, 1000)
    }

    public override fun onStop() {
        isTimerPause = true
        super.onStop()
    }

    public override fun onResume() {
        isTimerPause = false
        super.onResume()
    }

    private fun confirmToExitDialog() {
        isTimerPause = true
        val dialog = Dialog(this)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_exercise_exit)

        dialog.window?.setLayout((resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(getCompactDrawable(R.drawable.back_exercise_item))

        dialog.findViewById<Button>(R.id.btnQuite).changeTint(WorkoutActivity.backgroundColor)
        dialog.findViewById<Button>(R.id.btnContinue).changeTint(WorkoutActivity.backgroundColor)

        dialog.findViewById<ImageButton>(R.id.ivClose).setOnClickListener {
            isTimerPause = false
            dialog.dismiss()
        }
        dialog.findViewById<Button>(R.id.btnQuite).setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
            dialog.dismiss()
        }
        dialog.findViewById<Button>(R.id.btnContinue).setOnClickListener {
            isTimerPause = false
            dialog.dismiss()
        }
        dialog.setOnCancelListener { isTimerPause = false }
        dialog.show()
    }

    private fun admobBigNative(admob_native_ll: FrameLayout) {

        val builder = AdLoader.Builder(this, HomeWorkoutApplication.get_Admob_native_Id())
            .forNativeAd { nativeAd: NativeAd ->
                @SuppressLint("InflateParams") val adView =
                    layoutInflater.inflate(R.layout.layout_big_native_ad, null) as NativeAdView
                populateUnifiedNativeAdView(nativeAd, adView)
                findViewById<AppCompatTextView>(R.id.tvSpaceForAd).gone()
                admob_native_ll.removeAllViews()
                admob_native_ll.addView(adView)
            }

        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                findViewById<AppCompatTextView>(R.id.tvSpaceForAd).visible()
            }

        }).build()

        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_CROP)
        adView.mediaView = mediaView
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.invisible()
        } else {
            adView.bodyView?.visible()
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.invisible()
        } else {
            adView.callToActionView?.visible()
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.gone()
        } else {
            (adView.iconView as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.visible()
        }
        adView.setNativeAd(nativeAd)

    }

    fun onClick(view: View) {
        if (view.id == R.id.btnSkip) {
            if (timer != null) timer?.cancel()
            finish()
            overridePendingTransition(R.anim.none, R.anim.slide_down)
        }
    }
}