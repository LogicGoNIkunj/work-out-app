package homeworkout.sixpackabs.workoutfitness.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.databinding.ItemExerciseBinding
import homeworkout.sixpackabs.workoutfitness.extension.MATCH_PARENT
import homeworkout.sixpackabs.workoutfitness.extension.changeTint
import homeworkout.sixpackabs.workoutfitness.interfaces.OnExerciseItemClick
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WorkoutAdapter : RecyclerView.Adapter<WorkoutAdapter.ViewHolder>() {

    var workoutDetailList: ArrayList<WorkOutDetail> = arrayListOf()
        set(value) {
            field.clear()
            field = value
            notifyItemRangeChanged(0, field.size)
        }
    var common: Common? = null
    var backTintColor = 0
    var listener: OnExerciseItemClick? = null


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int) =
        ViewHolder(ItemExerciseBinding.inflate(LayoutInflater.from(viewGroup.context),viewGroup,false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (_, _, time, time_type, title) = workoutDetailList[position]
        holder.binding.root.changeTint(backTintColor)
        holder.binding.txtWorkoutTitle.text = title
        holder.binding.txtWorkoutTime.text =
            if (time_type == Constant.CONS_WORKOUT_TYPE_TIME) time else "x $time"
        holder.binding.imgWorkoutDemo.removeAllViews()
        val assetItems = common?.replaceSpacialCharacters(title)?.let { common?.getAssetItems(it) }

        CoroutineScope(Dispatchers.Main).launch {
            for (index in assetItems?.indices ?: 0..0) {
                val imageView = ImageView(holder.context)
                Glide.with(holder.context).load(assetItems?.get(index)).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                holder.binding.imgWorkoutDemo.addView(imageView)
            }
            if (assetItems?.size == 1) {
                val imageView = ImageView(holder.context)
                Glide.with(holder.context).load(assetItems[0]).into(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                holder.binding.imgWorkoutDemo.addView(imageView)
            }
            holder.binding.imgWorkoutDemo.isAutoStart = true
            holder.binding.imgWorkoutDemo.flipInterval = holder.context.resources.getInteger(R.integer.view_flipper_animation)
            holder.binding.imgWorkoutDemo.startFlipping()

        }

    }

    inner class ViewHolder(val binding: ItemExerciseBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context

        init {
            binding.root.setOnClickListener {
                listener?.onExerciseClick(adapterPosition)
            }
        }
    }

    override fun getItemCount() = workoutDetailList.size

}