package homeworkout.sixpackabs.workoutfitness.model

import java.io.Serializable

data class WorkOutDetail(
    var description: String = "",
    var workoutImage: String = "",
    var workoutTime: String = "",
    var workoutTimeType: String = "",
    var workoutTitle: String = "",
    var workoutVideoLink: String = "",
    var workoutId: Int = 0
) : Serializable