package homeworkout.sixpackabs.workoutfitness.model

data class HistoryDetailModel(
    var burnCalories: String = "",
    var completionTime: String = "",
    var strDateTime: String = "",
    var strDay: String = "",
    var experienceRate: String = "",
    var heightInFeet: String = "",
    var heightInInch: String = "",
    var weightInKg: String = "",
    var strLevel: String = "",
    var strPlan: String = "",
    var totalWorkout: String = "",
    var week: String = ""
)