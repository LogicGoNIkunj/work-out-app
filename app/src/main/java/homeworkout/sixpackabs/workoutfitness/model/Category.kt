package homeworkout.sixpackabs.workoutfitness.model

data class Category(
    var category: String = "",
    var image: Int = 0,
    var id: Int=-1
)