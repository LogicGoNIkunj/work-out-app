package homeworkout.sixpackabs.workoutfitness.model

data class DayModel(
    var day: String = "",
    var isCompleted: String = "",
    var weekName: String = ""
)