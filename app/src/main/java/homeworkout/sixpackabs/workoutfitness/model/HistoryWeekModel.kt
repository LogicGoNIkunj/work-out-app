package homeworkout.sixpackabs.workoutfitness.model

import java.io.Serializable
import java.util.*

data class HistoryWeekModel(
    var historyDetailList: ArrayList<HistoryDetailModel> = ArrayList<HistoryDetailModel>(),
    var totalCalories: Int = 0,
    var totalTime: Int = 0,
    var totalWorkout: Int = 0,
    var strWeekEnd: String = "",
    var strWeekNumber: String = "",
    var strWeekStart: String = ""
) : Serializable