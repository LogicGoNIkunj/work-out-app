package homeworkout.sixpackabs.workoutfitness.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import homeworkout.sixpackabs.workoutfitness.model.DayModel
import homeworkout.sixpackabs.workoutfitness.model.HistoryDetailModel
import homeworkout.sixpackabs.workoutfitness.model.HistoryWeekModel
import homeworkout.sixpackabs.workoutfitness.model.WorkOutDetail
import homeworkout.sixpackabs.workoutfitness.utils.Common
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import homeworkout.sixpackabs.workoutfitness.utils.PreferenceDataBase
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.jvm.internal.Intrinsics

class DataBaseHelper constructor(
    val context: Context, val preferenceDataBase: PreferenceDataBase, var common: Common
) {
    private val burnCalories = "BurnKcal"
    private val completionTime = "CompletionTime"
    private val currentDate = "CurrentDate"
    private val databaseName = "HomeWorkout_.db"
    private val date = "Date"
    private val dateTime = "DateTime"
    private val dayName = "Day_name"
    private val description = "Description"
    private val experiencesRate = "FeelRate"
    private val feet = "Feet"
    private val id = "Id"
    private val inch = "Inch"
    private val isCompleted = "Is_completed"
    private val kg = "Kg"
    private val level = "Level"
    private val levelName = "LevelName"
    private val planName = "PlanName"
    private val totalWorkout = "TotalWorkout"
    private val weekName = "Week_name"
    private val weightKG = "WeightKG"
    private val weightLB = "WeightLB"
    private val weightTable = "tbl_weight"
    private val workoutId = "Workout_id"
    private val categoryName = "Category_Name"
    private val image = "Image"
    private val historyTable = "tbl_history"
    private val youtubeLinkTable = "tbl_youtube_link"
    private val tableHistory = "tbl_history"
    private val time = "Time"
    private val timeType = "time_type"
    private val title = "Title"
    private val videoLink = "videoLink"
    private val youtubeLink = "youtube_link"


    val readWriteDatabase: SQLiteDatabase
        get() {
            val databasePath = context.getDatabasePath(databaseName)
            if (!databasePath.exists()) {
                try {
                    val openOrCreateDatabase = context.openOrCreateDatabase(databaseName, 0, null)
                    openOrCreateDatabase?.close()
                    copyDatabase(databasePath)
                } catch (e: Exception) {
                    throw RuntimeException("Error creating source database", e)
                }
            }
            return SQLiteDatabase.openDatabase(databasePath.path, null, 0)
        }

    @Throws(IOException::class)
    private fun copyDatabase(file: File) {
        val open = context.assets.open(databaseName)
        val fileOutputStream = FileOutputStream(file)
        val byteArray = ByteArray(1024)
        while (open.read(byteArray) > 0) {
            fileOutputStream.write(byteArray)
        }
        fileOutputStream.flush()
        fileOutputStream.close()
        open.close()
    }

    fun getWorkOutDetails(tableName: String): ArrayList<WorkOutDetail> {
        val list = ArrayList<WorkOutDetail>()

        try {

            val rawQuery = readWriteDatabase.rawQuery("Select * From $tableName", null)
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    val workOutDetails = WorkOutDetail()
                    workOutDetails.workoutId =
                        rawQuery.getInt(rawQuery.getColumnIndexOrThrow(workoutId))
                    workOutDetails.workoutTitle =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(title))
                    workOutDetails.workoutVideoLink =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(videoLink))
                    workOutDetails.description =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(description))
                    workOutDetails.workoutTime =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(time))
                    workOutDetails.workoutTimeType =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(timeType))
                    workOutDetails.workoutImage =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(image))
                    list.add(workOutDetails)
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()
        }
        return list
    }

    fun getVideoLink(keyValue: String): String {
        var videoLink = ""

        try {
            val rawQuery = readWriteDatabase.rawQuery(
                "Select * From $youtubeLinkTable WHERE ${this.title}='$keyValue'", null
            )
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    videoLink = rawQuery.getString(rawQuery.getColumnIndexOrThrow(youtubeLink))
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) {
                rawQuery.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) {
                readWriteDatabase.close()
            }
        }
        return videoLink
    }

    fun insertHistory(
        levelName: String,
        planName: String,
        dateTime: String,
        completionTime: String,
        burnKcal: String,
        totalWorkout: String,
        kg: String,
        feet: String,
        inch: String,
        feelRate: String,
        dayName: String,
        weekName: String
    ): Int {
        val contentValues = ContentValues()
        contentValues.put(this.levelName, levelName)
        contentValues.put(this.planName, planName)
        contentValues.put(this.dateTime, dateTime)
        contentValues.put(this.completionTime, completionTime)
        contentValues.put(this.burnCalories, burnKcal)
        contentValues.put(this.totalWorkout, totalWorkout)
        contentValues.put(this.kg, kg)
        contentValues.put(this.feet, feet)
        contentValues.put(this.inch, inch)
        contentValues.put(this.experiencesRate, feelRate)
        contentValues.put(this.dayName, dayName)
        contentValues.put(this.weekName, weekName)
        return try {

            val insert = readWriteDatabase.insert(historyTable, null, contentValues).toInt()
            if (!readWriteDatabase.isOpen) {
                return insert
            }
            readWriteDatabase.close()
            insert
        } catch (e: Exception) {
            e.printStackTrace()
            0
        } finally {
            if (readWriteDatabase.isOpen) {
                readWriteDatabase.close()
            }
        }
    }

    fun isHistoryDataAvailable(date: String): Boolean {
        var isAvailable = false
        try {

            val rawQuery = readWriteDatabase.rawQuery(
                "Select * From $historyTable Where DateTime(str('%Y-%m-%d', DateTime($dateTime)))= DateTime(str('%Y-%m-%d', DateTime('$date')));",
                null
            )

            isAvailable = (rawQuery != null && rawQuery.count > 0)
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()
        } catch (e: Exception) {
        }

        return isAvailable
    }

    val historyTotalWorkoutCount: Int
        get() {
            var i = 0
            try {
                val rawQuery = readWriteDatabase.rawQuery(
                    "SELECT SUM(CAST($totalWorkout as INTEGER)) as totCompletionTime FROM $historyTable",
                    null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    rawQuery.moveToFirst()
                    i = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("totCompletionTime"))
                }
                if (rawQuery != null && !rawQuery.isClosed) {
                    rawQuery.close()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return i
        }

    val historyTotalCalories: Int
        get() {
            var burnCalories = 0.0f
            try {

                val rawQuery = readWriteDatabase.rawQuery(
                    "SELECT SUM(CAST(${this.burnCalories} as Float)) as ${this.burnCalories} FROM $historyTable",
                    null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    rawQuery.moveToFirst()
                    burnCalories =
                        rawQuery.getFloat(rawQuery.getColumnIndexOrThrow(this.burnCalories))
                }
                if (rawQuery != null && !rawQuery.isClosed) {
                    rawQuery.close()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return burnCalories.toInt()
        }

    val historyTotalMinutes: Int
        get() {

            var historyTotalMinute = 0
            try {

                val rawQuery = readWriteDatabase.rawQuery(
                    "SELECT SUM(CAST($completionTime as INTEGER)) as $completionTime FROM $historyTable",
                    null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    rawQuery.moveToFirst()
                    historyTotalMinute =
                        rawQuery.getInt(rawQuery.getColumnIndexOrThrow(completionTime))
                }
                if (rawQuery != null && !rawQuery.isClosed) {
                    rawQuery.close()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return historyTotalMinute
        }

    val completeExerciseDate: ArrayList<String>
        get() {
            val dateTimeList = ArrayList<String>()

            try {
                val rawQuery = readWriteDatabase.rawQuery("Select * from $historyTable", null)
                if (rawQuery != null && rawQuery.count > 0) {
                    while (rawQuery.moveToNext()) {

                        val string = rawQuery.getString(rawQuery.getColumnIndexOrThrow(dateTime))
                        if (!dateTimeList.contains(common.convertFullDateToDate(string))) dateTimeList.add(
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(dateTime))
                        )
                    }
                }
                if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dateTimeList
        }

    val userWeightData: ArrayList<HashMap<String, String>>
        get() {
            val arrayList = ArrayList<HashMap<String, String>>()

            try {

                val rawQuery = readWriteDatabase.rawQuery(
                    "Select * from $weightTable where $weightKG != '0' group by $date order by $date",
                    null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    while (rawQuery.moveToNext()) {
                        val hashMap = HashMap<String, String>()
                        hashMap[Constant.CONS_KG] =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(weightKG))
                        hashMap["DT"] = rawQuery.getString(rawQuery.getColumnIndexOrThrow(date))
                        arrayList.add(hashMap)
                    }
                }
                if (rawQuery != null && !rawQuery.isClosed) {
                    rawQuery.close()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return arrayList
        }


    fun weightExistOrNotFromDate(date: String): Boolean {

        var isDataAvailable = false
        try {
            val rawQuery = readWriteDatabase.rawQuery(
                "Select * From $weightTable Where ${this.date} = '$date'", null
            )
            isDataAvailable = (rawQuery != null && rawQuery.count > 0)
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return isDataAvailable
    }

    fun insertWeight(weightKG: String, date: String, weightLB: String): Int {

        val contentValues = ContentValues()
        contentValues.put(this.weightKG, weightKG)
        contentValues.put(this.date, date)
        contentValues.put(this.weightLB, weightLB)
        contentValues.put(currentDate, common.currentDate)
        try {

            val insert = readWriteDatabase.insert(weightTable, null, contentValues).toInt()
            if (!readWriteDatabase.isOpen) return insert

            readWriteDatabase.close()
            return insert
        } catch (e: Exception) {
            e.printStackTrace()
            if (readWriteDatabase.isOpen) readWriteDatabase.close()

            return -1
        } catch (th: Throwable) {
            if (readWriteDatabase.isOpen) readWriteDatabase.close()

            return -1
        }
    }

    fun updateValue(value: String, id: String, fieldName: String, tableName: String): Int {
        val contentValues = ContentValues()
        contentValues.put(fieldName, value)
        val i = readWriteDatabase.update(tableName, contentValues, "Workout_id=?", arrayOf(id))
        if (!readWriteDatabase.isOpen) {
            readWriteDatabase.close()
        }
        return i
    }

    fun updateWeight(date: String, weightKG: String, weightLB: String): Boolean {
        var i: Int

        val contentValues = ContentValues()
        contentValues.put(this.weightKG, weightKG)
        contentValues.put(this.weightLB, weightLB)
        try {

            i = readWriteDatabase.update(
                weightTable, contentValues, "${this.date} = ?", arrayOf(date)
            )
            if (!readWriteDatabase.isOpen) {
                readWriteDatabase.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (!readWriteDatabase.isOpen) {
                readWriteDatabase.close()
            }
            i = 0
        } finally {
            if (!readWriteDatabase.isOpen) {
                readWriteDatabase.close()
            }
        }
        return i > 0
    }

    val maximumWeight: String
        get() {
            var weight = "0"

            try {

                val rawQuery = readWriteDatabase.rawQuery(
                    "SELECT MAX(CAST($weightKG as INTEGER)) as maxkg from $weightTable", null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    while (rawQuery.moveToNext()) weight =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow("maxkg"))

                }
                if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (th: Throwable) {
                if (!readWriteDatabase.isOpen) readWriteDatabase.close()

            }
            return weight
        }

    val minimumWeight: String
        get() {
            var weight = "0"

            try {

                val rawQuery = readWriteDatabase.rawQuery(
                    "SELECT MIN(CAST($weightKG as INTEGER)) as minkg from $weightTable", null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    while (rawQuery.moveToNext()) weight =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow("minkg"))

                }
                if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (th: Throwable) {
                if (!readWriteDatabase.isOpen) readWriteDatabase.close()

            }
            return weight
        }

    val weekDayOfHistory: ArrayList<HistoryWeekModel>
        get() {
            val arrayList = ArrayList<HistoryWeekModel>()
            try {

                val rawQuery = readWriteDatabase.rawQuery(
                    "select strftime('%W', " + dateTime + ") " + "WeekNumber" + ',' + "    max(date(" + dateTime + ", 'weekday 0' ,'-6 day')) " + "WeekStart" + ',' + "    max(date(" + dateTime + ", 'weekday 0', '-0 day')) " + "WeekEnd" + ' ' + "from " + historyTable + ' ' + "group by " + "WeekNumber",
                    null
                )
                if (rawQuery != null && rawQuery.count > 0) {
                    while (rawQuery.moveToNext()) {
                        val historyWeekDataClass = HistoryWeekModel()
                        historyWeekDataClass.strWeekNumber =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow("WeekNumber"))
                        historyWeekDataClass.strWeekStart =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow("WeekStart"))
                        historyWeekDataClass.strWeekEnd =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow("WeekEnd"))
                        historyWeekDataClass.totalCalories = getTotalBurnCaloriesInWeek(
                            historyWeekDataClass.strWeekStart,
                            historyWeekDataClass.strWeekEnd
                        )
                        historyWeekDataClass.totalTime = getTotalWeekWorkoutTime(
                            historyWeekDataClass.strWeekStart,
                            historyWeekDataClass.strWeekEnd
                        )
                        historyWeekDataClass.historyDetailList = getWeekHistory(
                            historyWeekDataClass.strWeekStart,
                            historyWeekDataClass.strWeekEnd
                        )
                        historyWeekDataClass.totalWorkout =
                            historyWeekDataClass.historyDetailList.size
                        arrayList.add(historyWeekDataClass)
                    }
                }
                if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (th: Throwable) {
                if (!readWriteDatabase.isOpen) readWriteDatabase.close()

            }
            return arrayList
        }

    private fun getTotalBurnCaloriesInWeek(startDate: String, endDate: String): Int {

        var calories = 0
        try {

            val rawQuery = readWriteDatabase.rawQuery(
                "SELECT sum($burnCalories) as BurnKcal from $historyTable WHERE date('$startDate') <= date(datetime) AND date('$endDate') >= date(datetime)",
                null
            )
            if (rawQuery != null && rawQuery.count > 0) {
                rawQuery.moveToFirst()
                calories = rawQuery.getInt(rawQuery.getColumnIndexOrThrow(burnCalories))
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return calories
    }

    private fun getTotalWeekWorkoutTime(startDate: String, endDate: String): Int {
        var time = 0
        try {

            val rawQuery = readWriteDatabase.rawQuery(
                "SELECT sum($completionTime) as $completionTime from $historyTable WHERE date('$startDate') <= date(datetime) AND date('$endDate') >= date(datetime)",
                null
            )
            if (rawQuery != null && rawQuery.count > 0) {
                rawQuery.moveToFirst()
                time = rawQuery.getInt(rawQuery.getColumnIndexOrThrow(completionTime))
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()
        }
        return time
    }

    private fun getWeekHistory(startDate: String, endDate: String): ArrayList<HistoryDetailModel> {

        val weekHistoryList = ArrayList<HistoryDetailModel>()
        try {

            val rawQuery = readWriteDatabase.rawQuery(
                "SELECT * FROM tbl_history WHERE date('$startDate') <= date(datetime) AND date('$endDate') >= date(datetime) Order by $id Desc ",
                null
            )
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    HistoryDetailModel().apply {
                        strLevel = rawQuery.getString(rawQuery.getColumnIndexOrThrow(levelName))
                        strPlan = rawQuery.getString(rawQuery.getColumnIndexOrThrow(planName))
                        strDateTime = rawQuery.getString(rawQuery.getColumnIndexOrThrow(dateTime))
                        completionTime =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(completionTime))
                        burnCalories =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(burnCalories))
                        totalWorkout =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(totalWorkout))
                        weightInKg = rawQuery.getString(rawQuery.getColumnIndexOrThrow(kg))
                        heightInFeet = rawQuery.getString(rawQuery.getColumnIndexOrThrow(feet))
                        heightInInch = rawQuery.getString(rawQuery.getColumnIndexOrThrow(inch))
                        experienceRate =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(experiencesRate))
                        strDay = rawQuery.getString(rawQuery.getColumnIndexOrThrow(dayName))
                        week = rawQuery.getString(rawQuery.getColumnIndexOrThrow(weekName))
                        weekHistoryList.add(this)
                    }

                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return weekHistoryList
    }

    fun getWorkoutWeekFromCategory(categoryName: String): ArrayList<DayModel> {
        val weekDay = ArrayList<DayModel>()

        try {

            var query = ""
            if (categoryName == Constant.CONS_FULL_BODY) {
                query = if (isTableExists(Constant.CONS_TABLE_FULL_BODY_WORKOUT_LIST)) {
                    "SELECT  max($workoutId) as Workout_id, $workoutId, group_concat(DISTINCT(CAST($dayName as INTEGER))) as $dayName, $weekName, $isCompleted from tbl_full_body_workouts_list GROUP BY CAST($weekName as INTEGER)"
                } else {
                    readWriteDatabase.execSQL("CREATE TABLE tbl_full_body_workouts_list($workoutId INTEGER PRIMARY KEY AUTOINCREMENT,$dayName TEXT,$weekName TEXT,$isCompleted TEXT,${this.categoryName} TEXT)")
                    "SELECT  max($workoutId) as Workout_id, $workoutId, group_concat(DISTINCT(CAST($dayName as INTEGER))) as $dayName, $weekName, $isCompleted from tbl_full_body_workouts_list GROUP BY CAST($weekName as INTEGER)"
                }
            } else if (categoryName == Constant.CONS_LOWER_BODY) {
                query = if ((isTableExists(Constant.CONS_TABLE_LOWER_BODY_LIST))) {
                    "SELECT  max($workoutId) as Workout_id, $workoutId, group_concat(DISTINCT(CAST($dayName as INTEGER))) as $dayName, $weekName, $isCompleted from tbl_lower_body_list GROUP BY CAST($weekName as INTEGER)"
                } else {
                    readWriteDatabase.execSQL("CREATE TABLE tbl_lower_body_list($workoutId INTEGER PRIMARY KEY AUTOINCREMENT,$dayName TEXT,$weekName TEXT,$isCompleted TEXT,${this.categoryName} TEXT)")
                    "SELECT  max($workoutId) as Workout_id, $workoutId, group_concat(DISTINCT(CAST($dayName as INTEGER))) as $dayName, $weekName, $isCompleted from tbl_lower_body_list GROUP BY CAST($weekName as INTEGER)"
                }
            }


            val rawQuery = readWriteDatabase.rawQuery(query, null)
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    val mWeekName = rawQuery.getString(rawQuery.getColumnIndexOrThrow(weekName))
                    weekDay.addAll(
                        getWeekOfDays(
                            rawQuery.getString(
                                rawQuery.getColumnIndexOrThrow(
                                    weekName
                                )
                            ), categoryName, mWeekName
                        )
                    )
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        return weekDay
    }

    private fun getWeekOfDays(
        strWeekName: String,
        strCategoryName: String,
        mWeekName: String
    ): ArrayList<DayModel> {
        val weekOfDayList = ArrayList<DayModel>()
        try {

            var query = ""
            if (strCategoryName == Constant.CONS_FULL_BODY) {
                query =
                    "select $dayName,$isCompleted FROM tbl_full_body_workouts_list WHERE $dayName IN ('01','02','03','04','05','06','07') AND $weekName = '$strWeekName' GROUP by $dayName"
            } else if (strCategoryName == Constant.CONS_LOWER_BODY) {
                query =
                    "select $dayName,$isCompleted FROM tbl_lower_body_list WHERE $dayName IN ('01','02','03','04','05','06','07') AND $weekName = '$strWeekName' GROUP by $dayName"
            }
            val rawQuery = readWriteDatabase.rawQuery(query, null)
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    val weekDayData = DayModel()
                    weekDayData.day = rawQuery.getString(rawQuery.getColumnIndexOrThrow(dayName))
                    weekDayData.isCompleted =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(isCompleted))
                    weekDayData.weekName = mWeekName
                    weekOfDayList.add(weekDayData)
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return weekOfDayList
    }

    fun getDayExerciseFromWeek(
        strDayName: String, strWeekName: String, strTableName: String
    ): ArrayList<WorkOutDetail> {
        val time: String
        val dayExerciseList = ArrayList<WorkOutDetail>()

        try {

            val rawQuery = readWriteDatabase.rawQuery(
                "SELECT * from $strTableName WHERE $dayName = '$strDayName' AND $weekName = '$strWeekName'",
                null
            )
            time =
                if (strTableName == Constant.CONS_TABLE_FULL_BODY_WORKOUT_LIST) preferenceDataBase.getFullBodyLevel()
                else Constant.CONS_FULL_BODY_BEGINNER

            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    if (rawQuery.getString(rawQuery.getColumnIndexOrThrow(time)) != "-") {
                        val workOutDetails = WorkOutDetail()
                        workOutDetails.workoutId =
                            rawQuery.getInt(rawQuery.getColumnIndexOrThrow(workoutId))
                        workOutDetails.workoutTitle =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(title))
                        workOutDetails.workoutVideoLink =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(videoLink))
                        workOutDetails.description =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(description))
                        workOutDetails.workoutTime =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(time))
                        workOutDetails.workoutTimeType =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(timeType))
                        workOutDetails.workoutImage =
                            rawQuery.getString(rawQuery.getColumnIndexOrThrow(image))
                        dayExerciseList.add(workOutDetails)
                    }
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return dayExerciseList
    }

    fun getWorkoutWeekCompletedDayCount(strCategoryName: String): Int {

        var completedDayCount = 0
        try {

            var query = ""
            if (strCategoryName == Constant.CONS_FULL_BODY) {
                query =
                    "SELECT $workoutId, group_concat(DISTINCT(CAST($dayName as INTEGER))) as $dayName, $weekName, $isCompleted from tbl_full_body_workouts_list GROUP BY CAST($weekName as INTEGER)"
            } else if (Intrinsics.areEqual(strCategoryName, Constant.CONS_LOWER_BODY)) {
                query =
                    "SELECT $workoutId, group_concat(DISTINCT(CAST($dayName as INTEGER))) as $dayName, $weekName, $isCompleted from tbl_lower_body_list  GROUP BY CAST($weekName as INTEGER)"
            }
            val rawQuery = readWriteDatabase.rawQuery(query, null)
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    val string = rawQuery.getString(rawQuery.getColumnIndexOrThrow(weekName))
                    completedDayCount += getWeekCompletedDayList(string, strCategoryName).size
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) {
                rawQuery.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()
        }
        return completedDayCount
    }

    private fun getWeekCompletedDayList(
        strWeekName: String, strCategoryName: String
    ): ArrayList<DayModel> {

        val weekCompletedDayList = ArrayList<DayModel>()

        try {

            var query = ""
            if (strCategoryName == Constant.CONS_FULL_BODY) query =
                "select $dayName,$isCompleted FROM tbl_full_body_workouts_list WHERE $dayName IN ('01','02','03','04','05','06','07') AND $weekName = '$strWeekName' AND $isCompleted = '1' GROUP by $dayName"
            else if (strCategoryName == Constant.CONS_LOWER_BODY) query =
                "select $dayName,$isCompleted FROM tbl_lower_body_list WHERE $dayName IN ('01','02','03','04','05','06','07') AND $weekName = '$strWeekName' AND $isCompleted = '1' GROUP by $dayName"

            val rawQuery = readWriteDatabase.rawQuery(query, null)
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    val weekDayData = DayModel()
                    weekDayData.day = rawQuery.getString(rawQuery.getColumnIndexOrThrow(dayName))
                    weekDayData.isCompleted =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(isCompleted))
                    weekCompletedDayList.add(weekDayData)
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return weekCompletedDayList
    }

    fun updateFullWorkoutDay(dayName: String, weekName: String, strTableName: String): Int {
        val contentValues = ContentValues()
        contentValues.put(isCompleted, "1")
        return readWriteDatabase.update(
            strTableName,
            contentValues,
            "${this.dayName} = '$dayName' AND ${this.weekName} ='$weekName'",
            null
        )
    }

    fun getWiderWorkoutDetailList(
        workoutTable: String, difficultyLvl: String
    ): ArrayList<WorkOutDetail> {
        val widerWorkoutDetailList = ArrayList<WorkOutDetail>()

        try {

            val rawQuery = readWriteDatabase.rawQuery(
                "Select * From $workoutTable Where $level = '$difficultyLvl'", null
            )
            if (rawQuery != null && rawQuery.count > 0) {
                while (rawQuery.moveToNext()) {
                    val workOutDetails = WorkOutDetail()
                    workOutDetails.workoutId =
                        rawQuery.getInt(rawQuery.getColumnIndexOrThrow(workoutId))
                    workOutDetails.workoutTitle =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(title))
                    workOutDetails.workoutVideoLink =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(videoLink))
                    workOutDetails.description =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(description))
                    workOutDetails.workoutTime =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(time))
                    workOutDetails.workoutTimeType =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(timeType))
                    workOutDetails.workoutImage =
                        rawQuery.getString(rawQuery.getColumnIndexOrThrow(image))
                    widerWorkoutDetailList.add(workOutDetails)
                }
            }
            if (rawQuery != null && !rawQuery.isClosed) rawQuery.close()

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (!readWriteDatabase.isOpen) readWriteDatabase.close()

        }
        return widerWorkoutDetailList
    }


    private fun isTableExists(tableName: String): Boolean {

        val query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '$tableName'"
        readWriteDatabase.rawQuery(query, null).use { cursor ->
            if (cursor != null) if (cursor.count > 0) return true

            return false
        }
    }

    fun restartProgress(): Int {
        val contentValues = ContentValues()
        contentValues.put(this.isCompleted, "0")
        readWriteDatabase.update(
            Constant.CONS_TABLE_FULL_BODY_WORKOUT_LIST, contentValues, null, null
        )
        readWriteDatabase.update(Constant.CONS_TABLE_LOWER_BODY_LIST, contentValues, null, null)
        readWriteDatabase.delete(this.tableHistory, null, null)
        readWriteDatabase.delete(this.weightTable, null, null)
        return 1
    }


}