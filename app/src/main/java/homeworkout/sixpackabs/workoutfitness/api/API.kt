package homeworkout.sixpackabs.workoutfitness.api

import homeworkout.sixpackabs.workoutfitness.model.FeedbackModel
import homeworkout.sixpackabs.workoutfitness.model.Model_Ads
import homeworkout.sixpackabs.workoutfitness.model.Model_Playdata
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface API {

    @FormUrlEncoded
    @POST("get-ads-list")
    fun getAdsData(@Field("app_id") app_id: Int): Call<Model_Ads>

    @FormUrlEncoded
    @POST("feedback")
    fun sendFeedback(
        @Field("app_name") app_name: String?,
        @Field("package_name") package_name: String?,
        @Field("title") title: String?,
        @Field("description") description: String?,
        @Field("device_name") device_name: String?,
        @Field("android_version") android_version: String?,
        @Field("version") version: String?
    ): Call<FeedbackModel>

    @GET("qureka-ad")
    fun getData(): Call<Model_Playdata>
}