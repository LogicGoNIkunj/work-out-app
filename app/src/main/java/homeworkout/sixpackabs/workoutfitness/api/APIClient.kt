package homeworkout.sixpackabs.workoutfitness.api

import homeworkout.sixpackabs.workoutfitness.BuildConfig
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIClient {

    private var retrofit: Retrofit? = null

    fun getClient(): Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(Constant.FEEDBACK_BACK_URL)
                .client(OkHttpClient.Builder().addInterceptor { chain: Interceptor.Chain ->
                    chain.proceed(
                        chain.request().newBuilder()
                            .addHeader("Authorization", BuildConfig.APPLICATION_ID)
                            .addHeader("content-type", "application/json").build()
                    )
                }.connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build())
                .addConverterFactory(GsonConverterFactory.create()).build()
        }
        return retrofit
    }
}