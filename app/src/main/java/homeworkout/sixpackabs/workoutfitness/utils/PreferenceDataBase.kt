package homeworkout.sixpackabs.workoutfitness.utils

import android.content.Context
import android.content.SharedPreferences

class PreferenceDataBase(
    private val context: Context,
    private val sharedPreferences: SharedPreferences
) {

    fun clearAllData() {
        context.getSharedPreferences("home_workout_", Context.MODE_PRIVATE).edit().clear().apply()
        sharedPreferences.edit().putBoolean(Constant.CONS_LAST_COMPLETE_DAY_ID, false).apply()
        sharedPreferences.edit().clear().apply()
    }

    fun getWeightUnit() = sharedPreferences.getString(Constant.CONS_WEIGHT_UNIT, Constant.CONS_KG) ?: ""

    fun setWeightUnit(str: String) = sharedPreferences.edit().putString(Constant.CONS_WEIGHT_UNIT, str).apply()

    fun getHeightUnit() = sharedPreferences.getString(Constant.CONS_HEIGHT_UNIT, Constant.CONS_IN) ?: ""

    fun setHeightUnit(value: String) = sharedPreferences.edit().putString(Constant.CONS_HEIGHT_UNIT, value).apply()

    fun setWeight(value: Float) = sharedPreferences.edit().putFloat(Constant.CONS_LAST_INPUT_WEIGHT, value).apply()

    fun getWeight() = sharedPreferences.getFloat(Constant.CONS_LAST_INPUT_WEIGHT, 0f)

    fun setFoot(value: Int) = sharedPreferences.edit().putInt(Constant.CONS_FOOT, value).apply()

    fun getFoot() = sharedPreferences.getInt(Constant.CONS_FOOT, 0)

    fun setInch(value: Float) = sharedPreferences.edit().putFloat(Constant.CONS_LAST_INPUT_INCH, value).apply()

    fun getInch() = sharedPreferences.getFloat(Constant.CONS_LAST_INPUT_INCH, 0.0f)

    fun setSoundMute(isMute: Boolean) = sharedPreferences.edit().putBoolean(Constant.CONS_MUTE, isMute).apply()

    fun isSoundMute() = sharedPreferences.getBoolean(Constant.CONS_MUTE, false)

    fun setVoiceGuide(bool: Boolean) = sharedPreferences.edit().putBoolean(Constant.CONS_VOICE_GUIDE, bool).apply()

    fun isVoiceGuide() = sharedPreferences.getBoolean(Constant.CONS_VOICE_GUIDE, true)

    fun setCoachTips(bool: Boolean) = sharedPreferences.edit().putBoolean(Constant.CONS_COACH_TIPS, bool).apply()

    fun isCoachTips() = sharedPreferences.getBoolean(Constant.CONS_COACH_TIPS, true)

    fun setCountDownTime(time: Int) = sharedPreferences.edit().putInt(Constant.CONS_COUNTDOWN_TIME, time).apply()

    fun getCountDownTime() = sharedPreferences.getInt(Constant.CONS_COUNTDOWN_TIME, 15)

    fun setRestTime(time: Int) = sharedPreferences.edit().putInt(Constant.CONS_REST_TIME, time).apply()

    fun getRestTime() = sharedPreferences.getInt(Constant.CONS_REST_TIME, 30)

    fun setKeepScreen(bool: Boolean) = sharedPreferences.edit().putBoolean(Constant.CONS_COACH_TIPS, bool).apply()

    fun isOnKeepScreen() = sharedPreferences.getBoolean(Constant.CONS_COACH_TIPS, true)

    fun setBirthDate(date: String) = sharedPreferences.edit().putString(Constant.CONS_BIRTH_DATE, date).apply()

    fun getBirthDate() = sharedPreferences.getString(Constant.CONS_BIRTH_DATE, "1990") ?: ""

    fun setGender(gender: String) = sharedPreferences.edit().putString(Constant.CONS_GENDER, gender).apply()

    fun getGender() = sharedPreferences.getString(Constant.CONS_GENDER, "Male") ?: ""

    fun getFullBodyLevel() = sharedPreferences.getString(Constant.CONS_FULL_BODY_LEVEL, Constant.CONS_FULL_BODY_BEGINNER) ?: ""

    fun setWeekGoalDay(day: Int) = sharedPreferences.edit().putInt(Constant.CONS_WEEK_GOAL_DAYS, day).apply()

    fun getWeekGoalDay() = sharedPreferences.getInt(Constant.CONS_WEEK_GOAL_DAYS, 7)

    fun setFirstDayOfWeek(day: Int) = sharedPreferences.edit().putInt(Constant.CONS_FIRST_DAY_OF_WEEK, day).apply()

    fun getFirstDayOfWeek() = sharedPreferences.getInt(Constant.CONS_FIRST_DAY_OF_WEEK, 1)

    fun setString(key: String, value: String) = sharedPreferences.edit().putString(key, value).apply()

    fun getString(key: String, value: String) = sharedPreferences.getString(key, value) ?: ""

    fun setLastUnCompletedExercisePosition(key: String, subKey: String, value: Int) = sharedPreferences.edit().putInt("PREF_LAST_UN_COMPLETE_BEGINNER_DAY_" + key + "_" + subKey, value).apply()

    fun getLastUnCompletedExercisePosition(key: String, subKey: String) = sharedPreferences.getInt("PREF_LAST_UN_COMPLETE_BEGINNER_DAY_" + key + "_" + subKey, 0)
}