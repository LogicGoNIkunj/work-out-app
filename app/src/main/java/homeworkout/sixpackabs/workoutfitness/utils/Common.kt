package homeworkout.sixpackabs.workoutfitness.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.extension.getCompactColor
import java.io.IOException
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.pow

class Common(
    private var context: Context, private var preferenceDataBase: PreferenceDataBase
) {

    fun convertKgToLb(value: Double): Double = value * 2.2046226218488

    fun convertLbToKg(value: Double): Double = value / 2.2046226218488

    fun bmiWeightString(value: Float): String {
        return if (value < 15f)
            "Severely underweight"
        else if (value <= 15f || value >= 16.toFloat())
            if (value <= 16.toFloat() || value.toDouble() >= 18.5)
                if (value.toDouble() <= 18.5 || value >= 25.toFloat())
                    if (value <= 25.toFloat() || value >= 30.toFloat())
                        if (value <= 30.toFloat() || value >= 35.toFloat())
                            if (value <= 35.toFloat() || value >= 40.toFloat())
                                if (value > 40.toFloat())
                                    "Severely obese"
                                else
                                    "Overweight"
                            else
                                "Very obese"
                        else
                            "Moderately obese"
                    else
                        "Overweight"
                else
                    "Healthy Weight"
            else
                "Underweight"
        else
            "Very underweight"
    }

    fun convertInchToFeet(inch: Double): Int = (inch / 12.0).toInt()

    fun calculationForBmiGraph(value: Float): Float {
        val weight: Float
        val height: Float
        val underWeight = 15.toFloat()
        if (value < underWeight) {
            return 0.0f
        }
        if (value > underWeight && value < 16.toFloat()) {
            return value * 0.25f / 15.5f
        }
        if (value > 16.toFloat() && value.toDouble() < 18.5) {
            return value * 0.75f / 15.5f + 0.5f
        }
        if (value.toDouble() > 18.5 && value < 25.toFloat()) {
            weight = value * 1.0f / 15.5f
            height = 2.0f
        } else if (value > 25.toFloat() && value < 30.toFloat()) {
            weight = value * 0.5f / 15.5f
            height = 4.0f
        } else if (value > 30.toFloat() && value < 35.toFloat()) {
            weight = value * 0.5f / 15.5f
            height = 5.0f
        } else if (value <= 35.toFloat() || value >= 40.toFloat()) {
            return if (value > 40.toFloat()) 6.9f else 0.0f
        } else {
            weight = value * 0.5f / 15.5f
            height = 6.0f
        }
        return weight + height
    }

    fun convertFeetInToInch(feet: Int, inch: Double): Double = (feet * 12).toDouble() + inch


    fun getFirstWeekOfDayByDayNo(dayNo: Int): String {
        return if (dayNo != 1) if (dayNo != 2) if (dayNo != 3) "" else "Saturday" else "Monday" else "Sunday"
    }

    private fun getMeter(inch: Double): Double = inch * 0.0254

    fun getStringFormat(d: Double) =
        String.format("%.2f", *arrayOf<Any>(java.lang.Double.valueOf(d)).copyOf(1))

    fun getBmiValue(weight: Float, feet: Int, inch: Int): Double {
        if (weight == 0f && feet == 0 && inch == 0) return 0.0

        return weight.toDouble() / getMeter(convertFeetInToInch(feet, inch.toDouble())).pow(2.0)
    }

    fun getBmiWeightTextColor(weight: String): Int {
        return when (weight) {
            "Severely obese" -> context.getCompactColor(R.color.colorFirst)
            "Overweight" -> context.getCompactColor(R.color.colorFour)
            "Very obese" -> context.getCompactColor(R.color.colorSix)
            "Moderately obese" -> context.getCompactColor(R.color.colorFive)
            "Healthy Weight" -> context.getCompactColor(R.color.colorThird)
            "Underweight" -> context.getCompactColor(R.color.colorSecond)
            else -> context.getCompactColor(R.color.black)
        }
    }

    fun convertInchFromInch(value: Double): Float =
        BigDecimal(value % 12.0).setScale(1, 6).toFloat()

    private fun unitFormat(i: Int): String {
        return if (i < 0 || i >= 10) {
            "" + i
        } else StringBuilder().append('0').append(i).toString()
    }

    fun convertCmToInch(d: Double): Double = d / 2.54

    fun convertInchToCm(d: Double): Double = d * 2.54

    fun convertSecondToTime(i: Int): String {
        return if (i <= 0) "00:00" else unitFormat(i / 60) + ":" + unitFormat(i % 60)
    }

    fun shareLink(context: Context, subject: String, desc: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_TEXT, desc)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.type = "text/plain"
        context.startActivity(
            Intent.createChooser(
                intent, context.resources.getString(R.string.app_name)
            )
        )
    }


    fun rateUs(context: Context) {

        val packageName = context.packageName
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")
                )
            )
        } catch (unused: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }

    fun openUrl(context: Context, str: String) {

        try {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(str)))
        } catch (unused: ActivityNotFoundException) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(str)))
        }
    }

    fun downloadTTSEngine(context: Context) {
        try {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=text to speech&c=apps")))
        } catch (e: ActivityNotFoundException) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/search?q=text to speech&c=apps")))
        }
    }


    val currentDate: String
        get() = SimpleDateFormat(Constant.CONS_DATE_FORMAT_DISPLAY, Locale.getDefault()).format(Date()).toString()

    fun getStringToDate(date: String): Date? =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(date)

    fun convertFullDateToDate(str: String): String {
        return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(
            SimpleDateFormat(Constant.CONS_DATE_FORMAT_DISPLAY, Locale.getDefault()).parse(str)
                ?: Date()
        )
    }

    fun getCurrentWeekByFirstDay(): ArrayList<String> {

        val list = ArrayList<String>()
        try {
            val simpleDateFormat =
                SimpleDateFormat(Constant.CONS_DATE_FORMAT_DISPLAY, Locale.getDefault())
            val calendar = Calendar.getInstance()

            val firstWeekDayNameByDayNo =
                getFirstWeekOfDayByDayNo(preferenceDataBase.getFirstDayOfWeek())
            val hashCode = firstWeekDayNameByDayNo.hashCode()
            if (hashCode != -2049557543) {
                if (hashCode != -1984635600) {
                    if (hashCode == -1807319568 && firstWeekDayNameByDayNo == "Sunday") {
                        calendar.firstDayOfWeek = 1
                        calendar[7] = 1
                    }
                } else if (firstWeekDayNameByDayNo == "Monday") {
                    calendar.firstDayOfWeek = 2
                    calendar[7] = 2
                }
            } else if (firstWeekDayNameByDayNo == "Saturday") {
                calendar.firstDayOfWeek = 7
                calendar[7] = 7
            }
            for (i in 1..7) {
                try {
                    list.add(simpleDateFormat.format(calendar.time))
                    calendar.add(5, 1)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e2: Exception) {
            e2.printStackTrace()
        }
        return list
    }


    fun getFirstWeekDayNoByDayName(str: String): Int {
        val hashCode = str.hashCode()
        if (hashCode == -2049557543) return if (str == "Saturday") 3 else 1

        if (hashCode == -1984635600) {
            return if (str == "Monday") 2 else 1
        }

        if (hashCode != -1807319568) return 1

        return 1
    }

    fun isLeapYear(year: Int): Int {
        val calendar = Calendar.getInstance()
        calendar[Calendar.YEAR] = year
        return calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
    }

    fun getAssetItems(folderName: String): ArrayList<String> {
        val arrayList = ArrayList<String>()
        try {
            val list = context.assets.list(folderName)
            if (list != null) {
                for (string in list) arrayList.add("///android_asset/$folderName/$string")
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return arrayList
    }

    fun replaceSpacialCharacters(str: String): String =
        str.replace(" ", "").replace("&", "").replace("-", "").replace("'", "")

    fun getDecimalValue(value: String): String = DecimalFormat("##.##").format(value.toFloat())

    fun openYoutube(uri: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setPackage("com.google.android.youtube")
        context.startActivity(intent)
    }

    fun checkVolume(): Boolean {
        val audioManager = context.getSystemService(AUDIO_SERVICE) as AudioManager
        val volumeLevel = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
        return volumeLevel >= 5
    }




}