package homeworkout.sixpackabs.workoutfitness.utils

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import homeworkout.sixpackabs.workoutfitness.R
import java.util.*

class SoundUtil(
    private val context: Context,
    private val preferenceDataBase: PreferenceDataBase
) {

    private var audioManager: AudioManager? = null
    private var soundMap: MutableMap<Int, Int> = mutableMapOf()
    private var soundPool: SoundPool? = null
    private var ttsSoundPool: SoundPool? = null
    private fun initView() {
        try {

            soundPool = SoundPool(3, 3, 0)
            val hashMap = HashMap<Int, Int>()
            soundMap = hashMap
            hashMap[SOUND_WHISTLE] = soundPool?.load(context, R.raw.whistle, 1) ?: 0
            soundMap[SOUND_DING] = soundPool?.load(context, R.raw.ding, 1) ?: 0
            audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            ttsSoundPool = SoundPool(1, 3, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: Error) {
            e.printStackTrace()
        }
    }

    fun playSound(i: Int) {
        if ((!preferenceDataBase.isSoundMute() && preferenceDataBase.isCoachTips() || preferenceDataBase.isSoundMute() && preferenceDataBase.isCoachTips()) && soundPool != null && audioManager != null)
            soundPool?.play(soundMap[i] ?: 0, 1.0f, 1.0f, 1, 0, 1.0f)
    }

    companion object {
        var SOUND_DING = 1
        var SOUND_WHISTLE = 0
    }

    init {
        initView()
    }
}