package homeworkout.sixpackabs.workoutfitness.utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.DecelerateInterpolator
import android.widget.Scroller
import androidx.viewpager.widget.ViewPager

class CustomViewPager : ViewPager {
    override fun onInterceptTouchEvent(motionEvent: MotionEvent): Boolean {
        return false
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(motionEvent: MotionEvent): Boolean {
        return false
    }

    constructor(context: Context?) : super(context!!) {
        setMyScroller()
    }

    constructor(context: Context?, attributeSet: AttributeSet?) : super(context!!, attributeSet) {
        setMyScroller()
    }

    private fun setMyScroller() {
        try {
            val declaredField = ViewPager::class.java.getDeclaredField("mScroller")
            declaredField.isAccessible = true
            declaredField[this] = MyScroller(context)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class MyScroller(context: Context) : Scroller(context, DecelerateInterpolator()) {
        override fun startScroll(i: Int, i2: Int, i3: Int, i4: Int, i5: Int) {
            super.startScroll(i, i2, i3, i4, 350)
        }
    }
}