package homeworkout.sixpackabs.workoutfitness.utils

import android.content.Context
import android.speech.tts.TextToSpeech
import java.util.*

class SpeechToText(
    context: Context,
    private var preferenceDataBase: PreferenceDataBase
) {
    private var textToSpeech: TextToSpeech? = null

    init {
        textToSpeech = TextToSpeech(context) { i: Int ->
            if (i != -1)
                textToSpeech?.language = Locale.ENGLISH
        }
    }

    fun speechText(strSpeechText: String) {

        if (checkVoiceOnOrOff() && textToSpeech != null) {
            textToSpeech?.setSpeechRate(0.9f)
            textToSpeech?.speak(strSpeechText, 0, null, null)
        }
    }

    fun speechTextToProfile(strSpeechText: String) {
        textToSpeech?.setSpeechRate(0.9f)
        textToSpeech?.speak(strSpeechText, 0, null, null)

    }

    private fun checkVoiceOnOrOff(): Boolean {
        return !preferenceDataBase.isSoundMute() && preferenceDataBase.isVoiceGuide()
    }
}
