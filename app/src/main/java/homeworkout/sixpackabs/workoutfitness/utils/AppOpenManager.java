package homeworkout.sixpackabs.workoutfitness.utils;

import static androidx.lifecycle.Lifecycle.Event.ON_START;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.appopen.AppOpenAd;

import java.util.Date;
import java.util.Objects;

import homeworkout.sixpackabs.workoutfitness.ui.activity.HomeActivity;

public class AppOpenManager implements LifecycleObserver, Application.ActivityLifecycleCallbacks {
    public AppOpenAd appOpenAd = null;
    private long loadTime = 0;
    public Boolean isAdShow = false;
    SharedPreferences sharedPreferences;
    private final HomeWorkoutApplication myApplication;
    public Activity currentActivity;
    private static boolean isShowingAd = false;

    private static final String[] permissions = new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.CAMERA", "android.permission.CALL_PHONE", "android.permission.WRITE_CONTACTS", "android.permission.READ_CONTACTS"};

    public AppOpenManager(HomeWorkoutApplication myApplication) {
        this.myApplication = myApplication;
        this.myApplication.registerActivityLifecycleCallbacks(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(ON_START)
    public void onStart() {
        sharedPreferences = currentActivity.getSharedPreferences("rating", Context.MODE_PRIVATE);
        String s = currentActivity + "";
        if (!isAdShow) {
            showAdIfAvailable(false);
        }
    }

    public void fetchAd() {
        try {
            if (isAdAvailable()) {
                return;
            }
            AppOpenAd.AppOpenAdLoadCallback loadCallback = new AppOpenAd.AppOpenAdLoadCallback() {
                @Override
                public void onAdLoaded(@NonNull AppOpenAd appOpenAd) {
                    super.onAdLoaded(appOpenAd);
                    AppOpenManager.this.appOpenAd = appOpenAd;
                    AppOpenManager.this.loadTime = (new Date()).getTime();
                }

                @Override
                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                }
            };
            AdRequest request = getAdRequest();
            AppOpenAd.load(myApplication, Objects.requireNonNull(HomeWorkoutApplication.Companion.get_Admob_openapp()), request, AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT, loadCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }

    public boolean isAdAvailable() {
        return appOpenAd != null;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        currentActivity = null;
    }

    public void showAdIfAvailable(boolean activity) {
        if (!isShowingAd && isAdAvailable()) {
            FullScreenContentCallback fullScreenContentCallback =
                    new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            try {
                                AppOpenManager.this.appOpenAd = null;
                                isShowingAd = false;
                                fetchAd();
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                            if (activity) {
                                currentActivity.startActivity(new Intent(currentActivity, HomeActivity.class));
                                currentActivity.finish();
                            }
                        }

                        @Override
                        public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                            if (activity) {
                                currentActivity.startActivity(new Intent(currentActivity, HomeActivity.class));
                                currentActivity.finish();
                            }
                        }

                        @Override
                        public void onAdShowedFullScreenContent() {
                            isShowingAd = true;
                        }
                    };
            appOpenAd.setFullScreenContentCallback(fullScreenContentCallback);
            appOpenAd.show(currentActivity);
        } else {
            fetchAd();
            if (activity) {
                currentActivity.startActivity(new Intent(currentActivity, HomeActivity.class));
                currentActivity.finish();
            }
        }
    }
}