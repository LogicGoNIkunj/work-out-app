package homeworkout.sixpackabs.workoutfitness.utils

enum class ExerciseLevel {
    BEGINNER_LEVEL,
    INTERMEDIATE_LEVEL,
    ADVANCED_LEVEL
}