package homeworkout.sixpackabs.workoutfitness.utils

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import homeworkout.sixpackabs.workoutfitness.BuildConfig
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.api.API
import homeworkout.sixpackabs.workoutfitness.api.APIClient
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.model.FeedbackModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedbackDialogFragment : DialogFragment() {

    var progressBar: ProgressBar? = null
    var btnSend: MaterialButton? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
            = inflater.inflate(R.layout.dialog_feedback, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        val etTitle: TextInputEditText = requireDialog().findViewById(R.id.etTitle)
        val etDescription: TextInputEditText = requireDialog().findViewById(R.id.etDescription)
        btnSend = requireDialog().findViewById(R.id.btnSend)
        progressBar = requireDialog().findViewById(R.id.progressBar)

        btnSend?.setOnClickListener {
            if (requireContext().isNetworkAvailable()) {
                when {
                    TextUtils.isEmpty(etTitle.text) -> {
                        requireContext().showToast("Enter title")
                        etTitle.requestFocus()
                    }
                    TextUtils.isEmpty(etDescription.text) -> {
                        requireContext().showToast("Enter description")
                        etDescription.requestFocus()
                    }
                    else -> {
                        requireActivity().hideKeyboard(etDescription)
                        var packageInfo: PackageInfo? = null
                        try {
                            packageInfo = requireActivity().packageManager.getPackageInfo(BuildConfig.APPLICATION_ID,0)
                        } catch (e: PackageManager.NameNotFoundException) {
                            e.printStackTrace()
                        }
                        val title = etTitle.text.toString()
                        val description = etDescription.text.toString()
                        val appVersion: String = packageInfo?.versionName ?: BuildConfig.VERSION_NAME
                        val appName = resources.getString(R.string.app_name)
                        val appVersionCode: Int = Build.VERSION.SDK_INT
                        val mobileModel: String = Build.MODEL

                        sendFeedBack(appName, title, description, mobileModel, appVersionCode, appVersion)
                    }
                }
            } else
                requireActivity().showToast(getString(R.string.check_your_network))

        }
        dialog?.findViewById<View>(R.id.ivClose)?.setOnClickListener { dialog?.dismiss() }
    }


    private fun sendFeedBack( appName: String, title: String, description: String, mobileModel: String, appVersionCode: Int, appVersion: String ) {

        val packageName: String = BuildConfig.APPLICATION_ID

        progressBar?.visible()
        btnSend?.invisible()

        val api = APIClient.getClient()?.create(API::class.java)
        api?.sendFeedback( appName, packageName, title, description, mobileModel, appVersionCode.toString(), appVersion )?.enqueue(object : Callback<FeedbackModel> {
            override fun onResponse(call: Call<FeedbackModel>, response: Response<FeedbackModel>) {
                if (response.code() == 200 && response.body() != null) {
                    if (response.body()?.status == true) {
                        progressBar?.gone()
                        btnSend?.visible()
                        dialog?.dismiss()
                        requireActivity().showToast("Thank you for your feedback it is very helpful.")
                        return
                    }
                }

                progressBar?.gone()
                btnSend?.visible()
                requireActivity().showToast("Please send feedback in play-store")
                dialog?.dismiss()
            }

            override fun onFailure(call: Call<FeedbackModel>, t: Throwable) {
                progressBar?.gone()
                btnSend?.visible()
                requireActivity().showToast("Please send feedback in play-store")
                dialog?.dismiss()
            }
        })
    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout((resources.displayMetrics.widthPixels*0.9).toInt(), WRAP_CONTENT)
        dialog?.window?.apply {
            val drawable =requireActivity().getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(requireActivity().getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

    }


}
