package homeworkout.sixpackabs.workoutfitness.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.github.mikephil.charting.utils.Utils
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.CalendarMode
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.shawnlin.numberpicker.NumberPicker
import com.suke.widget.SwitchButton
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.extension.*
import homeworkout.sixpackabs.workoutfitness.interfaces.DialogCallBack
import homeworkout.sixpackabs.workoutfitness.interfaces.DialogDismissCallBack
import homeworkout.sixpackabs.workoutfitness.interfaces.OnMetricUnitDialogListener
import homeworkout.sixpackabs.workoutfitness.ui.activity.HomeActivity
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt


class DialogUtils @Inject constructor(private val preferenceDataBase: PreferenceDataBase, private val common: Common) {

    var countDownTime = 0
    var maxDuration = 0
    var minDuration = 0
    var isAutoIncrement = false
    var isAutoDecrement = false

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    fun soundOptionDialog(
        context: Context, ivSound: AppCompatImageView?, listener: DialogDismissCallBack?
    ) {

        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dl_sound_option)

        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            context.getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                setTint(context.getCompactColor(R.color.white))
                setBackgroundDrawable(this)
            }
        }
        val switchMute = dialog.findViewById<SwitchButton>(R.id.swtMute)
        val switchVoiceGuide = dialog.findViewById<SwitchButton>(R.id.swtVoiceGuide)
        val switchCoachTips = dialog.findViewById<SwitchButton>(R.id.swtCoachTips)
        val btnPositive = dialog.findViewById<TextView>(R.id.btnOk)
        switchVoiceGuide.isChecked = preferenceDataBase.isVoiceGuide()
        switchCoachTips.isChecked = preferenceDataBase.isCoachTips()
        if (preferenceDataBase.isSoundMute()) {
            switchMute.isChecked = true
            switchCoachTips.isChecked = false
            switchVoiceGuide.isChecked = false
        }
        switchMute.setOnCheckedChangeListener { _, isChecked ->
            preferenceDataBase.setSoundMute(isChecked)
            switchVoiceGuide.isChecked = !isChecked
            switchCoachTips.isChecked = !isChecked

            if (!isChecked) {
                switchVoiceGuide.isChecked = preferenceDataBase.isVoiceGuide()
                switchCoachTips.isChecked = preferenceDataBase.isCoachTips()
            }
            ivSound?.let {
                if (isChecked) ivSound.setImageResource(R.drawable.ic_sound_off)
                else ivSound.setImageResource(R.drawable.ic_sound_on)
            }
        }

        switchVoiceGuide.setOnCheckedChangeListener { _, isChecked ->
            switchMute.isChecked = !isChecked
            preferenceDataBase.setSoundMute(!isChecked)
            preferenceDataBase.setVoiceGuide(isChecked)

            if (isChecked) ivSound?.setImageResource(R.drawable.ic_sound_on)
        }
        switchCoachTips.setOnCheckedChangeListener { _, isChecked ->
            switchMute.isChecked = !isChecked
            preferenceDataBase.setSoundMute(!isChecked)
            preferenceDataBase.setCoachTips(isChecked)

            if (isChecked) ivSound?.setImageResource(R.drawable.ic_sound_on)

        }
        btnPositive.setOnClickListener {
            dialog.dismiss()
            listener?.onDismiss()
        }
        dialog.show()
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    fun showDurationDialog(context: Context, strCountDownTime: String, txtCountDownTime: TextView) {

        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_set_duration)
        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            setBackgroundDrawable(context.getCompactDrawable(R.drawable.back_exercise_item)?.apply {
                setTint(context.getCompactColor(R.color.white))
            })
        }
        val tvTitle = dialog.findViewById<TextView>(R.id.tvDlDurationTitle)
        val tvDuration = dialog.findViewById<TextView>(R.id.tvDurations)

        val btnPrevious = dialog.findViewById<ImageView>(R.id.btnPrev)
        val btnNext = dialog.findViewById<ImageView>(R.id.btnNext)

        val repeatUpdateHandler = Handler(Looper.getMainLooper())
        var rect:Rect?=null
        btnNext.setOnTouchListener { v, motionEvent ->

            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN  -> {
                    isAutoIncrement = true
                    repeatUpdateHandler.post(RptUpdater(repeatUpdateHandler, tvDuration))
                    rect = Rect(v.left, v.top, v.right, v.bottom)
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_UP->{
                    isAutoIncrement = false
                    rect = null
                }
                MotionEvent.ACTION_MOVE->{
                    rect?.let{ rect->
                        if (!rect.contains(v.left + motionEvent.x.toInt(), v.top + motionEvent.y.toInt())) {
                            isAutoIncrement = false
                        }
                    }
                }
            }
            return@setOnTouchListener false
        }

        btnPrevious.setOnTouchListener { v, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_CANCEL -> {
                    isAutoDecrement = true
                    repeatUpdateHandler.post(RptUpdater(repeatUpdateHandler, tvDuration))
                    rect = Rect(v.left, v.top, v.right, v.bottom)
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_UP -> {
                    isAutoDecrement = false
                    rect = null
                }
                MotionEvent.ACTION_MOVE->{
                    rect?.let { rect->
                        if (!rect.contains(v.left + motionEvent.x.toInt(), v.top + motionEvent.y.toInt())) {
                            isAutoDecrement = false
                        }
                    }
                }
            }
            return@setOnTouchListener false
        }

        if (strCountDownTime == Constant.CONS_COUNT_DOWN_TIME) {
            tvTitle.setText(R.string.str_set_duration)
            countDownTime = preferenceDataBase.getCountDownTime()
            maxDuration = 15
            minDuration = 10
        } else if (strCountDownTime == Constant.CONS_REST_SET) {
            tvTitle.setText(R.string.str_set_duration_5_180)
            countDownTime = preferenceDataBase.getRestTime()
            maxDuration = 180
            minDuration = 5
        }
        tvDuration.text = countDownTime.toString()

        btnPrevious.setOnClickListener {
            if (countDownTime > minDuration) {
                countDownTime--
                tvDuration.text = countDownTime.toString()
            }
        }

        btnNext.setOnClickListener {
            if (countDownTime < maxDuration) {
                countDownTime++
                tvDuration.text = countDownTime.toString()
            }
        }
        dialog.findViewById<MaterialButton>(R.id.tvCancel).setOnClickListener { dialog.cancel() }
        dialog.findViewById<MaterialButton>(R.id.tvSet).setOnClickListener {
            if (strCountDownTime == Constant.CONS_COUNT_DOWN_TIME) preferenceDataBase.setCountDownTime(
                countDownTime
            )
            else if (strCountDownTime == Constant.CONS_REST_SET) preferenceDataBase.setRestTime(
                countDownTime
            )

            txtCountDownTime.text = "$countDownTime secs"
            dialog.cancel()
        }
        dialog.show()

    }

    inner class RptUpdater(
        private var repeatUpdateHandler: Handler, private var tvDuration: TextView
    ) : Runnable {
        override fun run() {
            if (isAutoIncrement) {
                if (countDownTime < maxDuration) {
                    countDownTime++
                    tvDuration.text = countDownTime.toString()

                    repeatUpdateHandler.postDelayed(RptUpdater(repeatUpdateHandler, tvDuration), 100)
                }

            } else if (isAutoDecrement) {
                if (countDownTime > minDuration) {
                    countDownTime--
                    tvDuration.text = countDownTime.toString()

                    repeatUpdateHandler.postDelayed(RptUpdater(repeatUpdateHandler, tvDuration), 100)
                }

            }
        }

    }

    fun showWeekDayGoalDialog(context: Context,listener: DialogDismissCallBack) {
        var weekGoalDay = preferenceDataBase.getWeekGoalDay()
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_set_week_goal)
        dialog.window?.apply {
          setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            val drawable =context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

        val numberPicker: NumberPicker = dialog.findViewById(R.id.npWeeklyDayGoal)
        numberPicker.minValue = 1
        numberPicker.maxValue = 7
        numberPicker.displayedValues = arrayOf("1", "2", "3", "4", "5", "6", "7")
        numberPicker.wrapSelectorWheel = false
        numberPicker.value = weekGoalDay
        numberPicker.setOnValueChangedListener { _: NumberPicker?, _: Int, newVal: Int ->
            weekGoalDay = newVal
        }
        dialog.findViewById<MaterialButton>(R.id.btnCancel).setOnClickListener { dialog.dismiss() }
        dialog.findViewById<MaterialButton>(R.id.btnOkay).setOnClickListener {
            listener.onDismiss(weekGoalDay.toString())
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showFirstWeekDayDialog(context: Context, listener: DialogDismissCallBack) {

        var firstDayOfWeek = preferenceDataBase.getFirstDayOfWeek()
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_first_day_of_week)

        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            val drawable = context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

        dialog.findViewById<NumberPicker>(R.id.npWeeklyDayGoal).apply {
            minValue = 1
            maxValue = 3
            displayedValues = arrayOf("Sunday", "Monday", "Saturday")
            wrapSelectorWheel = false
            value = firstDayOfWeek
            setOnValueChangedListener { _: NumberPicker?, _: Int, newVal: Int ->
                firstDayOfWeek = newVal
            }
        }

        dialog.findViewById<MaterialButton>(R.id.btnCancel).setOnClickListener { dialog.dismiss() }
        dialog.findViewById<MaterialButton>(R.id.btnOkay).setOnClickListener {
            listener.onDismiss(common.getFirstWeekOfDayByDayNo(firstDayOfWeek))
            dialog.dismiss()
        }
        dialog.show()
    }


    fun showHeightWeightDialog(context: Context, databaseHelper: DataBaseHelper?, dialogCallBack: DialogCallBack) {

        var isWeightKg: Boolean
        var isInch = true

        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_weight_height)

        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.95).toInt(), WRAP_CONTENT)
            val drawable =context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

        val tvKG = dialog.findViewById<TextView>(R.id.txtKG)
        val tvLB = dialog.findViewById<TextView>(R.id.txtLB)
        val tvCM = dialog.findViewById<TextView>(R.id.txtCM)
        val tvIN = dialog.findViewById<TextView>(R.id.txtIN)
        val layoutInch = dialog.findViewById<LinearLayout>(R.id.lnyInch)
        val etWeight = dialog.findViewById<EditText>(R.id.edWeight)
        val etCM = dialog.findViewById<EditText>(R.id.edCM)
        val etFeet = dialog.findViewById<EditText>(R.id.edFeet)
        val etInch = dialog.findViewById<EditText>(R.id.edInch)

        if (preferenceDataBase.getString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG).equals(Constant.CONS_LB, ignoreCase = true)) {
            isWeightKg = false
            etWeight.setText(common.getStringFormat(common.convertKgToLb(preferenceDataBase.getWeight().toDouble())))
            tvKG.setTextColor(context.getCompactColor(R.color.colorBlack))
            tvLB.setTextColor(context.getCompactColor(R.color.colorWhite))
            tvKG.background = null
            tvLB.background = context.getCompactDrawable(R.drawable.ic_selected)
        } else {
            isWeightKg = true
            etWeight.setText(preferenceDataBase.getWeight().toString())
            tvKG.setTextColor(context.getCompactColor(R.color.colorWhite))
            tvLB.setTextColor(context.getCompactColor(R.color.colorBlack))
            tvKG.background = context.getCompactDrawable(R.drawable.ic_selected)
            tvLB.background = null
        }

        if (preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN).equals(Constant.CONS_IN, true)) {
            etFeet.setText(preferenceDataBase.getFoot().toString())
            etInch.setText(preferenceDataBase.getInch().toString())
        }else{
            isInch = false
            etCM.visible()
            layoutInch.invisible()

            tvIN.setTextColor(context.getCompactColor(R.color.colorBlack))
            tvCM.setTextColor(context.getCompactColor(R.color.colorWhite))
            tvIN.background = null
            tvCM.background = context.getCompactDrawable(R.drawable.ic_selected)

            etCM.setText(preferenceDataBase.getString(Constant.CONS_CENTI_METER, ""))
        }

        dialog.findViewById<View>(R.id.btnCancel).setOnClickListener { dialog.cancel() }

        tvKG.setOnClickListener {
            preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG)
            if (!isWeightKg) {
                isWeightKg = true
                tvKG.setTextColor(context.getCompactColor(R.color.colorWhite))
                tvLB.setTextColor(context.getCompactColor(R.color.colorBlack))
                tvKG.background = context.getCompactDrawable(R.drawable.ic_selected)
                tvLB.background = null
                etWeight.hint = Constant.CONS_KG
                if (etWeight.text.toString().isNotEmpty())
                    etWeight.setText(common.getStringFormat(common.convertLbToKg(etWeight.text.toString().toDouble())))

            }
        }

        tvLB.setOnClickListener {
            preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_LB)
            if (isWeightKg) {
                isWeightKg = false
                tvKG.setTextColor(context.getCompactColor(R.color.colorBlack))
                tvLB.setTextColor(context.getCompactColor(R.color.colorWhite))
                tvKG.background = null
                tvLB.background = context.getCompactDrawable(R.drawable.ic_selected)
                etWeight.hint = Constant.CONS_LB
                if (etWeight.text.toString().isNotEmpty()) etWeight.setText(
                    common.getStringFormat(common.convertKgToLb(etWeight.text.toString().toDouble())))

            }
        }

        tvCM.setOnClickListener {
            if (isInch) {
                isInch = false
                etCM.visible()
                layoutInch.invisible()
                tvIN.setTextColor(context.getCompactColor(R.color.colorBlack))
                tvCM.setTextColor(context.getCompactColor(R.color.colorWhite))
                tvIN.background = null
                tvCM.background = context.getCompactDrawable(R.drawable.ic_selected)

                var footToInch = 0.0
                if (etFeet.text.toString().isNotEmpty() && etInch.text.toString().isNotEmpty()) {
                    footToInch = common.convertFeetInToInch(etFeet.text.toString().toInt(), etInch.text.toString().toDouble())
                } else if (etFeet.text.toString().isNotEmpty() && etInch.text.toString().isNotEmpty()) {
                    footToInch = common.convertFeetInToInch(etFeet.text.toString().toInt(), Utils.DOUBLE_EPSILON)
                } else if (etFeet.text.toString().isEmpty() && etInch.text.toString().isNotEmpty()) {
                    footToInch = common.convertFeetInToInch(1, etInch.text.toString().toDouble())
                }
                etCM.setText(common.convertInchToCm(footToInch).roundToInt().toString())
                preferenceDataBase.setString(Constant.CONS_IN_CM_UNIT, Constant.CONS_CM)
            }
        }

        tvIN.setOnClickListener {
            preferenceDataBase.setString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN)
            etCM.gone()
            layoutInch.visible()
            tvIN.setTextColor(context.getCompactColor(R.color.colorWhite))
            tvCM.setTextColor(context.getCompactColor(R.color.colorBlack))
            tvIN.background = context.getCompactDrawable(R.drawable.ic_selected)
            tvCM.background = null
            if (!isInch) {
                isInch = true
                if (etCM.text.toString().isNotEmpty()) {
                    val stringFormat = common.getStringFormat(common.convertCmToInch(etCM.text.toString().toDouble()))
                    etFeet.setText(common.convertInchToFeet(stringFormat.toDouble()).toString())
                    etInch.setText(common.convertInchFromInch(stringFormat.toDouble()).roundToInt().toString())
                }
            }
        }

        dialog.findViewById<View>(R.id.btnSave).setOnClickListener {

            if (etWeight.text.toString().isEmpty()) {
                context.showToast("Please enter Weight")
                return@setOnClickListener
            }

            if (isInch) {
                if (etFeet.text.toString().isEmpty()) {
                    context.showToast("Please enter Height")
                    return@setOnClickListener
                }
            }else{
                if (etCM.text.toString().isEmpty()) {
                    context.showToast("Please enter Height")
                    return@setOnClickListener
                }
            }
                val decimalValue: Float
                if (etFeet.text.toString().isNotEmpty() || preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN) != Constant.CONS_IN) {
                    if (etInch.text.toString().isEmpty() && preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN) == Constant.CONS_IN) {
                        context.showToast("Please enter Height")
                    } else if (preferenceDataBase.getString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG) == Constant.CONS_KG && (etWeight.text.toString().toFloat() < 20f || etWeight.text.toString().toFloat() > Constant.CONS_MAX_KG.toFloat())) {
                        context.showToast("Please enter proper weight in KG")
                    } else if (preferenceDataBase.getString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG) == Constant.CONS_LB && ((etWeight.text.toString().toFloat().toDouble()) < 44.09 || etWeight.text.toString().toFloat().toDouble() > 2198.01) && (etWeight.text.toString().toFloat() != 2198.01.toFloat() || etWeight.text.toString().toFloat() != 44.09.toFloat())) {
                        context.showToast("Please enter proper weight in LB")
                    } else if (preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN) == Constant.CONS_CM && (etCM.text.toString().toFloat() < 20.toFloat() || etCM.text.toString().toFloat() > Constant.CONS_MAX_CM.toFloat())) {
                        context.showToast("Please enter proper height in CM")
                    } else if (preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN) == Constant.CONS_IN && etFeet.text.toString().toInt() == 0 && etInch.text.toString().toFloat() < 7.9) {
                        context.showToast("Please enter proper height in INCH")
                    } else if (preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN) == Constant.CONS_IN && etFeet.text.toString().toInt() >= 13 && etInch.text.toString().toFloat() > 1.5) {
                        context.showToast("Please enter proper height in INCH")
                    } else if (preferenceDataBase.getString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN) != Constant.CONS_IN || etFeet.text.toString().toInt() < 14) {
                        try {
                            if (isInch) {
                                val parseFloat: Float = common.getDecimalValue(etInch.text.toString()).toFloat()
                                if (parseFloat > 12.0) {
                                    preferenceDataBase.setFoot(etFeet.text.toString().toInt() + 1)
                                    if (etFeet.text.toString().toInt() != 12 || parseFloat <= 13.5)
                                        preferenceDataBase.setInch((parseFloat - 12.0).toFloat())
                                    else
                                        preferenceDataBase.setInch(1.5.toFloat())

                                } else {
                                    preferenceDataBase.setFoot(etFeet.text.toString().toInt())
                                    preferenceDataBase.setInch(parseFloat)
                                }
                                preferenceDataBase.setString(Constant.CONS_IN_CM_UNIT, Constant.CONS_IN)
                            } else {

                                val inchValue = common.getStringFormat(common.convertCmToInch(etCM.text.toString().toDouble()))

                                preferenceDataBase.setFoot(common.convertInchToFeet(inchValue.toDouble()))
                                preferenceDataBase.setInch(common.convertInchFromInch(inchValue.toDouble()).roundToInt().toFloat())
                                preferenceDataBase.setHeightUnit(Constant.CONS_CM)
                                preferenceDataBase.setString(Constant.CONS_IN_CM_UNIT, Constant.CONS_CM)
                                preferenceDataBase.setString(Constant.CONS_CENTI_METER, common.getDecimalValue(etCM.text.toString()))
                            }
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        try {
                            if (isWeightKg) {
                                decimalValue = common.getDecimalValue(etWeight.text.toString()).toFloat()
                                preferenceDataBase.setWeightUnit(Constant.CONS_KG)
                                preferenceDataBase.setWeightUnit(Constant.CONS_KG)
                                preferenceDataBase.setWeight(decimalValue)
                            } else {
                                decimalValue = common.convertLbToKg(common.getDecimalValue(etWeight.text.toString()).toDouble()).toFloat()
                                preferenceDataBase.setWeightUnit(Constant.CONS_LB)
                                preferenceDataBase.setWeightUnit(Constant.CONS_LB)
                                preferenceDataBase.setWeight(decimalValue)
                            }
                            val convertFullDateToDate = common.convertFullDateToDate(common.currentDate)
                            databaseHelper?.let {
                                if (databaseHelper.weightExistOrNotFromDate(convertFullDateToDate))
                                    databaseHelper.updateWeight(convertFullDateToDate,decimalValue.toString(),"")
                                else
                                    databaseHelper.insertWeight(decimalValue.toString(), convertFullDateToDate, "")
                            }

                            if (isWeightKg)
                                preferenceDataBase.setWeight(etWeight.text.toString().toFloat())
                            else
                                preferenceDataBase.setWeight(common.convertLbToKg(etWeight.text.toString().toDouble()).toFloat())

                            dialogCallBack.positiveClick()
                        } catch (e2: java.lang.Exception) {
                            e2.printStackTrace()
                        }
                        dialog.dismiss()
                    } else {
                        context.showToast("Please enter proper height in INCH")
                    }
                }
                else {
                    context.showToast("Please enter Height")
                }
            }

         dialog.show()

        }

    fun restartProgressDialog(context: Context, message: String) {
        val builder = AlertDialog.Builder(context, R.style.AlertDialogStyle)

        builder.setMessage(message)
        builder.setCancelable(true)
        builder.setPositiveButton("Yes") { dialogInterface: DialogInterface, _: Int ->
            DataBaseHelper(context, preferenceDataBase, common).restartProgress()
            preferenceDataBase.clearAllData()
            context.startActivity<HomeActivity> {}
            dialogInterface.dismiss()
        }
        builder.setNegativeButton("No") { dialog: DialogInterface, _: Int ->
            dialog.dismiss()
        }
        builder.create().show()
    }

    fun showUnitDialog(
        context: Context, isWeight: Boolean, listener: OnMetricUnitDialogListener
    ) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_weight_unit)

        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            val drawable =context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

        val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)
        val rbWeightInLB = dialog.findViewById<RadioButton>(R.id.rbDlWeight1)
        val rbDlWeightInKG = dialog.findViewById<RadioButton>(R.id.rbDlWeight2)
        val rgWeight = dialog.findViewById<RadioGroup>(R.id.rgDlWeight)
        if (isWeight) {
            if (preferenceDataBase.getWeightUnit()
                    .equals(Constant.CONS_KG, ignoreCase = true)
            ) rbDlWeightInKG.isChecked = true
            else rbWeightInLB.isChecked = true

            tvTitle.text = context.getString(R.string.select_your_weight_unit)
            rbWeightInLB.text = Constant.CONS_LB
            rbDlWeightInKG.text = Constant.CONS_KG
        } else {
            if (preferenceDataBase.getHeightUnit()
                    .equals(Constant.CONS_IN, ignoreCase = true)
            ) rbDlWeightInKG.isChecked = true
            else rbWeightInLB.isChecked = true

            tvTitle.text = context.getString(R.string.select_your_height_unit)
            rbWeightInLB.text = Constant.CONS_CM
            rbDlWeightInKG.text = Constant.CONS_IN
        }
        rgWeight.setOnCheckedChangeListener { _: RadioGroup?, checkedId: Int ->
            listener.setHeightWeight(checkedId)
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showWeightDialog(context: Context, databaseHelper: DataBaseHelper, dialogCallBack: DialogCallBack) {

        var isWeightKG: Boolean
        var selectedDate = parseTime(Date().time)

        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_weight_set_dialog)

        dialog.window?.setLayout(((context.resources.displayMetrics.widthPixels * 0.95).toInt()), WRAP_CONTENT)
        dialog.window?.apply {
            val drawable = context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }
        val horizontalPicker: MaterialCalendarView = dialog.findViewById(R.id.calendarView)

        horizontalPicker.apply {
            tileWidth = MATCH_PARENT
            state().edit().setCalendarDisplayMode(CalendarMode.WEEKS)
                .setMaximumDate(CalendarDay.today()).commit()
        }
        horizontalPicker.setOnDateChangedListener { _, date, _ ->
            val calendar = Calendar.getInstance()
            calendar.set(date.year, date.month - 1, date.day)
            selectedDate = parseTime(calendar.time.time)
        }
        val etKG = dialog.findViewById<TextInputEditText>(R.id.edKG)
        val tvKg = dialog.findViewById<AppCompatTextView>(R.id.txtKg)
        val tvLB = dialog.findViewById<AppCompatTextView>(R.id.txtLB)
        val btnCancel = dialog.findViewById<MaterialButton>(R.id.btnCancel)
        val btnSave = dialog.findViewById<MaterialButton>(R.id.btnSave)
        val existWeight =
            if (preferenceDataBase.getWeight() == 0F) "" else preferenceDataBase.getWeight().toString()

        if (preferenceDataBase.getString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG) == Constant.CONS_KG) {
            isWeightKG = true
            etKG.setText(existWeight)
            tvKg.setTextColor(context.getCompactColor(R.color.colorWhite))
            tvLB.setTextColor(context.getCompactColor(R.color.colorBlack))
            etKG.hint = Constant.CONS_KG
            tvKg.background = context.getCompactDrawable(R.drawable.ic_selected)
            tvLB.background = null
            preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG)

        } else {
            isWeightKG = false

            if (existWeight.isNotEmpty()) etKG.setText(
                common.getStringFormat(common.convertKgToLb(existWeight.toDouble()))
            )

            etKG.hint = Constant.CONS_LB
            tvKg.setTextColor(context.getCompactColor(R.color.colorBlack))
            tvLB.setTextColor(context.getCompactColor(R.color.colorWhite))
            tvKg.background = null
            tvLB.background = context.getCompactDrawable(R.drawable.ic_selected)
            preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_LB)
        }

        tvLB.setOnClickListener {
            preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_LB)
            if (isWeightKG) {
                isWeightKG = false
                tvKg.setTextColor(context.getCompactColor(R.color.colorBlack))
                tvLB.setTextColor(context.getCompactColor(R.color.colorWhite))
                tvKg.background = null
                tvLB.background = context.getCompactDrawable(R.drawable.ic_selected)
                etKG.hint = Constant.CONS_LB
                if (etKG.text.toString().isNotEmpty()) {
                    etKG.setText(common.getStringFormat(common.convertKgToLb(etKG.text.toString().toDouble())))
                    etKG.setSelection(common.getStringFormat(common.convertKgToLb(etKG.text.toString().toDouble())).length - 1)
                }


            }
        }
        tvKg.setOnClickListener {
            preferenceDataBase.setString(Constant.CONS_KG_LB_UNIT, Constant.CONS_KG)
            if (!isWeightKG) {
                isWeightKG = true
                tvKg.setTextColor(context.getCompactColor(R.color.colorWhite))
                tvLB.setTextColor(context.getCompactColor(R.color.colorBlack))
                tvKg.background = context.getCompactDrawable(R.drawable.ic_selected)
                tvLB.background = null
                etKG.hint = Constant.CONS_KG
                if (etKG.text.toString().isNotEmpty()) {
                    etKG.setText(common.getStringFormat(common.convertLbToKg(etKG.text.toString().toDouble())))
                    etKG.setSelection(common.getStringFormat(common.convertLbToKg(etKG.text.toString().toDouble())).length)
                }


            }
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
        btnSave.setOnClickListener {
            val weight: Float
            when {
                etKG.text.toString().isEmpty() -> {
                    context.showToast("Please fill the field")
                    return@setOnClickListener
                }

                else -> try {
                    if (isWeightKG) {
                        weight = common.getDecimalValue(etKG.text.toString()).toFloat()
                        preferenceDataBase.setWeightUnit(Constant.CONS_KG)
                    } else {
                        weight = common.getDecimalValue(common.convertLbToKg(etKG.text.toString().toDouble()).toString()).toFloat()
                        preferenceDataBase.setWeightUnit(Constant.CONS_LB)
                    }
                    preferenceDataBase.setWeight(weight)
                    if (databaseHelper.weightExistOrNotFromDate(selectedDate)) {
                        if (preferenceDataBase.getWeightUnit() == Constant.CONS_KG)
                            databaseHelper.updateWeight(selectedDate, weight.toString(), "")
                          else
                            databaseHelper.updateWeight(selectedDate,common.convertLbToKg(weight.toString().toDouble()).roundToInt().toString(),"")

                    } else if (preferenceDataBase.getWeightUnit()== Constant.CONS_KG) {
                        databaseHelper.insertWeight(weight.toString(), selectedDate, "")
                    } else
                        databaseHelper.insertWeight(common.convertLbToKg(weight.toString().toDouble()).roundToInt().toString(), selectedDate, "")

                    dialogCallBack.positiveClick()
                    dialog.cancel()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        dialog.show()

    }

    private fun parseTime(date: Long): String =
        SimpleDateFormat(Constant.CONS_DATE_FORMAT, Locale.getDefault()).format(Date(date))

    private fun chooseTTSEngine(context: Context) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_select_tts_engine)

        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            val drawable =context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

        dialog.findViewById<RadioButton>(R.id.rdoGoogleEngine).setOnCheckedChangeListener { _, _ ->
            dialog.dismiss()
            Thread.sleep(500)
            confirmVoiceTest(context)
        }
        dialog.show()
        return

    }

    private fun confirmVoiceTest(context: Context): Boolean {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(context.getString(R.string.did_you_hear_test_voice))
        builder.setCancelable(true)
        builder.setPositiveButton("Yes") { dialog, _ ->
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog, _ ->
            testVoiceFailDialog(context)
            dialog.dismiss()
        }
        builder.create().show()

        return false
    }

    private fun testVoiceFailDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dl_test_voice_fail)

        dialog.window?.apply {
            setLayout((context.resources.displayMetrics.widthPixels * 0.9).toInt(), WRAP_CONTENT)
            val drawable =context.getCompactDrawable(R.drawable.back_exercise_item)
            drawable?.setTint(context.getCompactColor(R.color.white))
            setBackgroundDrawable(drawable)
        }

        val buttonDownloadTTSEngine = dialog.findViewById<MaterialButton>(R.id.btnDownloadTTSEngine)
        val buttonSelectTTSEngine = dialog.findViewById<MaterialButton>(R.id.btnSelectTTSEngine)

        buttonDownloadTTSEngine.setOnClickListener {
            common.downloadTTSEngine(context)
            dialog.dismiss()
        }
        buttonSelectTTSEngine.setOnClickListener {
            chooseTTSEngine(context)
            dialog.dismiss()
        }
        dialog.show()
    }


}