package homeworkout.sixpackabs.workoutfitness.utils

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.HiltAndroidApp
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.api.API
import homeworkout.sixpackabs.workoutfitness.extension.isNetworkAvailable
import homeworkout.sixpackabs.workoutfitness.model.Model_Playdata
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@HiltAndroidApp
class HomeWorkoutApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
        appOpenManager = AppOpenManager(this)
        sharedPreferences = getSharedPreferences("ps", MODE_PRIVATE)
        editor = sharedPreferences?.edit()
        getData(applicationContext)
    }

    companion object {
        var sharedPreferences: SharedPreferences? = null
        var editor: SharedPreferences.Editor? = null

        @SuppressLint("StaticFieldLeak")
        var appOpenManager: AppOpenManager? = null

        private var retrofit: Retrofit? = null
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null

        var qurekaimage = ""
        var querekatext: String? = ""
        var url: String? = ""
        var checkqureka = false

        fun set_AdsInt(adsInt: Int) = editor?.putInt("adsInt", adsInt)?.commit()

        fun get_AdsInt(): Int = sharedPreferences?.getInt("adsInt", 3) ?: 3

        //admob interstitial
        fun set_Admob_interstitial_Id(Admob_interstitial_Id: String?) {
            editor?.putString("Admob_interstitial_Id", Admob_interstitial_Id)?.commit()
        }

        fun get_Admob_interstitial_Id(): String? {
            return sharedPreferences?.getString("Admob_interstitial_Id", context?.getString(R.string.str_Interstitial_ad))
        }

        //admob native
        fun set_Admob_native_Id(Admob_native_Id: String?) {
            editor?.putString("Admob_native_Id", Admob_native_Id)?.commit()
        }

        fun get_Admob_native_Id(): String? {
            return sharedPreferences?.getString("Admob_native_Id", context?.getString(R.string.str_native_ad_key))
        }

        //qureka
        fun set_qureka(yes: Boolean) {
            editor?.putBoolean("qureka_Id", yes)?.commit()
        }

        fun get_qureka(): Boolean {
            return sharedPreferences?.getBoolean("qureka_Id", false) == true
        }

        //admob openapp
        fun set_Admob_openapp(Admob_native_Id: String?) {
           editor?.putString("Admob_open_Id", Admob_native_Id)?.commit()
        }

        fun get_Admob_openapp(): String? {
            return sharedPreferences?.getString("Admob_open_Id", context?.getString(R.string.open_app_ad_id))
        }

        fun getData(context: Context) {
            try {
                if (context.isNetworkAvailable()) {
                    if (retrofit == null) {
                        retrofit = Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(
                            OkHttpClient.Builder().addInterceptor { chain: Interceptor.Chain ->
                                chain.proceed(chain.request().newBuilder().build())
                            }.connectTimeout(100, TimeUnit.SECONDS)
                                .readTimeout(100, TimeUnit.SECONDS).build()
                        ).addConverterFactory(GsonConverterFactory.create()).build()
                    }
                    val nativeClient: API? = retrofit?.create(API::class.java)
                    nativeClient?.getData()?.enqueue(object : Callback<Model_Playdata> {
                        override fun onResponse(call: Call<Model_Playdata>, response: Response<Model_Playdata>) {
                            try {
                                response.body()?.let { body ->
                                    if (body.isStatus) {
                                        if (body.data != null) {
                                            checkqureka = body.data.isFlage
                                            if (body.data.isFlage) {
                                                qurekaimage = body.data.image
                                                url = body.data.url
                                                querekatext = body.data.title
                                            }
                                        }
                                    }
                                }
                            } catch (exception: Exception) {
                                exception.printStackTrace()
                            }
                        }

                        override fun onFailure(call: Call<Model_Playdata?>, t: Throwable) {}
                    })
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}