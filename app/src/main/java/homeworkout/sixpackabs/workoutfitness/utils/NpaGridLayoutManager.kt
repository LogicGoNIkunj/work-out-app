package homeworkout.sixpackabs.workoutfitness.utils

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager

internal class NpaGridLayoutManager(context: Context?, spanCount: Int) :
    GridLayoutManager(context, spanCount) {

    override fun supportsPredictiveItemAnimations() = false

}