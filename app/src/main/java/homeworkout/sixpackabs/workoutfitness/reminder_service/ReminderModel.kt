package homeworkout.sixpackabs.workoutfitness.reminder_service

data class ReminderModel(
    var reminderId: Int = 0,
    var title: String = "",
    var date: String = "",
    var time: String = "",
    var repeat: String = "",
    var repeatNo: String = "",
    var repeatType: String = "",
    var isActive: Boolean = false,
    var day: String = "",
) {


    constructor(title: String, date: String, time: String, repeat: String, repeatNo: String, repeatType: String, active: Boolean, days: String) : this() {
        this.title = title
        this.date = date
        this.time = time
        this.repeat = repeat
        this.repeatNo = repeatNo
        this.repeatType = repeatType
        this.isActive = active
        this.day = days
    }

}