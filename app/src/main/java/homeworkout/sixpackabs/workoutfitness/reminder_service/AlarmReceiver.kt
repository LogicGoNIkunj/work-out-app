package homeworkout.sixpackabs.workoutfitness.reminder_service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class AlarmReceiver : BroadcastReceiver() {

    @Inject
    lateinit var reminderDataBaseHelper: ReminderDatabase

    private var isActive = false
    private var alarmReceiver: ReminderReceiver? = null
    private var calendar: Calendar? = null
    private var date: String? = null
    private var dateSplit: Array<String> = arrayOf()
    private var day = 0
    private var hour = 0
    private var minute = 0
    private var month = 0
    private var receivedId = 0
    private var repeat: String? = null
    private var repeatNo: String? = null
    private var repeatTime: Long = 0
    private var repeatType: String? = null
    private var time: String? = null
    private var timeSplit: Array<String> = arrayOf()
    private var year = 0


    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {

            calendar = Calendar.getInstance()
            alarmReceiver = ReminderReceiver()
            val allReminders = reminderDataBaseHelper.listOfAllReminders
            for (next in allReminders) {
                receivedId = next.reminderId
                repeat = next.repeat
                repeatNo = next.repeatNo
                repeatType = next.repeatType
                isActive = next.isActive
                date = next.date
                time = next.time
                val dateSplit: Array<String> = Regex("/").split(date ?: "", 0).toTypedArray()
                this.dateSplit = dateSplit

                val timeSplit: Array<String> = Regex(":").split(time ?: "", 0).toTypedArray()
                this.timeSplit = timeSplit

                day = this.dateSplit[0].toInt()
                month = dateSplit[1].toInt()
                year = dateSplit[2].toInt()
                hour = timeSplit[0].toInt()
                minute = timeSplit[1].toInt()

                month -= 1
                calendar?.set(Calendar.MONTH, month)
                calendar?.set(Calendar.YEAR, year)
                calendar?.set(Calendar.DAY_OF_MONTH, day)
                calendar?.set(Calendar.HOUR, hour)
                calendar?.set(Calendar.MINUTE, minute)
                calendar?.set(Calendar.SECOND, 0)


                when (repeatType) {
                    "Minute" -> repeatNo?.let { repeatTime = it.toInt().toLong() * milMinute }
                    "Hour" -> repeatNo?.let { repeatTime = it.toInt().toLong() * milHour }
                    "Day" -> repeatNo?.let { repeatTime = it.toInt().toLong() * milDay }
                    "Week" -> repeatNo?.let { repeatTime = it.toInt().toLong() * milWeek }
                    "Month" -> repeatNo?.let { repeatTime = it.toInt().toLong() * milMonth }
                }
                if (isActive) {
                    if (repeat == "true") {
                        calendar?.let { alarmReceiver?.setRepeatAlarm(context, it, receivedId, repeatTime) }
                    } else if (repeat == "false") {
                        calendar?.let { alarmReceiver?.setAlarm(context, it, receivedId) }
                    }
                }
            }
        }
    }


    companion object {
        private const val milDay: Long = 86400000
        private const val milHour: Long = 3600000
        private const val milMinute: Long = 60000
        private const val milMonth = 2592000000L
        private const val milWeek: Long = 604800000
    }
}