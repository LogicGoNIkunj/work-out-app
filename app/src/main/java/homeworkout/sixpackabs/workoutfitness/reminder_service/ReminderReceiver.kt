package homeworkout.sixpackabs.workoutfitness.reminder_service

import android.annotation.SuppressLint
import android.app.*
import android.app.Notification.*
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import androidx.core.app.NotificationCompat
import dagger.hilt.android.AndroidEntryPoint
import homeworkout.sixpackabs.workoutfitness.ui.activity.HomeActivity
import homeworkout.sixpackabs.workoutfitness.utils.Constant
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


@SuppressLint("WrongConstant")
@AndroidEntryPoint
class ReminderReceiver : BroadcastReceiver() {

    @Inject
    lateinit var reminderDataBaseHelper: ReminderDatabase

    private var alarmManager: AlarmManager? = null
    private var pendingIntent: PendingIntent? = null
    private var reminderModel: ReminderModel? = null

    private val calendar: Calendar = Calendar.getInstance()


    override fun onReceive(context: Context, intent: Intent) {

        val reminderId = intent.getStringExtra(Constant.CONS_REMINDER_ID)

        try {

            reminderModel = reminderId?.toInt()?.let { reminderDataBaseHelper.getReminder(it) }
            if (reminderModel?.isActive == true) {
                var dayList = ArrayList<String>()

                if ((reminderModel?.day as CharSequence).contains(","))
                    dayList = (reminderModel?.day as CharSequence).split(",") as ArrayList<String>
                else
                    reminderModel?.day?.let { dayList.add(it) }

                for (i in dayList.indices)
                    dayList[i] = dayList[i].replace("'", "")


                val dayNumber = getDayNumber(currentDayName.uppercase(Locale.getDefault()))
                if (dayList.contains(dayNumber))
                    fireNotification(context, reminderModel)

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setAlarm(context: Context, calendar: Calendar, i: Int) {
        val systemService = context.getSystemService(CATEGORY_ALARM)
        if (systemService != null) {
            alarmManager = systemService as AlarmManager
            val intent = Intent(context, ReminderReceiver::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(Constant.CONS_REMINDER_ID, i.toString())
            pendingIntent = PendingIntent.getBroadcast(context, i, intent, 0)
            if (Build.VERSION.SDK_INT >= 23)
                alarmManager?.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis,
                    pendingIntent
                )
            else
                alarmManager?.setExact(
                    AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis,
                    pendingIntent
                )

            context.packageManager.setComponentEnabledSetting(
                ComponentName(
                    context,
                    AlarmReceiver::class.java
                ), 1, 1
            )
        }
    }

    fun setRepeatAlarm(context: Context, calendar: Calendar, i: Int, j: Long) {
        val systemService = context.getSystemService(NotificationCompat.CATEGORY_ALARM)
        if (systemService != null) {
            alarmManager = systemService as AlarmManager
            val currentTimeMillis = System.currentTimeMillis().toInt()
            val intent = Intent(context, ReminderReceiver::class.java)
            intent.addFlags(268435456)
            intent.putExtra(Constant.CONS_REMINDER_ID, i.toString())
            pendingIntent =
                PendingIntent.getBroadcast(context, currentTimeMillis, intent, 268435456)

            val timeInMillis = calendar.timeInMillis - this.calendar.timeInMillis

            alarmManager?.setRepeating(
                0,
                SystemClock.elapsedRealtime() + timeInMillis,
                j,
                pendingIntent
            )
            context.packageManager.setComponentEnabledSetting(
                ComponentName(
                    context,
                    AlarmReceiver::class.java
                ), 1, 1
            )

        }

    }

    fun cancelAlarm(context: Context, i: Int) {
        val systemService = context.getSystemService(NotificationCompat.CATEGORY_ALARM)
        if (systemService != null) {
            alarmManager = systemService as AlarmManager
            pendingIntent = PendingIntent.getBroadcast(
                context,
                i,
                Intent(context, ReminderReceiver::class.java),
                0
            )
            alarmManager?.cancel(pendingIntent)
            context.packageManager.setComponentEnabledSetting(
                ComponentName(
                    context,
                    AlarmReceiver::class.java
                ), 2, 1
            )

        }

    }

    private fun fireNotification(context: Context, reminder: ReminderModel?) {
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence =
                context.getString(homeworkout.sixpackabs.workoutfitness.R.string.app_name)
            val description = "$name Alert"
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(reminder?.reminderId.toString(), name, importance)
            channel.description = description
            val notificationManager: NotificationManager =
                context.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }

        val intent = Intent(context, HomeActivity::class.java)
        val pIntent =
            PendingIntent.getActivity(context, System.currentTimeMillis().toInt(), intent, 0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val builder = NotificationCompat.Builder(context, reminder?.reminderId.toString())
            builder.setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText("Your body needs energy! You haven't exercised in $currentFullDayName!")
            )
            builder.setContentIntent(pIntent)
            builder.setOngoing(false)
            builder.setDefaults(DEFAULT_SOUND or DEFAULT_VIBRATE).priority =
                NotificationCompat.PRIORITY_LOW
            builder.setSmallIcon(homeworkout.sixpackabs.workoutfitness.R.mipmap.ic_launcher)
            val notification: Notification = builder.build()
            manager.notify(1, notification)

            val ringToneManager: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            val ringtone = RingtoneManager.getRingtone(context, ringToneManager)
            ringtone?.play()

            Handler(Looper.getMainLooper()).postDelayed({
                ringtone?.stop()
            }, 15000)
        }

    }

    private val currentFullDayName: String
        get() {
            calendar.firstDayOfWeek = 2
            return SimpleDateFormat("EEEE", Locale.getDefault()).format(calendar.time)
        }
    private val currentDayName: String
        get() {
            calendar.firstDayOfWeek = 2
            return SimpleDateFormat("EEE", Locale.ENGLISH).format(calendar.time)
        }

    private fun getDayNumber(str: String): String {
        return when (str) {
            str -> "5"
            str -> "1"
            str -> "6"
            str -> "7"
            str -> "4"
            str -> "2"
            str -> "3"
            else -> ""
        }
    }


}