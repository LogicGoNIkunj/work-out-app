package homeworkout.sixpackabs.workoutfitness.reminder_service

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

class ReminderDatabase(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null as CursorFactory?, 2) {
    override fun onCreate(sQLiteDatabase: SQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE ReminderTable(id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT,date TEXT,time INTEGER,repeat BOOLEAN,repeat_no INTEGER,repeat_type TEXT,active BOOLEAN,days TEXT)")
    }

    override fun onUpgrade(sQLiteDatabase: SQLiteDatabase, i: Int, i2: Int) {
        if (i < i2) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ReminderTable")
            onCreate(sQLiteDatabase)
        }
    }

    fun addReminder(reminder: ReminderModel): Int {

        val contentValues = ContentValues()
        contentValues.put(FIELD_TITLE, reminder.title)
        contentValues.put(FIELD_DATE, reminder.date)
        contentValues.put(FIELD_TIME, reminder.time)
        contentValues.put(FIELD_REPEAT, reminder.repeat)
        contentValues.put(FIELD_REPEAT_NO, reminder.repeatNo)
        contentValues.put(FIELD_REPEAT_TYPE, reminder.repeatType)
        contentValues.put(FIELD_ACTIVE, reminder.isActive.toString())
        contentValues.put(FIELD_DAYS, reminder.day)
        val insert = writableDatabase.insert(TABLE_REMINDERS, null, contentValues)
        writableDatabase.close()
        return insert.toInt()
    }

    fun getReminder(i: Int): ReminderModel {
        var reminder = ReminderModel()
        try {
            val query = readableDatabase.query(
                TABLE_REMINDERS,
                arrayOf(
                    FIELD_ID,
                    FIELD_TITLE,
                    FIELD_DATE,
                    FIELD_TIME,
                    FIELD_REPEAT,
                    FIELD_REPEAT_NO,
                    FIELD_REPEAT_TYPE,
                    FIELD_ACTIVE,
                    FIELD_DAYS
                ),
                "id=?",
                arrayOf(i.toString()),
                null,
                null,
                null,
                null
            )

            if (query?.moveToFirst() == true) {
                val id = query.getString(query.getColumnIndexOrThrow(FIELD_ID))

                reminder = ReminderModel(
                    id?.toInt() ?: -1,
                    query.getString(query.getColumnIndexOrThrow(FIELD_TITLE)) ?: "",
                    query.getString(query.getColumnIndexOrThrow(FIELD_DATE)) ?: "",
                    query.getString(query.getColumnIndexOrThrow(FIELD_TIME)) ?: "",
                    query.getString(query.getColumnIndexOrThrow(FIELD_REPEAT)) ?: "",
                    query.getString(query.getColumnIndexOrThrow(FIELD_REPEAT_NO)) ?: "",
                    query.getString(query.getColumnIndexOrThrow(FIELD_REPEAT_TYPE)) ?: "",
                    query.getString(query.getColumnIndexOrThrow(FIELD_ACTIVE)) == "true",
                    query.getString(query.getColumnIndexOrThrow(FIELD_DAYS)) ?: ""
                )

            }
            query.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return reminder
    }

    val listOfAllReminders: ArrayList<ReminderModel>
        get() {
            val list = arrayListOf<ReminderModel>()
            val rawQuery =
                writableDatabase.rawQuery("SELECT * FROM ReminderTable ORDER BY id DESC", null)
            if (rawQuery.moveToFirst()) {
                do {
                    val reminder = ReminderModel()
                    val string = rawQuery.getString(0)
                    reminder.reminderId = string.toInt()
                    reminder.title = rawQuery.getString(1)
                    reminder.date = rawQuery.getString(2)
                    reminder.time = rawQuery.getString(3)
                    reminder.repeat = rawQuery.getString(4)
                    reminder.repeatNo = rawQuery.getString(5)
                    reminder.repeatType = rawQuery.getString(6)
                    reminder.isActive = rawQuery.getString(7) == "true"
                    reminder.day = rawQuery.getString(8)
                    list.add(reminder)
                } while (rawQuery.moveToNext())
            } else {
                rawQuery.close()
            }
            return list
        }

    fun updateReminderIsActive(reminderId: String, isActive: String): Int {
        val contentValues = ContentValues()
        contentValues.put("active", isActive)
        return writableDatabase.update(TABLE_REMINDERS, contentValues, "id=?", arrayOf(reminderId))
    }

    fun updateReminderDays(reminderId: String, days: String): Int {
        val contentValues = ContentValues()
        contentValues.put(FIELD_DAYS, days)
        return writableDatabase.update(TABLE_REMINDERS, contentValues, "id=?", arrayOf(reminderId))
    }

    fun updateReminderTime(reminderId: String, time: String): Int {
        val contentValues = ContentValues()
        contentValues.put("time", time)
        return writableDatabase.update(TABLE_REMINDERS, contentValues, "id=?", arrayOf(reminderId))
    }

    fun deleteReminder(reminderId: String) {
        writableDatabase.delete(TABLE_REMINDERS, "id=?", arrayOf(reminderId))
        writableDatabase.close()
    }


    companion object {
        private const val DATABASE_NAME = "ReminderDatabase"
        private const val TABLE_REMINDERS = "ReminderTable"
        private const val FIELD_ACTIVE = "active"
        private const val FIELD_DATE = "date"
        private const val FIELD_DAYS = "days"
        private const val FIELD_ID = "id"
        private const val FIELD_REPEAT = "repeat"
        private const val FIELD_REPEAT_NO = "repeat_no"
        private const val FIELD_REPEAT_TYPE = "repeat_type"
        private const val FIELD_TIME = "time"
        private const val FIELD_TITLE = "title"
    }
}