package homeworkout.sixpackabs.workoutfitness.di

import android.app.ActivityManager
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import homeworkout.sixpackabs.workoutfitness.db.DataBaseHelper
import homeworkout.sixpackabs.workoutfitness.reminder_service.ReminderDatabase
import homeworkout.sixpackabs.workoutfitness.utils.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSharePreference(@ApplicationContext context: Context): SharedPreferences {
        var sharedPreferences: SharedPreferences
        synchronized(PreferenceDataBase::class.java) {
            sharedPreferences =
                context.getSharedPreferences(Constant.CONS_PREFERENCE_MANAGER, Context.MODE_PRIVATE)
        }
        return sharedPreferences
    }

    @Singleton
    @Provides
    fun providePreferenceDataBase(
        @ApplicationContext context: Context,
        sharedPreferences: SharedPreferences
    ): PreferenceDataBase = PreferenceDataBase(context, sharedPreferences)

    @Singleton
    @Provides
    fun provideDataBaseHelper(
        @ApplicationContext context: Context,
        preferenceDataBase: PreferenceDataBase,
        common: Common
    ): DataBaseHelper = DataBaseHelper(context, preferenceDataBase, common)

    @Singleton
    @Provides
    fun provideReminderDatabase(@ApplicationContext context: Context): ReminderDatabase =
        ReminderDatabase(context)

    @Singleton
    @Provides
    fun provideCommon(
        @ApplicationContext context: Context,
        preferenceDataBase: PreferenceDataBase
    ): Common = Common(context, preferenceDataBase)

    @Singleton
    @Provides
    fun provideDialogUtils(
        preferenceDataBase: PreferenceDataBase,
        common: Common
    ): DialogUtils = DialogUtils(preferenceDataBase, common)

    @Singleton
    @Provides
    fun provideSpeechToText(
        @ApplicationContext context: Context,
        preferenceDataBase: PreferenceDataBase
    ): SpeechToText = SpeechToText(context, preferenceDataBase)

    @Singleton
    @Provides
    fun provideSoundUtil(
        @ApplicationContext context: Context,
        preferenceDataBase: PreferenceDataBase
    ): SoundUtil = SoundUtil(context, preferenceDataBase)

    @RequiresApi(Build.VERSION_CODES.M)
    @Singleton
    @Provides
    fun activityManager(@ApplicationContext context: Context): ActivityManager {
        return context.getSystemService(ActivityManager::class.java)
    }

    @Singleton
    @Provides
    fun providesPackageManager(@ApplicationContext context: Context): PackageManager {
        return context.packageManager
    }
}