package homeworkout.sixpackabs.workoutfitness.interfaces

interface DialogDismissCallBack {
    fun onDismiss(value: Any? = null)
}