package homeworkout.sixpackabs.workoutfitness.interfaces

interface OnMetricUnitDialogListener {
    fun setHeightWeight(checkedId: Int)
}