package homeworkout.sixpackabs.workoutfitness.interfaces

interface DialogCallBack {
    fun positiveClick()
    fun negativeClick()
}