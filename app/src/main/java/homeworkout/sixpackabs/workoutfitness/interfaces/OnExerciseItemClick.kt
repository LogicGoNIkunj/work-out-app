package homeworkout.sixpackabs.workoutfitness.interfaces

interface OnExerciseItemClick {
    fun onExerciseClick(index: Int)
}