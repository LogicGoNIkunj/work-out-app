package homeworkout.sixpackabs.workoutfitness.interfaces

import java.util.*

interface OnReminderListener {
    fun updateDays(reminderId: String, dayList: ArrayList<String>)
    fun updateTime(reminderId: String, timeList: ArrayList<String>, hour: Int, minute: Int)
}