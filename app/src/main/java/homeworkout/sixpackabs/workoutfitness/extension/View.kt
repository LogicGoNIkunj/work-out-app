package homeworkout.sixpackabs.workoutfitness.extension

import android.app.Activity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat


internal const val MATCH_PARENT = WindowManager.LayoutParams.MATCH_PARENT

internal const val WRAP_CONTENT = WindowManager.LayoutParams.WRAP_CONTENT

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun Activity.changeStatusBarColor(color: Int) {
    window.statusBarColor = this.getCompactColor(color)
}

internal fun AppCompatTextView.startDrawable(drawable: Int) {
    setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0)
}

internal fun EditText.startDrawable(drawable: Int) {
    setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0)
}

internal fun View.slideUp(animationDuration: Long = 500) {
    val animate = TranslateAnimation(0f, 0f, height.toFloat(), 0f)
    animate.duration = animationDuration
    animate.fillAfter = true
    animate.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationStart(p0: Animation?) = Unit

        override fun onAnimationEnd(p0: Animation?) = visible()

        override fun onAnimationRepeat(p0: Animation?) = Unit
    })
    startAnimation(animate)
}

internal inline fun View.slideDown(animationDuration: Long = 500, block: () -> Unit) {
    val animate = TranslateAnimation(0f, 0f, 0f, height.toFloat())
    animate.duration = animationDuration
    animate.fillAfter = true
    startAnimation(animate)
    block.invoke()
}

internal fun Activity.hideKeyboard(view: View? = currentFocus) {
    val imm: InputMethodManager =
        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.windowToken, 0)
}

internal fun Window.hideSystemBars() {
    decorView.systemUiVisibility =
        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    val windowInsetsController = ViewCompat.getWindowInsetsController(decorView) ?: return
    windowInsetsController.systemBarsBehavior =
        WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
}