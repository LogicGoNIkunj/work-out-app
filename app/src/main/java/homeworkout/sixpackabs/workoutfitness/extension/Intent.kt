package homeworkout.sixpackabs.workoutfitness.extension

import android.app.Activity
import android.content.Context
import android.content.Intent

inline fun <reified T> Context.startActivity(block: Intent.() -> Unit) {
    val intent = Intent(applicationContext, T::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
    intent.block()
    startActivity(intent)
}

inline fun Context.startAction(action: String, block: Intent.() -> Unit) {
    val intent = Intent(action)
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
    intent.block()
    startActivity(intent)
}


