package homeworkout.sixpackabs.workoutfitness.extension

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.net.ConnectivityManager
import android.os.Build
import android.text.Html
import android.view.View
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import homeworkout.sixpackabs.workoutfitness.R
import homeworkout.sixpackabs.workoutfitness.ui.activity.WorkoutActivity
import homeworkout.sixpackabs.workoutfitness.utils.Common
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

internal fun Context.showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

internal fun Context.getCompactColor(@ColorRes color: Int) = ContextCompat.getColor(this, color)

internal fun View.changeTint(@ColorRes color: Int){
    backgroundTintList = ColorStateList.valueOf(context.getCompactColor(color))
}

internal fun Context.getCompactDrawable(@DrawableRes drawable: Int) =ContextCompat.getDrawable(this, drawable)

internal fun String.getDay(common: Common) = SimpleDateFormat("dd", Locale.getDefault()).format(common.getStringToDate(this)?:"0").toInt()

internal fun String.getMonth(common: Common) = SimpleDateFormat("MM", Locale.getDefault()).format(common.getStringToDate(this)?:"0").toInt()

internal fun String.getYear(common: Common) = SimpleDateFormat("yyyy", Locale.getDefault()).format(common.getStringToDate(this)?:"0").toInt()

internal fun String.dateToDay(): String {
    return SimpleDateFormat("E", Locale.getDefault()).format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(this) ?: Date())
}

internal fun String.toHtml(): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY).toString()
      else
        Html.fromHtml(this).toString()

}

internal inline fun String.changeTimeFormat(toDate: String="HHH:mm",fromDate: String="hh:mm a",block:String.()->Unit) {
    val inputDateFormat = SimpleDateFormat(toDate,Locale.getDefault())
    val outDateFormat = SimpleDateFormat(fromDate,Locale.getDefault())

    var str = ""

    try {
       val date = inputDateFormat.parse(this)
        str = date?.let { outDateFormat.format(date) }?:""
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    str.block()

}

internal fun String.getExerciseColor(){
    when(this){
        "tbl_lower_body_list", "tbl_full_body_workouts_list"->{
            WorkoutActivity.backgroundColor = R.color.colorYellow_50
            WorkoutActivity.secondBackgroundColor = R.color.colorYellow_850
        }
        "tbl_chest_beginner", "tbl_chest_intermediate", "tbl_chest_advanced"->{
            WorkoutActivity.backgroundColor = R.color.colorPurple_500
            WorkoutActivity.secondBackgroundColor = R.color.colorPurple_850
        }
        "tbl_abs_beginner", "tbl_abs_intermediate", "tbl_abs_advanced"->{
            WorkoutActivity.backgroundColor = R.color.colorCreme_500
            WorkoutActivity.secondBackgroundColor = R.color.colorCreme_850
        }
        "tbl_arm_beginner", "tbl_arm_intermediate", "tbl_arm_advanced"->{
            WorkoutActivity.backgroundColor = R.color.colorBlue_500
            WorkoutActivity.secondBackgroundColor = R.color.colorBlue_850
        }
        "tbl_shoulder_back_beginner", "tbl_shoulder_back_intermediate", "tbl_shoulder_back_advanced"->{
            WorkoutActivity.backgroundColor = R.color.colorRed_500
            WorkoutActivity.secondBackgroundColor = R.color.colorRed_850
        }
        "tbl_leg_beginner", "tbl_leg_intermediate", "tbl_leg_advanced"->{
            WorkoutActivity.backgroundColor = R.color.colorPink_500
            WorkoutActivity.secondBackgroundColor = R.color.colorPink_850
        }
        "tbl_bw_exercise"->{
            WorkoutActivity.backgroundColor = R.color.colorCoffee_500
            WorkoutActivity.secondBackgroundColor = R.color.colorCoffee_850
        }

    }
}

fun Context.isNetworkAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    @SuppressLint("MissingPermission") val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}